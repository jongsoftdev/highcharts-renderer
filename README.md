# Highchart Renderer
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.jongsoft.utils/highchart-phantomjs/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.jongsoft.utils/highchart-renderer)
[![Coverage Status](https://coveralls.io/repos/bitbucket/jongsoftdev/highcharts-renderer/badge.svg?branch=master)](https://coveralls.io/bitbucket/jongsoftdev/highcharts-renderer?branch=master)
![](https://img.shields.io/bitbucket/pipelines/jongsoftdev/highcharts-renderer.svg)

[Highcharts](http://www.highcharts.com/products/highcharts) is a great JavaScript library to create dynamic charts for webpages. 

From the Highcharts website:
> Highcharts is a charting library written in pure JavaScript, offering an easy way of adding interactive charts to your web 
> site or web application. Highcharts currently supports line, spline, area, areaspline, column, bar, pie, scatter, angular
> gauges, arearange, areasplinerange, columnrange, bubble, box plot, error bars, funnel, waterfall and polar chart types.

This library enable Java 8 applications to use the power of the JavaScript Highcharts library. This library enables you to:

* Generate a JavaScript pojo which you can insert into the JavaScript from your Java code
* Create a screenshot of a Highchart.js chart using either
    * a phantomjs driven browser that will execute the Highchart JavaScript code
    * the export server provided by [Highcharts](http://export.highcharts.com)
* Expose the included JavaScript libraries through a servlet mapping

Current version of Highcharts: **v7.0.1 (2018-12-19)**

## Using the Core module

If you are only interested in injecting the needed JavaScript configuration into a website, but do not 
require any server side rendering. You only need to include the core module.

### Maven dependency information

```xml
<dependency>
	<groupId>com.jongsoft.utils</groupId>
	<artifactId>highchart-core</artifactId>
	<version>2.3.0</version>
</dependency>
```

For more information on the core module please see [Highchart Core Module](highcharts-core/README.md)

## Using the Export Server module
One way to generate images is using the official export server of Highchart.js or a self hosted instance of this export server. For this purpose a dedicated module is included to connect to the export server.

### Maven dependency information
```xml
<dependency>
	<groupId>com.jongsoft.utils</groupId>
	<artifactId>highchart-export-server</artifactId>
	<version>2.3.0</version>
</dependency>
```

For more information on the core module please see [Highchart Export Module](highcharts-export-server/README.md)

## Using the PhantomJS module
When you desire to use charts in PDF or as static images you can also opt to use the phantom js implementation that
is provided with this library to generate PNG files. To do so you will need to use the `PhantomJSRenderer` class, 
providing it with an `OutputStream` to write the image to.

### Maven dependency information

```xml
<dependency>
	<groupId>com.jongsoft.utils</groupId>
	<artifactId>highchart-phantomjs</artifactId>
	<version>2.3.0</version>
</dependency>
```
For more information on the core module please see [Highchart PhantomJS Module](highcharts-phantomjs/README.md)
