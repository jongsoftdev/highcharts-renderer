# Highchart Core module
![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.jongsoft.utils/highchart-core/badge.svg)

The Highchart Core module includes:

* Java POJO classes to contain Highcharts chart configuration
* Serialization tools to generate JavaScript objects from the Java POJO
* The base classes needed for the more advanced graph rendering modules

The root POJO classes to generate full and usable Highchart Configuration JavaScript objects are:

* Highchart, the basic Highcharts version (supports most type of series)
* GaugeHighchart
* Highchart3D, for 3D rendered graphs
* PolarHighchart

## Maven dependency information
The module is published in Maven Central for each release.

### Maven
```xml
<dependency>
  <groupId>com.jongsoft.utils</groupId>
  <artifactId>highchart-core</artifactId>
  <version>2.2.4</version>
</dependency>
```

### Gradle
```
compile group: 'com.jongsoft.utils', name: 'highchart-core', version: '2.2.4'
```

### SBT
```
libraryDependencies += "com.jongsoft.utils" % "highchart-core" % "2.2.4"
```

## Code example
```java
Highchart highchart = new Highchart();
        highchart.getChart()
            .setAlignTicks(true)
            .setAnimation(false)
            .setHeight(500)
            .setWidth(600)
            .setMargin(new Margin(15, 20))
            .getStyle()
                .css("color", new GraphColor("#ffffff"))
                .build()
            .build()
        .getCredits()
            .setEnabled(false);
    
    // Don't forget to set the axis
    highchart.addYAxis(new Axis()
        .setTickInterval(1.0)
        .setStartOnTick(Boolean.FALSE)
        .setEndOnTick(Boolean.FALSE)
        .setMin(0)
        .setMax(24)
        .getTitle().setText("Y Axis").build()
        .getLabels().setFormat("{value}").build());
    chartOptions.addXAxis(new Axis()
        .getLabels().setAlign(Alignment.LEFT).setX(5).setY(14).build()
        .setShowLastLabel(false));
    
    // Set some series with data
    LineSeries series = SeriesFactory.createSeries(SeriesType.LINE);
    series.add(new SeriesPoint().setX(1).setY(9));
    series.add(new SeriesPoint().setX(2).setY(12));
    series.add(new SeriesPoint().setX(3).setY(15));
    highchart.addSeries(series);
    
    // Now we can generate the JavaScript pojo for use in HTML files
    String javaScriptPojo = highchart.toJson();
```

You can then use the generated string to feed your web page. An example of that is provided below:

```javascript
Highcharts.Chart(
   // The JavaScript object generated
)
// Or it's jQuery equivalent 

$("my-dom-element").highcharts(
  // The JavaScript object generated
)
```