package com.jongsoft;

import java.util.Collections;
import java.util.Set;

/**
 * The {@link Extensions} interface allows for marking which {@link HighchartModules} are required
 * to render the implementing class.
 */
public interface Extensions {

    /**
     * A list of the required modules to be able to render the graph successfully.
     *
     * @return  the required list of javascript libraries
     */
    default Set<HighchartModules> neededModules() {
        return Collections.emptySet();
    }

}
