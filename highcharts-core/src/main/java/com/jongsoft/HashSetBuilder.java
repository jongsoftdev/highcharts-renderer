package com.jongsoft;

import com.jongsoft.util.Builder;

import java.util.HashSet;
import java.util.Set;

public class HashSetBuilder<T> implements Builder<Set<T>> {

    private final Set<T> internalSet = new HashSet<>();

    public HashSetBuilder<T> add(boolean condition, T entity) {
        if (condition) {
            internalSet.add(entity);
        }
        return this;
    }

    public HashSetBuilder<T> add(Set<T> set) {
        internalSet.addAll(set);
        return this;
    }

    public HashSetBuilder<T> add(T entity) {
        internalSet.add(entity);
        return this;
    }

    @Override
    public Set<T> build() {
        return internalSet;
    }

}
