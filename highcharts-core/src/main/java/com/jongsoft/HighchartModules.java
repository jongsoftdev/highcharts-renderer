package com.jongsoft;

import org.apache.commons.lang3.StringUtils;

/**
 * An enumerate containing all the supported modules from the official Highcharts.js
 */
public enum HighchartModules {
    // The core module for Highcharts
    HIGHCHART("highchart.js", "core", 0),
    // The expansion module for Highcharts
    HIGHCHART_MORE("highcharts-more.js", "more", 1),
    // The expansion module for using in heat maps
    HEATMAPS("highchart-map.js", "map", 2),
    GAUGE("highchart-gauge.js", "gauge", 2),
    // The expansion module needed for drawing 3D graphs
    THREE_D("highcharts-3d.js", "3d", 2),
    // The expansion module needed for the exporting options
    EXPORT("highcharts-export.js", "export", 2);

    private String fileName;
    private String logicalName;
    private int order;

    HighchartModules(String fileName, String logicalName, int order) {
        this.fileName = fileName;
        this.logicalName = logicalName;
        this.order = order;
    }

    public int getOrder() {
        return order;
    }

    @Override
    public String toString() {
        return fileName;
    }

    public static HighchartModules byLogicalName(String name) {
        for (HighchartModules module : HighchartModules.values()) {
            if (StringUtils.equalsIgnoreCase(name, module.logicalName)) {
                return module;
            }
        }

        return null;
    }
}
