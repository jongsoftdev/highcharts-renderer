package com.jongsoft;

public class Percentage extends Number {

    private double percentage;

    public Percentage(double percentage) {
        this.percentage = percentage;
    }

    @Override
    public int intValue() {
        return (int) (percentage * 100);
    }

    @Override
    public long longValue() {
        return (long) (percentage * 100);
    }

    @Override
    public float floatValue() {
        return (float) percentage;
    }

    @Override
    public double doubleValue() {
        return percentage;
    }

    @Override
    public String toString() {
        return "\"" + Integer.toString(intValue()) + "%\"";
    }

}
