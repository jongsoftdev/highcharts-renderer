package com.jongsoft;

/**
 * An empty interface depicting that an entity is can be rendered by a specialized serialization class.
 */
public interface Renderable {
}
