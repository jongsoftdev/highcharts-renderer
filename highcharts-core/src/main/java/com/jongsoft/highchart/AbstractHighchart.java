/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Extensions;
import com.jongsoft.HashSetBuilder;
import com.jongsoft.HighchartModules;
import com.jongsoft.Renderable;
import com.jongsoft.highchart.accessibility.Accessibility;
import com.jongsoft.highchart.axis.Axis;
import com.jongsoft.highchart.chart.*;
import com.jongsoft.highchart.common.Credits;
import com.jongsoft.highchart.common.GraphColor;
import com.jongsoft.highchart.monitoring.Monitoring;
import com.jongsoft.highchart.plotoptions.PlotOptions;
import com.jongsoft.highchart.responsive.Responsive;
import com.jongsoft.highchart.serializer.JsonUtils;
import com.jongsoft.highchart.serializer.SerializerException;
import com.jongsoft.highchart.series.Series;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.jongsoft.highchart.monitoring.Monitoring.Level.INFO;

public abstract class AbstractHighchart<T, C extends BaseChart<C, T>> implements Extensions, Renderable {

    @JsonProperty private Accessibility<T> accessibility;
    @JsonProperty private List<GraphColor> colors;
    @JsonProperty private Credits<T> credits;
    @JsonProperty private Exporting<T> exporting;
    @JsonProperty private Labels<T> labels;
    @JsonProperty private PlotOptions plotOptions;
    @JsonProperty private Responsive<T> responsive;
    @JsonProperty private List<Series<?>> series;
    @JsonProperty private Tooltip<T> tooltip;
    @JsonProperty private ChartTitle<T> title;
    @JsonProperty private ChartTitle<T> subtitle;
    @JsonProperty private List<Axis> xAxis;
    @JsonProperty private List<Axis> yAxis;
    @JsonProperty private Legend<T> legend;

    public AbstractHighchart() {
        this.accessibility = new Accessibility<>(self());
        this.series = new ArrayList<>();
        this.exporting = new Exporting<>(self());
        this.colors = new ArrayList<>();
        this.credits = new Credits<>(self());
        this.labels = new Labels<>(self());
        this.subtitle = new ChartTitle<>(self());
        this.title = new ChartTitle<>(self());
        this.xAxis = new ArrayList<>();
        this.yAxis = new ArrayList<>();
        this.legend = new Legend<>(self());
        this.plotOptions = new PlotOptions();
        this.responsive = new Responsive<>(self());
        this.tooltip = new Tooltip<>(self());
    }

    protected abstract T self();
    public abstract C getChart();

    /**
     * Options for the Accessibility module.
     */
    public Accessibility<T> getAccessibility() {
        return accessibility;
    }

    public List<GraphColor> getColors() {
        return colors;
    }

    /**
     * Options for the exporting module.
     *
     * @see <a href="http://www.highcharts.com/docs/export-module/export-module-overview">Exporting module online documentation</a>
     */
    public Exporting<T> getExporting() {
        return exporting;
    }

    /**
     * Highchart by default puts a credits label in the lower right corner of the chart. This can be
     * changed using these options.
     */
    public Credits<T> getCredits() {
        return credits;
    }

    /**
     * HTML labels that can be positioned anywhere in the chart area.
     */
    public Labels<T> getLabels() {
        return labels;
    }

    /**
     * The chart's subtitle
     */
    public ChartTitle<T> getSubtitle() {
        return subtitle;
    }

    /**
     * The chart's main title.
     */
    public ChartTitle<T> getTitle() {
        return title;
    }

    /**
     * Add a series to the chart.
     */
    public T addSeries(Series series) {
        this.series.add(series);
        return self();
    }

    /**
     * The X axis or category axis. Normally this is the horizontal axis, though if the chart is inverted this is the
     * vertical axis. In case of multiple axes, the xAxis node is an array of
     * configuration objects.
     */
    public T addXAxis(Axis axis) {
        xAxis.add(axis);
        return self();
    }

    /**
     * The Y axis or value axis. Normally this is the vertical axis, though if the chart is inverted this is the horizontal
     * axis. In case of multiple axes, the yAxis node is an array of configuration objects.
     */
    public T addYAxis(Axis axis) {
        yAxis.add(axis);
        return self();
    }

    public Legend<T> getLegend() {
        return legend;
    }

    public PlotOptions getPlotOptions() {
        return plotOptions;
    }

    /**
     * Allows setting a set of rules to apply for different screen or chart sizes. Each rule specifies
     * additional chart options.
     *
     * @since 5.0.0
     */
    public Responsive<T> getResponsive() {
        return responsive;
    }

    /**
     * Options for the tooltip that appears when the user hovers over a series or point.
     */
    public Tooltip<T> getTooltip() {
        return tooltip;
    }
    
    /**
     * @return  the JSON representation of the Highcart Graph
     * @throws SerializerException  in case of failures to serialize the entity
     */
    @Monitoring(level = INFO)
    public String toJson() {
        return JsonUtils.serialize(self());
    }

    @Override
    public Set<HighchartModules> neededModules() {
        return new HashSetBuilder<HighchartModules>()
                .add(series.stream().map(Series::neededModules).flatMap(Set::stream).collect(Collectors.toSet()))
                .build();
    }
}
