/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.highchart.common.Title;
import com.jongsoft.highchart.common.VerticalAlignment;

public class ChartTitle<T> extends Title<ChartTitle<T>, T> {

    @JsonProperty private VerticalAlignment verticalAlign;
    @JsonProperty private Boolean floating;
    @JsonProperty private Boolean useHTML;

    public ChartTitle(T parent) {
        super(parent);
    }

    /**
     * When the title is floating, the plot area will not move to make space for it.
     * <p>
     * <b>Default:</b> false.
     */
    public ChartTitle<T> setFloating(Boolean floating) {
        this.floating = floating;
        return self();
    }

    /**
     * Whether to use HTML to render the text.
     * <p>
     * <b>Default:</b> false.
     */
    public ChartTitle<T> setUseHTML(Boolean useHTML) {
        this.useHTML = useHTML;
        return self();
    }

    /**
     * The vertical alignment of the title. Can be one of "top", "middle" and "bottom". When a value is given, the title behaves as floating.
     * <p>
     * @since 2.1
     */
    public ChartTitle<T> setVerticalAlign(VerticalAlignment verticalAlign) {
        this.verticalAlign = verticalAlign;
        return self();
    }

    @Override
    protected ChartTitle<T> self() {
        return this;
    }

}
