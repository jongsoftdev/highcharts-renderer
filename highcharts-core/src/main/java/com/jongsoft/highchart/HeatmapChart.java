/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.HashSetBuilder;
import com.jongsoft.HighchartModules;
import com.jongsoft.highchart.axis.ColorAxis;
import com.jongsoft.highchart.chart.Chart;

import java.util.Set;

public class HeatmapChart extends AbstractHighchart<HeatmapChart, Chart<HeatmapChart>> {

    @JsonProperty private Chart<HeatmapChart> chart;
    @JsonProperty private ColorAxis colorAxis;

    public HeatmapChart() {
        chart = new Chart<>(this);
    }

    public HeatmapChart setColorAxis(ColorAxis colorAxis) {
        this.colorAxis = colorAxis;
        return this;
    }
    
    public ColorAxis getColorAxis() {
        if (colorAxis == null) {
            colorAxis = new ColorAxis();
        }
        return colorAxis;
    }

    @Override
    public Chart<HeatmapChart> getChart() {
        return chart;
    }

    @Override
    public Set<HighchartModules> neededModules() {
        return new HashSetBuilder<HighchartModules>()
                .add(chart.neededModules())
                .add(HighchartModules.HIGHCHART)
                .add(HighchartModules.HEATMAPS)
                .build();
    }

    @Override
    protected HeatmapChart self() {
        return this;
    }

}
