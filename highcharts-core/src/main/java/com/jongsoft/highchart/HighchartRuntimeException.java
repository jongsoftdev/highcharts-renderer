package com.jongsoft.highchart;

public class HighchartRuntimeException extends RuntimeException {

    public HighchartRuntimeException() {
    }

    public HighchartRuntimeException(String message) {
        super(message);
    }

    public HighchartRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

}
