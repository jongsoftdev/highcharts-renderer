/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.HashSetBuilder;
import com.jongsoft.HighchartModules;
import com.jongsoft.highchart.chart.Chart;
import com.jongsoft.highchart.chart.Pane;

import java.util.Set;

public class PolarHighchart extends AbstractHighchart<PolarHighchart, Chart<PolarHighchart>> {

    @JsonProperty private Pane<PolarHighchart> pane;

    /**
     * This configuration object holds general options for the combined X and Y axes set.
     * Each xAxis or yAxis can reference the pane by index.
     */
    public Pane<PolarHighchart> getPane() {
        if (pane == null) {
            pane = new Pane<>(self());
        }
        return pane;
    }

    @Override
    public Chart<PolarHighchart> getChart() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<HighchartModules> neededModules() {
        return new HashSetBuilder<HighchartModules>()
                .add(HighchartModules.HIGHCHART)
                .add(HighchartModules.HIGHCHART_MORE)
                .build();
    }

    @Override
    protected PolarHighchart self() {
        return this;
    }
}
