/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.accessibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.highchart.common.Function;
import com.jongsoft.util.Builder;

/**
 *
 * @since 5.0.0
 * @param <T>   the highchart class that instantiated this instance
 */
public class Accessibility<T> implements Builder<T> {

    @JsonProperty private Boolean describeSingleSeries;
    @JsonProperty private Boolean enabled;
    @JsonProperty private KeyboardNavigation<T> keyboardNavigation;
    @JsonProperty private Function onTableAnchorClick;
    @JsonProperty private String pointDateFormat;
    @JsonProperty private Function pointDateFormatter;
    @JsonProperty private Function pointDescriptionFormatter;
    @JsonProperty private Function screenReaderSectionFormatter;

    private final T parent;

    public Accessibility(T parent) {
        this.parent = parent;
        this.keyboardNavigation = new KeyboardNavigation<>(this);
    }

    /**
     * Whether or not to add series descriptions to charts with a single series.
     * <p>
     * <b>Default:</b> false.
     *
     * @since 5.0.0
     */
    public Accessibility<T> setDescribeSingleSeries(Boolean describeSingleSeries) {
        this.describeSingleSeries = describeSingleSeries;
        return this;
    }

    /**
     * Enable accessibility features for the chart.
     * <p>
     * <b>Default:</b> true.
     *
     * @since 5.0.0
     */
    public Accessibility<T> setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    /**
     * Options for keyboard navigation.
     *
     * @since 5.0.0
     */
    public KeyboardNavigation<T> getKeyboardNavigation() {
        return keyboardNavigation;
    }

    /**
     * Function to run upon clicking the "View as Data Table" link in the screen reader region.
     * <br/>
     * By default Highcharts will insert and set focus to a data table representation of the chart.
     *
     * @since 5.0.0
     */
    public Accessibility<T> setOnTableAnchorClick(Function onTableAnchorClick) {
        this.onTableAnchorClick = onTableAnchorClick;
        return this;
    }

    /**
     * Date format to use for points on datetime axes when describing them to screen reader users.
     * <p>
     * For an overview of the replacement codes, see
     * <a href="http://api.highcharts.com/highcharts/accessibility.onTableAnchorClick#Highcharts.dateFormat">dateFormat</a>.
     * <p>
     * <b>Default:</b> the same format as in tooltip.
     *
     * @since 5.0.0
     */
    public Accessibility<T> setPointDateFormat(String pointDateFormat) {
        this.pointDateFormat = pointDateFormat;
        return this;
    }

    /**
     * Formatter function to determine the date/time format used with points on datetime axes when
     * describing them to screen reader users. Receives one argument, point, referring to the point to describe.
     * Should return a date format string compatible with dateFormat.
     *
     * @since 5.0.0
     */
    public Accessibility<T> setPointDateFormatter(Function.Point pointDateFormatter) {
        this.pointDateFormatter = pointDateFormatter;
        return this;
    }

    /**
     * Formatter function to use instead of the default for point descriptions. Receives one argument,
     * point, referring to the point to describe. Should return a String with the description of the
     * point for a screen reader user.
     *
     * @since 5.0.0
     */
    public Accessibility<T> setPointDescriptionFormatter(Function.Point pointDescriptionFormatter) {
        this.pointDescriptionFormatter = pointDescriptionFormatter;
        return this;
    }

    /**
     * A formatter function to create the HTML contents of the hidden screen reader information region.
     * Receives one argument, chart, referring to the chart object. Should return a String with
     * the HTML content of the region.
     * <p>
     * The link to view the chart as a data table will be added automatically after the custom HTML content.
     *
     * @since 5.0.0
     */
    public Accessibility<T> setScreenReaderSectionFormatter(Function.Chart screenReaderSectionFormatter) {
        this.screenReaderSectionFormatter = screenReaderSectionFormatter;
        return this;
    }

    @Override
    public T build() {
        return parent;
    }
}
