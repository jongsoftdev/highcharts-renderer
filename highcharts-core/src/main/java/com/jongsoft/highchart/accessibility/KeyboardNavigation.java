/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.accessibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.util.Builder;

/**
 * @since 5.0.0
 */
public class KeyboardNavigation<Y> implements Builder<Accessibility<Y>> {

    @JsonProperty private Boolean enabled;
    @JsonProperty private Boolean skipNullPoints;

    private final Accessibility<Y> parent;

    public KeyboardNavigation(Accessibility<Y> parent) {
        this.parent = parent;
    }

    /**
     * Enable keyboard navigation for the chart. Defaults to true.
     * <p>
     * <b>Default:</b> true.
     *
     * @since 5.0.0
     */
    public KeyboardNavigation<Y> setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    /**
     * Skip null points when navigating through points with the keyboard.
     * <p>
     * <b>Default:</b> false.
     *
     * @since 5.0.0
     */
    public KeyboardNavigation<Y> setSkipNullPoints(Boolean skipNullPoints) {
        this.skipNullPoints = skipNullPoints;
        return this;
    }

    @Override
    public Accessibility<Y> build() {
        return parent;
    }
}
