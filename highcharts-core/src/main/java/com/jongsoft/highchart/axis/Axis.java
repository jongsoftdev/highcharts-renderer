package com.jongsoft.highchart.axis;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Renderable;
import com.jongsoft.highchart.common.Crosshair;
import com.jongsoft.highchart.common.DashStyle;
import com.jongsoft.highchart.common.Function;
import com.jongsoft.highchart.common.GraphColor;

import java.util.ArrayList;
import java.util.List;

public class Axis implements Renderable {

    @JsonProperty private Boolean allowDecimals;
    @JsonProperty private GraphColor alternateGridColor;
    @JsonProperty private final List<Break> breaks;
    @JsonProperty private final List<String> categories;
    @JsonProperty private Number ceiling;
    @JsonProperty private String className;
    @JsonProperty private final Crosshair<Axis> crosshair;
    @JsonProperty private Boolean endOnTick;
    @JsonProperty private AxisEvents events;
    @JsonProperty private Number floor;
    @JsonProperty private GraphColor gridLineColor;
    @JsonProperty private DashStyle gridLineDashStyle;
    @JsonProperty private Number gridLineWidth;
    @JsonProperty private Number gridLineZIndex;
    @JsonProperty private String id;
    @JsonProperty private final AxisLabels<Axis> labels;
    @JsonProperty private GraphColor lineColor;
    @JsonProperty private Number lineWidth;
    @JsonProperty private Number linkedTo;
    @JsonProperty private Number max;
    @JsonProperty private Number maxPadding;
    @JsonProperty private Number min;
    @JsonProperty private Number minPadding;
    @JsonProperty private Number minRange;
    @JsonProperty private Number minTickInterval;
    @JsonProperty private GraphColor minorGridLineColor;
    @JsonProperty private DashStyle minorGridLineDashStyle;
    @JsonProperty private Number minorGridLineWidth;
    @JsonProperty private GraphColor minorTickColor;
    @JsonProperty private Number minorTickLength;
    @JsonProperty private TickPosition minorTickPosition;
    @JsonProperty private Number minorTickWidth;
    @JsonProperty private Number offset;
    @JsonProperty private Boolean opposite;
    @JsonProperty private List<PlotLine> plotLines;
    @JsonProperty private Boolean reversed;
    @JsonProperty private Number softMax;
    @JsonProperty private Number softMin;
    @JsonProperty private Boolean showEmpty;
    @JsonProperty private Boolean showFirstLabel;
    @JsonProperty private Boolean showLastLabel;
    @JsonProperty private Boolean startOnTick;
    @JsonProperty private Number tickAmount;
    @JsonProperty private GraphColor tickColor;
    @JsonProperty private Number tickInterval;
    @JsonProperty private Number tickLength;
    @JsonProperty private Number tickPixelInterval;
    @JsonProperty private TickPosition tickPosition;
    @JsonProperty private Function tickPositioner;
    @JsonProperty private List<Number> tickPositions;
    @JsonProperty private Number tickWidth;
    @JsonProperty private String tickmarkPlacement;
    @JsonProperty private final AxisTitle<Axis> title;
    @JsonProperty private AxisType type;
    @JsonProperty private List<AxisUnits> units;
    @JsonProperty private Boolean visible;

    public Axis() {
        categories = new ArrayList<>();
        breaks = new ArrayList<>();
        crosshair = new Crosshair<>(this);
        labels = new AxisLabels<>(this);
        title = new AxisTitle<>(this);
        units = new ArrayList<>();
        plotLines = new ArrayList<>();
    }

    /**
     * Whether to allow decimals in this axis' ticks. When counting integers, like persons or hits on a web page,
     * decimals must be avoided in the axis tick labels.
     * <p>
     * <b>Default:</b> true.
     */
    public Axis setAllowDecimals(Boolean allowDecimals) {
        this.allowDecimals = allowDecimals;
        return this;
    }

    /**
     * Whether to force the axis to end on a tick. Use this option with the maxPadding option to control the axis end.
     */
    public Axis setEndOnTick(Boolean endOnTick) {
        this.endOnTick = endOnTick;
        return this;
    }

    /**
     * When using an alternate grid color, a band is painted across the plot area between every other grid line.
     */
    public Axis setAlternateGridColor(GraphColor alternateGridColor) {
        this.alternateGridColor = alternateGridColor;
        return this;
    }

    /**
     * If categories are present for the xAxis, names are used instead of numbers for that axis. Since Highcharts 3.0, categories
     * can also be extracted by giving each point a name and setting axis type to category. However, if you have multiple series,
     * best practice remains defining the categories array.
     */
    public Axis addCategory(String category) {
        categories.add(category);
        return this;
    }

    /**
     * An array defining breaks in the axis, the sections defined will be left out and all the points shifted closer to
     * each other. Requires that the broken-axis.js module is loaded.
     */
    public Axis addBreak(Break breaks) {
        this.breaks.add(breaks);
        return this;
    }

    /**
     * The highest allowed value for automatically computed axis extremes.
     *
     * @see #setFloor(Number)
     * @since 4.0
     */
    public Axis setCeiling(Number ceiling) {
        this.ceiling = ceiling;
        return this;
    }

    /**
     * A class name that opens for styling the axis by CSS, especially in Highcharts styled mode. The class name is
     * applied to group elements for the grid, axis elements and labels.
     *
     * @since 5.0.0
     */
    public Axis setClassName(String className) {
        this.className = className;
        return this;
    }

    /**
     * The lowest allowed value for automatically computed axis extremes.
     *
     * @see #setCeiling(Number)
     * @since 4.0
     */
    public Axis setFloor(Number floor) {
        this.floor = floor;
        return this;
    }

    /**
     * Color of the grid lines extending the ticks across the plot area.
     * <p>
     * <b>Default:</b> #D8D8D8
     */
    public Axis setGridLineColor(GraphColor gridLineColor) {
        this.gridLineColor = gridLineColor;
        return this;
    }

    /**
     * The dash or dot style of the grid lines.
     * <p>
     * <b>Default:</b> {@link DashStyle#SOLID}
     */
    public Axis setGridLineDashStyle(DashStyle gridLineDashStyle) {
        this.gridLineDashStyle = gridLineDashStyle;
        return this;
    }

    /**
     * Event handlers for the axis.
     */
    public Axis setEvents(AxisEvents events) {
        this.events = events;
        return this;
    }

    /**
     * The width of the grid lines extending the ticks across the plot area.
     * <p>
     * <b>Default: </b> 0.
     */
    public Axis setGridLineWidth(Number gridLineWidth) {
        this.gridLineWidth = gridLineWidth;
        return this;
    }

    /**
     * The Z index of the grid lines.
     * <p>
     * <b>Default:</b> 1.
     */
    public Axis setGridLineZIndex(Number gridLineZIndex) {
        this.gridLineZIndex = gridLineZIndex;
        return this;
    }

    /**
     * An id for the axis. This can be used after render time to get a pointer to the axis object through chart.get().
     */
    public Axis setId(String id) {
        this.id = id;
        return this;
    }

    /**
     * The color of the line marking the axis itself.
     */
    public Axis setLineColor(GraphColor lineColor) {
        this.lineColor = lineColor;
        return this;
    }

    /**
     * The width of the line marking the axis itself.
     */
    public Axis setLineWidth(Number lineWidth) {
        this.lineWidth = lineWidth;
        return this;
    }

    /**
     * Index of another axis that this axis is linked to. When an axis is linked to a master axis,
     * it will take the same extremes as the master, but as assigned by min or max or by setExtremes. It can be
     * used to show additional info, or to ease reading the chart by duplicating the scales.
     */
    public Axis setLinkedTo(Number linkedTo) {
        this.linkedTo = linkedTo;
        return this;
    }

    /**
     * The maximum value of the axis. If null, the max value is automatically calculated. If the endOnTick option is true,
     * the max value might be rounded up. The actual maximum value is also influenced by {@link }chart.alignTicks.
     */
    public Axis setMax(Number max) {
        this.max = max;
        return this;
    }

    /**
     * Padding of the max value relative to the length of the axis. A padding of 0.05 will make a 100px axis 5px longer.
     * This is useful when you don't want the highest data value to appear on the edge of the plot area. When the axis' max
     * option is set or a max extreme is set using {@link Axis#setExtemes()}, the maxPadding will be ignored.
     */
    public Axis setMaxPadding(Number maxPadding) {
        this.maxPadding = maxPadding;
        return this;
    }

    /**
     * The minimum value of the axis. If null the min value is automatically calculated.
     * If the startOnTick option is true, the min value might be rounded down.
     */
    public Axis setMin(Number min) {
        this.min = min;
        return this;
    }

    /**
     * A soft maximum for the axis. If the series data maximum is greater than this, the axis will stay at this
     * maximum, but if the series data maximum is higher, the axis will flex to show all data.
     *
     * @since 5.0.1
     */
    public Axis setSoftMax(Number softMax) {
        this.softMax = softMax;
        return this;
    }

    /**
     * A soft minimum for the axis. If the series data minimum is greater than this, the axis will stay at
     * this minimum, but if the series data minimum is lower, the axis will flex to show all data.
     *
     * @since 5.0.1
     */
    public Axis setSoftMin(Number softMin) {
        this.softMin = softMin;
        return this;
    }

    /**
     * Padding of the min value relative to the length of the axis. A padding of 0.05 will make a 100px axis 5px longer.
     * This is useful when you don't want the lowest data value to appear on the edge of the plot area. When the axis' min
     * option is set or a min extreme is set using {@link #setExtremes()}, the minPadding will be ignored.
     */
    public Axis setMinPadding(Number minPadding) {
        this.minPadding = minPadding;
        return this;
    }

    /**
     * The minimum range to display on this axis. The entire axis will not be allowed to span over a smaller interval than this.
     * For example, for a datetime axis the main unit is milliseconds. If minRange is set to 3600000, you
     * can't zoom in more than to one hour.
     * <p>
     * The default minRange for the x axis is five times the smallest interval between any of the data points.
     * <p>
     * On a logarithmic axis, the unit for the minimum range is the power. So a minRange of 1 means that the axis
     * can be zoomed to 10-100, 100-1000, 1000-10000 etc.
     * <p>
     * <b>Note</b> the minPadding, maxPadding, startOnTick and endOnTick settings also affect how
     * the extremes of the axis are computed.
     */
    public Axis setMinRange(Number minRange) {
        this.minRange = minRange;
        return this;
    }

    /**
     * The minimum tick interval allowed in axis values. For example on zooming in on an axis with daily data, this can
     * be used to prevent the axis from showing hours. Defaults to the closest distance between two points on the axis.
     */
    public Axis setMinTickInterval(Number minTickInterval) {
        this.minTickInterval = minTickInterval;
        return this;
    }

    /**
     * Color of the minor, secondary grid lines.
     */
    public Axis setMinorGridLineColor(GraphColor minorGridLineColor) {
        this.minorGridLineColor = minorGridLineColor;
        return this;
    }

    /**
     * The dash or dot style of the minor grid lines.
     * <p>
     * <b>Default: </b> {@link DashStyle#SOLID}
     */
    public Axis setMinorGridLineDashStyle(DashStyle minorGridLineDashStyle) {
        this.minorGridLineDashStyle = minorGridLineDashStyle;
        return this;
    }

    /**
     * Width of the minor, secondary grid lines.
     */
    public Axis setMinorGridLineWidth(Number minorGridLineWidth) {
        this.minorGridLineWidth = minorGridLineWidth;
        return this;
    }

    /**
     * Color for the minor tick marks.
     */
    public Axis setMinorTickColor(GraphColor minorTickColor) {
        this.minorTickColor = minorTickColor;
        return this;
    }

    /**
     * The pixel length of the minor tick marks.
     * <p>
     * <b>Default: </b> 2
     */
    public Axis setMinorTickLength(Number minorTickLength) {
        this.minorTickLength = minorTickLength;
        return this;
    }

    /**
     * The position of the minor tick marks relative to the axis line.
     */
    public Axis setMinorTickPosition(TickPosition minorTickPosition) {
        this.minorTickPosition = minorTickPosition;
        return this;
    }

    /**
     * The pixel width of the minor tick mark.
     */
    public Axis setMinorTickWidth(Number minorTickWidth) {
        this.minorTickWidth = minorTickWidth;
        return this;
    }

    /**
     * The distance in pixels from the plot area to the axis line. A positive offset moves the axis with it's
     * line, labels and ticks away from the plot area. This is typically used when two or more axes are
     * displayed on the same side of the plot.
     */
    public Axis setOffset(Number offset) {
        this.offset = offset;
        return this;
    }

    /**
     * Whether to display the axis on the opposite side of the normal. The normal is on the left side for vertical axes
     * and bottom for horizontal, so the opposite sides will be right and top respectively. This is typically used
     * with dual or multiple axes.
     */
    public Axis setOpposite(Boolean opposite) {
        this.opposite = opposite;
        return this;
    }

    /**
     * Whether to reverse the axis so that the highest number is closest to the origin. If the chart is inverted,
     * the x axis is reversed by default.
     */
    public Axis setReversed(Boolean reversed) {
        this.reversed = reversed;
        return this;
    }

    /**
     * Whether to show the axis line and title when the axis has no data
     * <p>
     * <b>Default: </b> true
     */
    public Axis setShowEmpty(Boolean showEmpty) {
        this.showEmpty = showEmpty;
        return this;
    }

    /**
     * Whether to show the first tick label.
     */
    public Axis setShowFirstLabel(Boolean showFirstLabel) {
        this.showFirstLabel = showFirstLabel;
        return this;
    }

    /**
     * Whether to show the last tick label.
     */
    public Axis setShowLastLabel(Boolean showLastLabel) {
        this.showLastLabel = showLastLabel;
        return this;
    }

    /**
     * Whether to force the axis to start on a tick. Use this option with the minPadding option to control the axis start
     */
    public Axis setStartOnTick(Boolean startOnTick) {
        this.startOnTick = startOnTick;
        return this;
    }

    /**
     * The amount of ticks to draw on the axis. This opens up for aligning the ticks of multiple charts or panes within a
     * chart. This option overrides the {@link #setTickPixelInterval(Number)} option.
     * <p>
     * This option only has an effect on linear axes. Datetime, logarithmic or category axes are not affected.
     */
    public Axis setTickAmount(Number tickAmount) {
        this.tickAmount = tickAmount;
        return this;
    }

    /**
     * Color for the main tick marks.
     */
    public Axis setTickColor(GraphColor tickColor) {
        this.tickColor = tickColor;
        return this;
    }

    /**
     * The interval of the tick marks in axis units. When null, the tick interval is computed to approximately follow the
     * {@link #setTickPixelInterval(Number)} on linear and datetime axes.
     * <p>
     * On categorized axes, a null tickInterval will default to 1, one category.
     * <p>
     * <b>Note:</b> that datetime axes are based on milliseconds, so for example an interval of one day is expressed as 24 * 3600 * 1000.
     * <p>
     * On logarithmic axes, the tickInterval is based on powers, so a tickInterval of 1 means one tick on each of 0.1, 1, 10, 100 etc.
     * A tickInterval of 2 means a tick of 0.1, 10, 1000 etc. A tickInterval of 0.2 puts a tick on 0.1, 0.2, 0.4, 0.6, 0.8, 1, 2, 4, 6, 8, 10, 20, 40 etc.
     * <p>
     * If the tickInterval is too dense for labels to be drawn, Highcharts may remove ticks.
     */
    public Axis setTickInterval(Number tickInterval) {
        this.tickInterval = tickInterval;
        return this;
    }

    /**
     * The pixel length of the main tick marks.
     * <p>
     * <b>Default:</b> 10
     */
    public Axis setTickLength(Number tickLength) {
        this.tickLength = tickLength;
        return this;
    }

    /**
     * If tickInterval is null this option sets the approximate pixel interval of the tick marks. Not applicable to categorized axis.
     */
    public Axis setTickPixelInterval(Number tickPixelInterval) {
        this.tickPixelInterval = tickPixelInterval;
        return this;
    }

    /**
     * The position of the major tick marks relative to the axis line.
     * <p>
     * <b>Default: </b> {@link TickPosition#OUTSIDE}
     */
    public Axis setTickPosition(TickPosition tickPosition) {
        this.tickPosition = tickPosition;
        return this;
    }

    /**
     * A callback function returning array defining where the ticks are laid out on the axis.
     * This overrides the default behaviour of {@link #setTickPixelInterval(Number)}} and {@link #setTickInterval(Number)}.
     * <p>
     * The automatic tick positions are accessible through this.tickPositions and can
     * be modified by the callback.
     */
    public Axis setTickPositioner(Function tickPositioner) {
        this.tickPositioner = tickPositioner;
        return this;
    }

    /**
     * An array defining where the ticks are laid out on the axis.
     * This overrides the default behaviour of {@link #setTickPixelInterval(Number)}} and {@link #setTickInterval(Number)}.
     */
    public Axis setTickPositions(List<Number> tickPositions) {
        this.tickPositions = tickPositions;
        return this;
    }

    /**
     * The pixel width of the major tick marks.
     */
    public Axis setTickmarkPlacement(String tickmarkPlacement) {
        this.tickmarkPlacement = tickmarkPlacement;
        return this;
    }

    /**
     * For categorized axes only. If on the tick mark is placed in the center of the category, if between the tick
     * mark is placed between categories. The default is between if the tickInterval is 1, else on.
     */
    public Axis setTickWidth(Number tickWidth) {
        this.tickWidth = tickWidth;
        return this;
    }

    /**
     * The type of axis. Can be one of "linear", "logarithmic", "datetime" or "category". In a datetime axis, the numbers are given in milliseconds, and tick
     * marks are placed on appropriate values like full hours or days. In a category axis, the point names of the chart's series are used for categories, if not
     * a categories array is defined.
     * <p>
     * <b>Default:</b> {@link AxisType#LINEAR}
     */
    public Axis setType(AxisType type) {
        this.type = type;
        return this;
    }

    /**
     * Whether axis, including axis title, line, ticks and labels, should be visible.
     * 
     * <b>Default: </b> true
     */
    public Axis setVisible(Boolean visible) {
        this.visible = visible;
        return this;
    }
    
    /**
     * An array of objects representing plot lines on the axis
     */
    public Axis addPlotLine(PlotLine plotLine) {
        plotLines.add(plotLine);
        return this;
    }

    /**
     * Configure a crosshair that follows either the mouse pointer or the hovered point.
     */
    public Crosshair<Axis> getCrosshair() {
        return crosshair;
    }

    /**
     * The axis labels show the number or category for each tick.
     */
    public AxisLabels<Axis> getLabels() {
        return labels;
    }

    /**
     * The axis title, showing next to the axis line.
     */
    public AxisTitle<Axis> getTitle() {
        return title;
    }

    /**
     * Datetime axis only. An array determining what time intervals the ticks are allowed to fall on. Each array item is an array where the first value is the
     * time unit and the second value another array of allowed multiples.
     */
    public List<AxisUnits> getUnits() {
        return units;
    }

}
