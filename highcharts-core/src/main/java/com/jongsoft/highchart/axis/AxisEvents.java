/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.axis;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Renderable;
import com.jongsoft.highchart.common.Function;

public class AxisEvents implements Renderable {

    @JsonProperty private Function afterBreaks;
    @JsonProperty private Function afterSetExtremes;
    @JsonProperty private Function pointBreak;
    @JsonProperty private Function setExtremes;

    /**
     * An event fired after the breaks have rendered.
     */
    public AxisEvents setAfterBreaks(Function afterBreaks) {
        this.afterBreaks = afterBreaks;
        return this;
    }

    /**
     * As opposed to the setExtremes event, this event fires after the final min and max values are
     * computed and corrected for minRange.
     */
    public AxisEvents setAfterSetExtremes(Function afterSetExtremes) {
        this.afterSetExtremes = afterSetExtremes;
        return this;
    }

    /**
     * An event fired when a break from this axis occurs on a point.
     * <p>
     * The <code>this</code> keyword refers to the Axis object.
     */
    public AxisEvents setPointBreak(Function pointBreak) {
        this.pointBreak = pointBreak;
        return this;
    }

    /**
     * Fires when the minimum and maximum is set for the axis, either by calling the .setExtremes() method or by
     * selecting an area in the chart. One parameter, event, is passed to the function. This contains common event
     * information based on jQuery.
     * <p>
     * The new user set minimum and maximum values can be found by event.min and event.max. When an axis is zoomed
     * all the way out from the "Reset zoom" button, event.min and event.max are null, and the new extremes are set
     * based on <code>this.dataMin</code> and <code>this.dataMax</code>.
     * <p>
     * The <code>this</code> keyword refers to the Axis object.
     */
    public AxisEvents setSetExtremes(Function setExtremes) {
        this.setExtremes = setExtremes;
        return this;
    }

}
