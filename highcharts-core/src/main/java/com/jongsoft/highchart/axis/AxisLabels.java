/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.axis;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Renderable;
import com.jongsoft.util.Builder;
import com.jongsoft.highchart.common.Alignment;
import com.jongsoft.highchart.common.CSSObject;
import com.jongsoft.highchart.common.Function;

import java.util.ArrayList;
import java.util.List;

public class AxisLabels<T> implements Builder<T>, Renderable {

    private final T parent;
    @JsonProperty private Alignment align;
    @JsonProperty private final List<Number> autoRotation;
    @JsonProperty private Number autoRotationLimit;
    @JsonProperty private Number distance;
    @JsonProperty private Boolean enabled;
    @JsonProperty private String format;
    @JsonProperty private Function.Formatter formatter;
    @JsonProperty private Number padding;
    @JsonProperty private Boolean reserveSpace;
    @JsonProperty private Number rotation;
    @JsonProperty private Number staggerLines;
    @JsonProperty private Number step;
    @JsonProperty private final CSSObject<AxisLabels<T>> style;
    @JsonProperty private Boolean useHTML;
    @JsonProperty private Number x;
    @JsonProperty private Number y;
    @JsonProperty private Number zIndex;

    public AxisLabels(T parent) {
        this.parent = parent;
        autoRotation = new ArrayList<>();
        this.style = new CSSObject<>(this);
    }

    /**
     * What part of the string the given position is anchored to. Can be one of "left", "center" or "right". Defaults to
     * an intelligent guess based on which side of the chart the axis is on and the rotation of the label.
     *
     * <b>Default: </b> {@link Alignment#CENTER}.
     */
    public AxisLabels<T> setAlign(Alignment align) {
        this.align = align;
        return this;
    }

    /**
     * For horizontal axes, the allowed degrees of label rotation to prevent overlapping labels. If there is enough
     * space, labels are not rotated. As the chart gets narrower, it will start rotating the labels -45 degrees,
     * then remove every second label and try again with rotations 0 and -45 etc. Set it to false to disable rotation,
     * which will cause the labels to word-wrap if possible.
     * <p>
     * <b>Default: </b> [-45].
     */
    public AxisLabels<T> addAutoRotation(Number rotation) {
        autoRotation.add(rotation);
        return this;
    }

    /**
     * When each category width is more than this many pixels, we don't apply auto rotation. Instead, we lay out the
     * axis label with word wrap. A lower limit makes sense when the label contains multiple short words that don't
     * extend the available horizontal space for each label.
     * <p>
     * <b>Default:</b> 80.
     */
    public AxisLabels<T> setAutoRotationLimit(Number autoRotationLimit) {
        this.autoRotationLimit = autoRotationLimit;
        return this;
    }

    /**
     * Polar charts only. The label's pixel distance from the perimeter of the plot area.
     * <p>
     * <b>Default:</b> 15.
     */
    public AxisLabels<T> setDistance(Number distance) {
        this.distance = distance;
        return this;
    }

    /**
     * Enable or disable the axis labels.
     * <p>
     * <b>Default:</b> true.
     */
    public AxisLabels<T> setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    /**
     * A <a href="http://www.highcharts.com/docs/chart-concepts/labels-and-string-formatting">format string</a>
     * for the axis label.
     * <p>
     * <b>Default:</b> {value}.
     */
    public AxisLabels<T> setFormat(String format) {
        this.format = format;
        return this;
    }

    /**
     * Callback JavaScript function to format the label. The value is given by this.value. Additional properties for this are axis, chart, isFirst and isLast
     */
    public AxisLabels<T> setFormatter(Function.Formatter formatter) {
        this.formatter = formatter;
        return this;
    }

    /**
     * The pixel padding for axis labels, to ensure white space between them.
     * <p>
     * <b>Default:</b> 5.
     */
    public AxisLabels<T> setPadding(Number padding) {
        this.padding = padding;
        return this;
    }

    /**
     * Whether to reserve space for the labels. This can be turned off when for example the labels are rendered inside the plot area instead of outside.
     * <p>
     * <b>Default:</b> true
     */
    public AxisLabels<T> setReserveSpace(Boolean reserveSpace) {
        this.reserveSpace = reserveSpace;
        return this;
    }

    /**
     * Rotation of the labels in degrees.
     * <p>
     * <b>Default:</b> 0
     */
    public AxisLabels<T> setRotation(Number rotation) {
        this.rotation = rotation;
        return this;
    }

    /**
     * Horizontal axes only. The number of lines to spread the labels over to make room or tighter labels.
     */
    public AxisLabels<T> setStaggerLines(Number staggerLines) {
        this.staggerLines = staggerLines;
        return this;
    }

    /**
     * To show only every n'th label on the axis, set the step to n. Setting the step to 2 shows every other label.
     * <p>
     * By default, the step is calculated automatically to avoid overlap. To prevent this, set it to 1. This usually
     * only happens on a category axis, and is often a sign that you have chosen the wrong axis type.
     * @see <a href="http://www.highcharts.com/docs/chart-concepts/axes">Read more at Axis docs: What axis should I use?</a>
     */
    public AxisLabels<T> setStep(Number step) {
        this.step = step;
        return this;
    }

    /**
     * Whether to use HTML to render the labels.
     * <p>
     * <b>Default: </b> false
     */
    public AxisLabels<T> setUseHTML(Boolean useHTML) {
        this.useHTML = useHTML;
        return this;
    }

    /**
     * The x position offset of the label relative to the tick position on the axis
     * <p>
     * <b>Default: </b> 0
     */
    public AxisLabels<T> setX(Number x) {
        this.x = x;
        return this;
    }

    /**
     * The y position offset of the label relative to the tick position on the axis. The default makes it
     * adapt to the font size on bottom axis.
     * <p>
     * <b>Default:</b> null
     */
    public AxisLabels<T> setY(Number y) {
        this.y = y;
        return this;
    }

    /**
     * The Z index for the axis labels.
     * <p>
     * <b>Default:</b> 7
     */
    public AxisLabels<T> setzIndex(Number zIndex) {
        this.zIndex = zIndex;
        return this;
    }

    @Override
    public T build() {
        return parent;
    }

    /**
     * CSS styles for the label. Use whiteSpace: 'nowrap' to prevent wrapping of category labels.
     * Use textOverflow: 'none' to prevent ellipsis (dots).
     */
    public CSSObject<AxisLabels<T>> getStyle() {
        return style;
    }

}
