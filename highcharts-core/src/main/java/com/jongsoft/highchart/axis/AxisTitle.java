/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.axis;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Renderable;
import com.jongsoft.highchart.common.Title;

public class AxisTitle<T> extends Title<AxisTitle<T>, T> implements Renderable {

    @JsonProperty private Number margin;
    @JsonProperty private Number offset;
    @JsonProperty private Number rotation;

    public AxisTitle(T parent) {
        super(parent);
    }

    /**
     * The pixel distance between the axis labels or line and the title.
     * <p>
     * <b>Default:</b> 0 for horizontal axes, 10 for vertical
     */
    public void setMargin(Number margin) {
        this.margin = margin;
    }

    /**
     * The distance of the axis title from the axis line. By default, this distance is computed from the offset width of the labels, the labels' distance from
     * the axis and the title's margin. However when the offset option is set, it overrides all this.
     */
    public void setOffset(Number offset) {
        this.offset = offset;
    }

    /**
     * The rotation of the text in degrees. 0 is horizontal, 270 is vertical reading from bottom to top.
     * <p>
     * <b>Default:</b> 0.
     */
    public AxisTitle<T> setRotation(Number rotation) {
        this.rotation = rotation;
        return self();
    }

    @Override
    protected AxisTitle<T> self() {
        return this;
    }

}
