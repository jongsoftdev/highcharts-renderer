/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.axis;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Renderable;

public class Break implements Renderable {

    @JsonProperty private Number breakSize;
    @JsonProperty private Number from;
    @JsonProperty private Number repeat;
    @JsonProperty private Number to;

    /**
     * A number indicating how much space should be left between the start and the end of the break. The break size
     * is given in axis units, so for instance on a datetime axis, a break size of 3600000 would indicate
     * the equivalent of an hour.
     */
    public Break setBreakSize(Number breakSize) {
        this.breakSize = breakSize;
        return this;
    }

    /**
     * The point where the break starts.
     */
    public Break setFrom(Number from) {
        this.from = from;
        return this;
    }

    /**
     * Defines an interval after which the break appears again. By default the breaks do not repeat
     */
    public Break setRepeat(Number repeat) {
        this.repeat = repeat;
        return this;
    }

    /**
     * The point where the break ends.
     */
    public Break setTo(Number to) {
        this.to = to;
        return this;
    }

}
