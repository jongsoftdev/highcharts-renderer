/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.axis;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.highchart.common.GraphColor;
import java.util.ArrayList;
import java.util.List;

/**
 * A color axis for choropleth mapping. Visually, the color axis will appear as a gradient or as separate items inside the legend, depending on whether the axis
 * is scalar or based on data classes.
 * <p>
 * For supported color formats, see the docs article about colors.
 * <p>
 * A scalar color axis is represented by a gradient. The colors either range between the minColor and the maxColor, or for more fine grained control the colors
 * can be defined in stops. Often times, the color axis needs to be adjusted to get the right color spread for the data. In addition to stops, consider using a
 * logarithmic axis type, or setting min and max to avoid the colors being determined by outliers.
 * <p>
 * When dataClasses are used, the ranges are subdivided into separate classes like categories based on their values. This can be used for ranges between two
 * values, but also for a true category. However, when your data is categorized, it may be as convenient to add each category to a separate series.
 */
public class ColorAxis extends Axis {

    public static class ColorStop {

        private final Number percentage;
        private final GraphColor color;

        public ColorStop(Number percentage, GraphColor color) {
            this.percentage = percentage;
            this.color = color;
        }

        public GraphColor getColor() {
            return color;
        }

        public Number getPercentage() {
            return percentage;
        }

    }

    @JsonProperty private String dataClassColor;
    @JsonProperty private List<ColorStop> stops;

    /**
     * Determines how to set each data class' color if no individual color is set. The default value, tween, computes intermediate colors between minColor and
     * maxColor. The other possible value, category, pulls colors from the global or chart specific colors array.
     * <p>
     * <b>Default:</b> tween
     */
    public ColorAxis setDataClassColor(String dataClassColor) {
        this.dataClassColor = dataClassColor;
        return this;
    }

    /**
     * Color stops for the gradient of a scalar color axis. Use this in cases where a linear gradient between a minColor and maxColor is not sufficient. The
     * stops is an array of tuples, where the first item is a float between 0 and 1 assigning the relative position in the gradient, and the second item is the
     * color.
     */
    public ColorAxis addStop(ColorStop stop) {
        if (stops == null) {
            stops = new ArrayList<>();
        }
        stops.add(stop);
        return this;
    }

}
