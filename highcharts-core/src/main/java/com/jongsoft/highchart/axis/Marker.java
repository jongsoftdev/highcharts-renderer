/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.axis;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.util.Builder;
import com.jongsoft.util.IsEmpty;
import com.jongsoft.highchart.common.GraphColor;

public class Marker<T> implements Builder<T>, IsEmpty {

    public enum MarkerSymbol {

        CIRCLE("circle"), SQUARE("square"), DIAMOND("diamond"), TRIANGLE("triangle"), TRIANGLE_DOWN("triangle-down"), RECTANGLE("rect");

        private final String type;

        private MarkerSymbol(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return type;
        }
    }

    @JsonProperty private Boolean enabled;
    @JsonProperty private GraphColor fillColor;
    @JsonProperty private GraphColor color;
    @JsonProperty private Number height;
    @JsonProperty private GraphColor lineColor;
    @JsonProperty private Number lineWidth;
    @JsonProperty private Number radius;
    @JsonProperty private MarkerSymbol symbol;
    @JsonProperty private Number width;
    private final T parent;

    public Marker(T parent) {
        this.parent = parent;
    }

    /**
     * Enable or disable the point marker. If null, the markers are hidden when the data is dense, and shown for more widespread data points.
     * <p>
     * <b>Default</b> null
     */
    public Marker<T> setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    /**
     * The fill color of the point marker. When null, the series' or point's color is used.
     */
    public Marker<T> setColor(GraphColor color) {
        this.color = color;
        return this;
    }

    /**
     * The fill color of the point marker. When null, the series' or point's color is used.
     */
    public Marker<T> setFillColor(GraphColor fillColor) {
        this.fillColor = fillColor;
        return this;
    }

    /**
     * Image markers only. Set the image width explicitly. When using this option, a width must also be set.
     * <p>
     * <b>Default: </b> null
     */
    public Marker<T> setHeight(Number height) {
        this.height = height;
        return this;
    }

    /**
     * The color of the point marker's outline. When null, the series' or point's color is used.
     * <p>
     * <b>Default:</b> #FFFFFF
     */
    public Marker<T> setLineColor(GraphColor lineColor) {
        this.lineColor = lineColor;
        return this;
    }

    /**
     * The width of the point marker's outline.
     * <p>
     * <b>Default:</b> 0
     */
    public Marker<T> setLineWidth(Number lineWidth) {
        this.lineWidth = lineWidth;
        return this;
    }

    /**
     * The radius of the point marker.
     * <p>
     * <b>Default:</b> 4
     */
    public Marker<T> setRadius(Number radius) {
        this.radius = radius;
        return this;
    }

    /**
     * A predefined shape or symbol for the marker. When null, the symbol is pulled from options.symbols.
     */
    public Marker<T> setSymbol(MarkerSymbol symbol) {
        this.symbol = symbol;
        return this;
    }

    /**
     * Image markers only. Set the image width explicitly. When using this option, a height must also be set.
     * <p>
     * <b>Default:</b> null
     */
    public void setWidth(Number width) {
        this.width = width;
    }

    @Override
    public T build() {
        return parent;
    }

    @Override
    public boolean isEmpty() {
        return IsEmpty.isEmpty(this.color, this.enabled, this.fillColor, this.height,
                this.lineColor, this.lineWidth, this.radius, this.symbol, this.width);
    }

}
