/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.axis;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Renderable;
import com.jongsoft.highchart.common.*;

public class PlotLine implements Renderable {

    public class PlotLineLabel {

        @JsonProperty private Alignment align;
        @JsonProperty private Number rotation;
        @JsonProperty private CSSObject<PlotLineLabel> style;
        @JsonProperty private String text;
        @JsonProperty private Alignment textAlign;
        @JsonProperty private Boolean useHTML;
        @JsonProperty private VerticalAlignment verticalAlign;
        @JsonProperty private Number x;
        @JsonProperty private Number y;

        /**
         * Horizontal alignment of the label. Can be one of "left", "center" or "right".
         * <p>
         * <b>Default:</b> {@link Alignment#LEFT}
         */
        public PlotLineLabel setAlign(Alignment align) {
            this.align = align;
            return this;
        }

        /**
         * Rotation of the text label in degrees. Defaults to 0 for horizontal plot lines and 90 for vertical lines.
         */
        public PlotLineLabel setRotation(Number rotation) {
            this.rotation = rotation;
            return this;
        }

        /**
         * The text itself. A subset of HTML is supported.
         */
        public PlotLineLabel setText(String text) {
            this.text = text;
            return this;
        }

        /**
         * The text alignment for the label. While align determines where the texts anchor point is placed within the plot band, textAlign determines how the
         * text is aligned against its anchor point. Possible values are "left", "center" and "right".
         * <p>
         * <b>Default:</b> to the same as {@link #setAlign(Alignment)}}
         */
        public PlotLineLabel setTextAlign(Alignment textAlign) {
            this.textAlign = textAlign;
            return this;
        }

        /**
         * Whether to use HTML to render the labels.
         * <p>
         * <b>Default:</b> false
         */
        public PlotLineLabel setUseHTML(Boolean useHTML) {
            this.useHTML = useHTML;
            return this;
        }

        /**
         * Vertical alignment of the label relative to the plot band.
         * <p>
         * <b>Default:</b> {@link VerticalAlignment#TOP}
         */
        public PlotLineLabel setVerticalAlign(VerticalAlignment verticalAlign) {
            this.verticalAlign = verticalAlign;
            return this;
        }

        /**
         * Horizontal position relative the alignment. Default varies by orientation.
         */
        public PlotLineLabel setX(Number x) {
            this.x = x;
            return this;
        }

        /**
         * Vertical position of the text baseline relative to the alignment. Default varies by orientation.
         */
        public PlotLineLabel setY(Number y) {
            this.y = y;
            return this;
        }

        public CSSObject<PlotLineLabel> getStyle() {
            if (style == null) {
                style = new CSSObject<>(this);
            }
            return style;
        }

    }

    @JsonProperty private GraphColor color;
    @JsonProperty private String className;
    @JsonProperty private DashStyle dashStyle;
    @JsonProperty private String id;
    @JsonProperty private PlotLineLabel label;
    @JsonProperty private Number value;
    @JsonProperty private Number width;
    @JsonProperty private Number zIndex;

    /**
     * The color of the line.
     */
    public PlotLine setColor(GraphColor color) {
        this.color = color;
        return this;
    }

    /**
     * @since 5.0.0
     */
    public PlotLine setClassName(String className) {
        this.className = className;
        return this;
    }

    /**
     * The dashing or dot style for the plot line
     */
    public PlotLine setDashStyle(DashStyle dashStyle) {
        this.dashStyle = dashStyle;
        return this;
    }

    /**
     * An id used for identifying the plot line in Axis.removePlotLine.
     */
    public PlotLine setId(String id) {
        this.id = id;
        return this;
    }

    /**
     * The position of the line in axis units
     */
    public PlotLine setValue(Number value) {
        this.value = value;
        return this;
    }

    /**
     * The width or thickness of the plot line.
     */
    public PlotLine setWidth(Number width) {
        this.width = width;
        return this;
    }

    /**
     * The z index of the plot line within the chart.
     */
    public PlotLine setZIndex(Number zIndex) {
        this.zIndex = zIndex;
        return this;
    }

    /**
     * Text labels for the plot bands
     */
    public PlotLineLabel getLabel() {
        if (label == null) {
            label = new PlotLineLabel();
        }
        return label;
    }

}
