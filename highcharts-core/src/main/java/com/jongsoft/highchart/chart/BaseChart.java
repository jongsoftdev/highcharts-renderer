/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.chart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Extensions;
import com.jongsoft.HashSetBuilder;
import com.jongsoft.HighchartModules;
import com.jongsoft.Renderable;
import com.jongsoft.highchart.chart.style.Margin;
import com.jongsoft.highchart.common.CSSObject;
import com.jongsoft.highchart.common.GraphColor;
import com.jongsoft.highchart.common.SeriesType;
import com.jongsoft.highchart.common.Styled;

import java.util.Set;

public abstract class BaseChart<T extends BaseChart<T, P>, P> extends Styled<T> implements Extensions, Renderable {

    @JsonProperty private Boolean alignTicks;
    @JsonProperty private Boolean animation;
    @JsonProperty private Boolean ignoreHiddenSeries;
    @JsonProperty private Boolean inverted;
    @JsonProperty private Number height;
    @JsonProperty private Number width;
    @JsonProperty private ZoomType pinchType;
    @JsonProperty private ZoomType zoomType;
    @JsonProperty private Margin margin;
    @JsonProperty private Margin spacing;

    @JsonProperty private GraphColor plotBackgroundColor;
    @JsonProperty private String plotBackgroundImage;
    @JsonProperty private GraphColor plotBorderColor;
    @JsonProperty private String plotBorderWidth;
    @JsonProperty private Boolean plotShadow;

    @JsonProperty private ChartEvents<T> events;

    @JsonProperty private Boolean polar;
    @JsonProperty private Boolean reflow;
    @JsonProperty private Boolean shadow;
    @JsonProperty private Boolean showAxis;
    @JsonProperty private CSSObject<T> style;

    @JsonProperty private SeriesType type;
    @JsonProperty private String selectionMarkerFill;
    @JsonProperty private String renderTo;

    private final P parent;

    public BaseChart(P parent) {
        this.parent = parent;
        this.events = new ChartEvents<>(self());
    }

    public T setAlignTicks(Boolean alignTicks) {
        this.alignTicks = alignTicks;
        return self();
    }

    public T setAnimation(Boolean animation) {
        this.animation = animation;
        return self();
    }

    public T setRenderTo(String renderTo) {
        this.renderTo = renderTo;
        return self();
    }

    /**
     * An explicit height for the chart. By default the height is calculated from the offset height of the containing element,
     * or 400 pixels if the containing element's height is 0.
     */
    public T setHeight(Number height) {
        this.height = height;
        return self();
    }

    public T setWidth(Number width) {
        this.width = width;
        return self();
    }

    /**
     * If true, the axes will scale to the remaining visible series once one series is hidden. If false, hiding and showing
     * a series will not affect the axes or the other series. For stacks, once one series within the stack is hidden, the rest of
     * the stack will close in around it even if the axis is not affected.
     * <p>
     * <b>Default:</b>  true.
     *
     * @since 1.2.0
     */
    public T setIgnoreHiddenSeries(Boolean ignoreHiddenSeries) {
        this.ignoreHiddenSeries = ignoreHiddenSeries;
        return self();
    }

    /**
     * Whether to invert the axes so that the x axis is vertical and y axis is horizontal. When true, the x axis is
     * reversed by default. If a bar series is present in the chart, it will be inverted automatically.
     * <p>
     * <b>Default:</b>  false.
     */
    public T setInverted(Boolean inverted) {
        this.inverted = inverted;
        return self();
    }

    /**
     * The margin between the outer edge of the chart and the plot area. The numbers in the array designate top, right, bottom and left respectively.
     * Use the options marginTop, marginRight, marginBottom and marginLeft for shorthand setting of one option.
     * <p>
     *     Since version 2.1, the margin is 0 by default. The actual space is dynamically calculated from the offset of axis labels,
     *     axis title, title, subtitle and legend in addition to the spacingTop, spacingRight, spacingBottom and spacingLeft options.
     * </p>
     * <b>Default:</b>  [null].
     */
    public T setMargin(Margin margin) {
        this.margin = margin;
        return self();
    }

    /**
     * Equivalent to zoomType, but for multi touch gestures only. By default, the pinchType is the same as the
     * {@link #setZoomType(ZoomType)} setting. However, pinching can be enabled separately in some cases,
     * for example in stock charts where a mouse drag pans the chart, while pinching is enabled.
     * <br />
     * <b>Default:</b>  null.
     *
     * @since 3.0
     */
    public T setPinchType(ZoomType pinchType) {
        this.pinchType = pinchType;
        return self();
    }

    /**
     * The background color or gradient for the plot area.
     */
    public T setPlotBackgroundColor(GraphColor plotBackgroundColor) {
        this.plotBackgroundColor = plotBackgroundColor;
        return self();
    }

    /**
     * The URL for an image to use as the plot background. To set an image as the background for the entire chart,
     * set a CSS background image to the container element. Note that for the image to be applied to exported charts,
     * its URL needs to be accessible by the export server.
     */
    public T setPlotBackgroundImage(String plotBackgroundImage) {
        this.plotBackgroundImage = plotBackgroundImage;
        return self();
    }

    /**
     * The color of the inner chart or plot area border.
     * <p>
     * <b>Default:</b> #C0C0C0.
     */
    public T setPlotBorderColor(GraphColor plotBorderColor) {
        this.plotBorderColor = plotBorderColor;
        return self();
    }

    /**
     * The pixel width of the plot area border.
     * <p>
     * <b>Default:</b> 0.
     */
    public T setPlotBorderWidth(String plotBorderWidth) {
        this.plotBorderWidth = plotBorderWidth;
        return self();
    }

    /**
     * Whether to apply a drop shadow to the plot area. Requires that plotBackgroundColor be set.
     * Since 2.3 the shadow can be an object configuration containing color, offsetX, offsetY, opacity and width.
     * <b>Default:</b> false.
     */
    public T setPlotShadow(Boolean plotShadow) {
        this.plotShadow = plotShadow;
        return self();
    }

    /**
     * When true, cartesian charts like line, spline, area and column are transformed into the polar coordinate system.
     * Requires highcharts-more.js.
     *
     * <b>Default:</b> false.
     *
     * @since 2.3.0
     */
    public T setPolar(Boolean polar) {
        this.polar = polar;
        return self();
    }

    /**
     * Whether to reflow the chart to fit the width of the container div on resizing the window.
     *
     * <b>Default:</b> true.
     *
     * @since 2.1
     */
    public T setReflow(Boolean reflow) {
        this.reflow = reflow;
        return self();
    }

    /**
     * The background color of the marker square when selecting (zooming in on) an area of the chart.
     *
     * <b>Default:</b> rgba(69,114,167,0.25).
     *
     * @since 2.1.7
     */
    public T setSelectionMarkerFill(String selectionMarkerFill) {
        this.selectionMarkerFill = selectionMarkerFill;
        return self();
    }

    /**
     * Whether to apply a drop shadow to the outer chart area. Requires that backgroundColor be set.
     *
     * <b>Default:</b> false.
     */
    public T setShadow(Boolean shadow) {
        this.shadow = shadow;
        return self();
    }

    /**
     * Whether to show the axes initially. This only applies to empty charts where series are added dynamically,
     * as axes are automatically added to cartesian series.
     *
     * <b>Default:</b> false.
     *
     * @since 1.2.5
     */
    public T setShowAxis(Boolean showAxis) {
        this.showAxis = showAxis;
        return self();
    }

    /**
     * The distance between the outer edge of the chart and the content, like title, legend, axis title or labels.
     * The numbers in the array designate top, right, bottom and left respectively.
     * <br/>
     * <b>Default:</b>  [10, 10, 15, 10].
     *
     * @since 3.0.6
     * @see #setMargin(Margin)
     */
    public T setSpacing(Margin spacing) {
        this.spacing = spacing;
        return self();
    }

    /**
     * The default series type for the chart. Can be any of the chart types listed under plotOptions.
     *
     * <b>Default:</b> line.
     *
     * @since 2.1.0
     */
    public T setType(SeriesType type) {
        this.type = type;
        return self();
    }

    /**
     * Decides in what dimensions the user can zoom by dragging the mouse. Can be one of x, y or xy.
     */
    public T setZoomType(ZoomType zoomType) {
        this.zoomType = zoomType;
        return self();
    }

    @Override
    public Set<HighchartModules> neededModules() {
        return new HashSetBuilder<HighchartModules>()
                .add(events.neededModules())
                .build();
    }

    public ChartEvents<T> getEvents() {
        return events;
    }

    public CSSObject<T> getStyle() {
        if (style == null) {
            style = new CSSObject<>(self());
        }
        return style;
    }

    public P build() {
        return parent;
    }

}
