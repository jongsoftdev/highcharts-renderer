/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.chart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.highchart.chart.style.Options3D;

/**
 * Options to render charts in 3 dimensions. This feature requires highcharts-3d.js,
 * found in the download package or online at code.highcharts.com/highcharts-3d.js.
 */
public class Chart3D<P> extends BaseChart<Chart3D<P>, P> {

    @JsonProperty private Options3D options3d;

    public Chart3D(P parent) {
        super(parent);
        this.options3d = new Options3D();
    }

    /**
     * Options to render charts in 3 dimensions. This feature requires highcharts-3d.js, found in the download
     * package or online at code.highcharts.com/highcharts-3d.js.
     * @since 4.0
     */
    public Options3D getOptions3D() {
        return options3d;
    }

    @Override
    protected Chart3D<P> self() {
        return this;
    }
}
