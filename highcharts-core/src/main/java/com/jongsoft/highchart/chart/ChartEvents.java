/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.chart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.util.Builder;
import com.jongsoft.HashSetBuilder;
import com.jongsoft.HighchartModules;
import com.jongsoft.util.IsEmpty;
import com.jongsoft.highchart.common.Function;

import java.util.Set;

/**
 * Event listeners for the chart.
 */
public class ChartEvents<T> implements Builder<T>, IsEmpty {

    private final T parent;

    @JsonProperty private Function addSeries;
    @JsonProperty private Function afterPrint;
    @JsonProperty private Function beforePrint;
    @JsonProperty private Function click;
    @JsonProperty private Function drilldown;
    @JsonProperty private Function drillup;
    @JsonProperty private Function drillupall;
    @JsonProperty private Function load;
    @JsonProperty private Function redraw;
    @JsonProperty private Function selection;

    public ChartEvents(T parent) {
        this.parent = parent;
    }

    /**
     * Fires when a series is added to the chart after load time, using the addSeries method.
     * <p>
     * One parameter, event, is passed to the function. This contains common event information based
     * on <code>jQuery</code> or <code>MooTools</code> depending on which library is used as the base for Highcharts.
     * <p>
     * Through event.options you can access the series options that was passed to the addSeries method.
     * <p>
     * Returning <code>false</code> prevents the series from being added.
     * <p>
     * <b>Note:</b> The this keyword refers to the {@link Chart} object.
     *
     * @since 1.2.0
     */
    public void setAddSeries(Function.Event addSeries) {
        this.addSeries = addSeries;
    }

    /**
     * Fires after a chart is printed through the context menu item or the <code>Chart.print</code> method.
     *
     * @since 4.1.0
     */
    public void setAfterPrint(Function afterPrint) {
        this.afterPrint = afterPrint;
    }

    /**
     * Fires before a chart is printed through the context menu item or the Chart.print method.
     *
     * @since 4.1.0
     */
    public void setBeforePrint(Function beforePrint) {
        this.beforePrint = beforePrint;
    }

    /**
     * Fires when clicking on the plot background. One parameter, event, is passed to the function. This contains common
     * event information based on jQuery or MooTools depending on which library is used as the base for Highcharts.
     * <p>
     * Information on the clicked spot can be found through event.xAxis and event.yAxis, which are arrays containing the
     * axes of each dimension and each axis' value at the clicked spot. The primary axes are event.xAxis[0] and
     * event.yAxis[0]. Remember the unit of a datetime axis is milliseconds since 1970-01-01 00:00:00.
     * <p>
     * <pre>
     * click: function(e) {
     *  console.log(Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', e.xAxis[0].value), e.yAxis[0].value)
     * }
     * </pre>
     * <p>
     * <b>Note:</b> the this keyword refers to the Chart object.
     *
     * @since 1.2.0
     */
    public void setClick(Function.Event click) {
        this.click = click;
    }

    /**
     * Fires when a drilldown point is clicked, before the new series is added. Event arguments:
     * <ul>
     *     <li><code>category</code>, if a category label was clicked, which index</li>
     *     <li><code>point</code>, the originating point</li>
     *     <li><code>originalEvent</code>, the original browser event (usually click) that triggered the drilldown</li>
     *     <li><code>points</code>, if a category label was clicked, this array holds all points corresponding to the category</li>
     *     <li><code>seriesOptions</code>, options for the new series</li>
     * </ul>
     * This event is also utilized for async drilldown, where the seriesOptions are not added by option, but rather loaded async.
     * <p>
     * <b>Note:</b> the this keyword refers to the Chart object.
     *
     * @since 3.0.8
     */
    public void setDrilldown(Function.Event drilldown) {
        this.drilldown = drilldown;
    }

    /**
     * Fires when drilling up from a drilldown series.
     * <p>
     * <b>Note:</b> the this keyword refers to the Chart object.
     *
     * @since 3.0.8
     */
    public void setDrillup(Function drillup) {
        this.drillup = drillup;
    }

    /**
     * In a chart with multiple drilldown series, this event fires after all the series have been drilled up.
     * <p>
     * <b>Note:</b> the this keyword refers to the Chart object.
     *
     * @since 4.2.4
     */
    public void setDrillupall(Function drillupall) {
        this.drillupall = drillupall;
    }

    /**
     * Fires when the chart is finished loading. Since v4.2.2, it also waits for images to be loaded, for example from
     * point markers. One parameter, event, is passed to the function. This contains common event information based on
     * jQuery or MooTools depending on which library is used as the base for Highcharts.
     * <p>
     * There is also a second parameter to the chart constructor where a callback function can be passed to be executed
     * on chart.load.
     * <p>
     * The this keyword refers to the Chart object.
     * <p>
     */
    public void setLoad(Function load) {
        this.load = load;
    }

    /**
     * Fires when the chart is redrawn, either after a call to chart.redraw() or after an axis, series or point is
     * modified with the redraw option set to true. One parameter, event, is passed to the function.
     * This contains common event information based on jQuery or MooTools depending on which
     * library is used as the base for Highcharts.
     * <p>
     * The this keyword refers to the Chart object.
     *
     * @since 1.2.0
     */
    public void setRedraw(Function.Event redraw) {
        this.redraw = redraw;
    }

    /**
     * Fires when an area of the chart has been selected. Selection is enabled by setting the chart's zoomType.
     * One parameter, event, is passed to the function. This contains common event information based on jQuery or MooTools
     * depending on which library is used as the base for Highcharts.
     * <p>
     * The default action for the selection event is to zoom the chart to the selected area.
     * It can be prevented by calling <code>event.preventDefault()</code>.
     * <p>
     * Information on the selected area can be found through <code>event.xAxis</code> and <code>event.yAxis</code>, which are arrays containing
     * the axes of each dimension and each axis' min and max values. The primary axes are <code>event.xAxis[0]</code> and <code>event.yAxis[0]</code>.
     * Remember the unit of a datetime axis is milliseconds since 1970-01-01 00:00:00.
     *
     * <pre>
     *   selection: function(event) {
     *      // log the min and max of the primary, datetime x-axis
     *      console.log(
     *          Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
     *          Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
     *      );
     *
     *      // log the min and max of the y axis
     *      console.log(event.yAxis[0].min, event.yAxis[0].max);
     *   }
     * </p>
     */
    public void setSelection(Function.Event selection) {
        this.selection = selection;
    }

    public T build() {
        return parent;
    }

    @Override
    public boolean isEmpty() {
        return IsEmpty.isEmpty(addSeries, beforePrint, this.click, this.drilldown, this.drillup, this.drillupall,
                this.load, this.afterPrint);
    }

    @Override
    public Set<HighchartModules> neededModules() {
        return new HashSetBuilder<HighchartModules>()
                .add(afterPrint != null, HighchartModules.EXPORT)
                .add(beforePrint != null, HighchartModules.EXPORT)
                .build();
    }

}
