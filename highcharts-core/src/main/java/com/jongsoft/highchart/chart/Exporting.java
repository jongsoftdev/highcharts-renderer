/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.chart;

import com.fasterxml.jackson.annotation.*;
import com.jongsoft.highchart.common.Function;
import com.jongsoft.util.Builder;
import com.jongsoft.util.EmptyBuilder;
import com.jongsoft.util.IsEmpty;
import com.jongsoft.highchart.AbstractHighchart;

public class Exporting<T> implements Builder<T>, IsEmpty {

    public enum ExportType {
        SVG("image/svg+xml"), PNG("image/png"), JPEG("image/jpeg"), PDF("application/pdf");

        private String type;
        ExportType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return type;
        }
    }

    private T parent;

    @JsonProperty private Boolean allowHTML;
    @JsonProperty private Boolean enabled;
    @JsonProperty private Function error;
    @JsonProperty private Boolean fallbackToExportServer;
    @JsonProperty private String filename;
    @JsonProperty private String url;

    @JsonProperty private Number scale;
    @JsonProperty private Number sourceHeight;
    @JsonProperty private Number sourceWidth;
    @JsonProperty private ExportType type;

    @JsonProperty private AbstractHighchart chartOptions;

    public Exporting(T parent) {
        this.parent = parent;
    }

    /**
     * Experimental setting to allow HTML inside the chart (added through the useHTML options), directly in the exported image.
     * This allows you to preserve complicated HTML structures like tables or bi-directional text in exported charts.
     * <br/>
     * Disclaimer: The HTML is rendered in a foreignObject tag in the generated SVG. The official export server is based
     * on PhantomJS, which supports this, but other SVG clients, like Batik, does not support it. This also applies to downloaded
     * SVG that you want to open in a desktop client.
     *
     * @since 4.1.8
     */
    public Exporting<T> setAllowHTML(Boolean allowHTML) {
        this.allowHTML = allowHTML;
        return this;
    }

    /**
     * Whether to enable the exporting module. Disabling the module will hide the context button, but API methods will
     * still be available.
     * <p>
     * <b>Default:</b> true.
     * @since 2.0
     */
    public Exporting<T> setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    /**
     * Function to call if the offline-exporting module fails to export a chart on the client side, and {@link #setFallbackToExportServer(Boolean)}
     * is disabled. If left undefined, an exception is thrown instead.
     * <p>
     * <b>Default:</b> undefined.
     *
     * @since 5.0.0
     */
    public Exporting<T> setError(Function error) {
        this.error = error;
        return this;
    }

    /**
     * Whether or not to fall back to the export server if the offline-exporting module is unable to export the chart
     * on the client side.
     * <p>
     * <b>Default:</b> true.
     *
     * @since 4.1.8
     */
    public Exporting<T>  setFallbackToExportServer(Boolean fallbackToExportServer) {
        this.fallbackToExportServer = fallbackToExportServer;
        return this;
    }

    /**
     * The filename, without extension, to use for the exported chart.
     * <p>
     * <b>Default:</b> chart.
     */
    public Exporting<T> setFilename(String filename) {
        this.filename = filename;
        return this;
    }

    /**
     * Defines the scale or zoom factor for the exported image compared to the on-screen display. While for instance a
     * 600px wide chart may look good on a website, it will look bad in print. The default scale of 2 makes
     * this chart export to a 1200px PNG or JPG.
     * <p>
     * <b>Default:</b> 2.
     */
    public Exporting<T> setScale(Number scale) {
        this.scale = scale;
        return this;
    }

    /**
     * @see #setSourceWidth(Number) Analogous to setSourceWidth
     */
    public Exporting<T> setSourceHeight(Number sourceHeight) {
        this.sourceHeight = sourceHeight;
        return this;

    }

    /**
     * The width of the original chart when exported, unless an explicit {@link BaseChart#setWidth(Number)} is set.
     * The width exported raster image is then multiplied by scale.
     */
    public Exporting<T> setSourceWidth(Number sourceWidth) {
        this.sourceWidth = sourceWidth;
        return this;
    }

    /**
     * The URL for the server module converting the SVG string to an image format. By default this points to
     * Highslide Software's free web service.
     * <p>
     * <b>Default:</b> http://export.highcharts.com.
     */
    public Exporting<T> setUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     * Default MIME type for exporting if chart.exportChart() is called without specifying a type option.
     * <p>
     * Default value is set to {@link ExportType#SVG}
     */
    public void setType(ExportType type) {
        this.type = type;
    }

    /**
     * Additional chart options to be merged into an exported chart. For example, the exported chart can be given a
     * specific width and height, or a printer-friendly color scheme.
     * <p>
     * <b>Default:</b> null.
     */
    public Exporting<T> setChartOptions(AbstractHighchart<?, ?> chartOptions) {
        this.chartOptions = chartOptions;
        return this;
    }

    @Override
    public T build() {
        return parent;
    }

    @Override
    public boolean isEmpty() {
        return new EmptyBuilder()
                .add(this.enabled)
                .build();
    }
}
