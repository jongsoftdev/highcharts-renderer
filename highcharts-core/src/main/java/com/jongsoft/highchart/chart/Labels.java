/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.chart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.util.Builder;
import com.jongsoft.highchart.common.CSSObject;
import com.jongsoft.util.IsEmpty;

import java.util.ArrayList;
import java.util.List;

public class Labels<T> implements Builder<T>, IsEmpty {

    private class Item {
        @JsonProperty private String html;

        public Item(String html) {
            this.html = html;
        }
    }

    private T parent;
    @JsonProperty private List<Item> items;
    @JsonProperty private CSSObject<Labels<T>> style;

    public Labels(T parent) {
        this.parent = parent;
        items = new ArrayList<>();
        style = new CSSObject<>(this);
    }

    /**
     * Shared CSS styles for all labels.
     * <p>
     * <b>Default:</b>
     * <pre>
     *   style: {
     *      color: '#3E576F'
     *   }
     * </pre>
     */
    public CSSObject<Labels<T>> getStyle() {
        return style;
    }

    /**
     * A HTML label that can be positioned anywhere in the chart area.
     */
    public Labels<T> addItem(String html) {
        items.add(new Item(html));
        return this;
    }

    @Override
    public T build() {
        return parent;
    }

    @Override
    public boolean isEmpty() {
        return items.isEmpty();
    }
}
