/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.chart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.util.Builder;
import com.jongsoft.highchart.chart.style.Margin;
import com.jongsoft.highchart.common.Alignment;
import com.jongsoft.highchart.common.CSSObject;
import com.jongsoft.highchart.common.Styled;
import com.jongsoft.highchart.common.VerticalAlignment;

/**
 * The legend is a box containing a symbol and name for each series item or point item in the chart.
 */
public class Legend<T> extends Styled<Legend<T>> implements Builder<T> {

    public enum Layout {
        HORIZONTAL, VERTICAL;

        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }

    /**
     * A title to be added on top of the legend.
     */
    public class Title<Z> {
        @JsonProperty private String text;
        @JsonProperty private CSSObject<Title<Z>> style;
        private Z parent;

        public Title(Z parent) {
            this.parent = parent;
        }

        public CSSObject<Title<Z>> getStyle() {
            if (style == null) {
                style = new CSSObject<>(this);
            }
            return style;
        }

        /**
         * A text or HTML string for the title.
         *
         * <b>Default:</b> null.
         */
        public Title<Z> setText(String text) {
            this.text = text;
            return this;
        }

        public Z build() {
            return parent;
        }

    }

    private T parent;

    @JsonProperty private Alignment align;
    @JsonProperty private VerticalAlignment verticalAlign;
    @JsonProperty private Boolean enabled;
    @JsonProperty private Boolean floating;
    @JsonProperty private Boolean reversed;
    @JsonProperty private Boolean rtl;
    @JsonProperty private Boolean useHTML;

    @JsonProperty private Number itemDistance;
    @JsonProperty private Number itemMarginBottom;
    @JsonProperty private Number itemMarginTop;
    @JsonProperty private Number itemWidth;

    @JsonProperty private Number margin;
    @JsonProperty private Number width;
    @JsonProperty private Number maxHeight;
    @JsonProperty private Number padding;
    @JsonProperty private Number symbolHeight;
    @JsonProperty private Number symbolWidth;
    @JsonProperty private Number symbolPadding;
    @JsonProperty private Number symbolRadius;
    @JsonProperty private Number x;
    @JsonProperty private Number y;

    @JsonProperty private String labelFormat;
    @JsonProperty private Layout layout;

    @JsonProperty private CSSObject<Legend<T>> itemHiddenStyle;
    @JsonProperty private CSSObject<Legend<T>> itemHoverStyle;
    @JsonProperty private CSSObject<Legend<T>> itemStyle;
    @JsonProperty private Title<Legend<T>> title;

    public Legend(T parent) {
        this.parent = parent;
    }

    /**
     * In the case that the legend is aligned in a corner position, the layout option will determine whether to place it
     * above/below or on the side of the plot area.
     *
     * <b>Default:</b> {@link Alignment#CENTER}
     */
    public Legend<T> setAlign(Alignment align) {
        this.align = align;
        return self();
    }

    /**
     * Enable or disable the legend.
     *
     * <b>Default:</b> true.
     */
    public Legend<T> setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return self();
    }

    /**
     * When the legend is floating, the plot area ignores it and is allowed to be placed below it.
     *
     * <b>Default:</b> false.
     *
     * @since 2.1
     */
    public Legend<T> setFloating(Boolean floating) {
        this.floating = floating;
        return self();
    }

    /**
     * In a legend with horizontal layout, the itemDistance defines the pixel distance between each item.
     *
     * <b>Default:</b> 20.
     *
     * @since 3.0.3
     */
    public Legend<T> setItemDistance(Number itemDistance) {
        this.itemDistance = itemDistance;
        return self();
    }

    /**
     * The pixel bottom margin for each legend item.
     *
     * <b>Default:</b> 0.
     */
    public Legend<T> setItemMarginBottom(Number itemMarginBottom) {
        this.itemMarginBottom = itemMarginBottom;
        return self();
    }

    /**
     * The pixel top margin for each legend item.
     *
     * <b>Default:</b> 0.
     */
    public Legend<T> setItemMarginTop(Number itemMarginTop) {
        this.itemMarginTop = itemMarginTop;
        return self();
    }

    /**
     * The width for each legend item. This is useful in a horizontal layout with many items when you
     * want the items to align vertically
     */
    public Legend<T> setItemWidth(Number itemWidth) {
        this.itemWidth = itemWidth;
        return self();
    }

    /**
     * A format string for each legend label. Available variables relates to properties on the series,
     * or the point in case of pies. <b>Default:</b> {name}.
     */
    public Legend<T> setLabelFormat(String labelFormat) {
        this.labelFormat = labelFormat;
        return self();
    }

    /**
     * The layout of the legend items. Can be one of "horizontal" or "vertical".
     *
     * <b>Default:</b> horizontal.
     */
    public Legend<T> setLayout(Layout layout) {
        this.layout = layout;
        return self();
    }

    /**
     * If the plot area sized is calculated automatically and the legend is not floating, the legend margin is the space
     * between the legend and the axis labels or plot area.
     * <br/>
     * <b>Default:</b> 12.
     */
    public Legend<T> setMargin(Number margin) {
        this.margin = margin;
        return self();
    }

    /**
     * Maximum pixel height for the legend. When the maximum height is extended, navigation will show.
     *
     * @since 2.3.0
     */
    public Legend<T> setMaxHeight(Number maxHeight) {
        this.maxHeight = maxHeight;
        return self();
    }

    /**
     * The inner padding of the legend box.
     *
     * <b>Default:</b> 8.
     */
    public Legend<T> setPadding(Number padding) {
        this.padding = padding;
        return self();
    }

    /**
     * Whether to reverse the order of the legend items compared to the order of the series or points as defined in
     * the configuration object.
     *
     * <b>Default:</b> false.
     */
    public Legend<T> setReversed(Boolean inversed) {
        this.reversed = inversed;
        return self();
    }

    /**
     * Whether to show the symbol on the right side of the text rather than the left side. This is common
     * in Arabic and Hebraic.
     *
     * <b>Default:</b> false.
     */
    public Legend<T> setRtl(Boolean rtl) {
        this.rtl = rtl;
        return self();
    }

    /**
     * The pixel height of the symbol for series types that use a rectangle in the legend. Defaults
     * to the font size of legend items.
     */
    public Legend<T> setSymbolHeight(Number symbolHeight) {
        this.symbolHeight = symbolHeight;
        return self();
    }

    /**
     * The pixel padding between the legend item symbol and the legend item text.
     *
     * <b>Default:</b> 5.
     */
    public Legend<T> setSymbolPadding(Number symbolPadding) {
        this.symbolPadding = symbolPadding;
        return self();
    }

    /**
     * The border radius of the symbol for series types that use a rectangle in the legend.
     *
     * <b>Default:</b> 0.
     */
    public Legend<T> setSymbolRadius(Number symbolRadius) {
        this.symbolRadius = symbolRadius;
        return self();
    }

    /**
     * The pixel width of the legend item symbol.
     *
     * <b>Default:</b> 16.
     */
    public Legend<T> setSymbolWidth(Number symbolWidth) {
        this.symbolWidth = symbolWidth;
        return self();
    }

    /**
     * Whether to use HTML to render the legend item texts. Prior to 4.1.7, when using HTML,
     * {@link #getNavigation()} was disabled.
     */
    public Legend<T> setUseHTML(Boolean useHTML) {
        this.useHTML = useHTML;
        return self();
    }

    /**
     * The vertical alignment of the legend box. Can be one of top, middle or bottom. Vertical position can be further
     * determined by the y option.
     * <p>
     * In the case that the legend is aligned in a corner position, the layout option will determine whether
     * to place it above/below or on the side of the plot area.
     * <p>
     * <b>Default:</b> bottom.
     */
    public Legend<T> setVerticalAlign(VerticalAlignment verticalAlign) {
        this.verticalAlign = verticalAlign;
        return self();
    }

    /**
     * The width of the legend box.
     */
    public Legend<T> setWidth(Number width) {
        this.width = width;
        return self();
    }

    /**
     * The x offset of the legend relative to its horizontal alignment align within {@link Chart#setSpacing(Margin)}.
     * Negative x moves it to the left, positive x moves it to the right.
     *
     * <b>Default:</b> 0.
     */
    public Legend<T> setX(Number x) {
        this.x = x;
        return self();
    }

    /**
     * The vertical offset of the legend relative to it's vertical alignment verticalAlign within
     * {@link Chart#setSpacing(Margin)} bottom and top. Negative y moves it up, positive y moves it down.
     *
     * <b>Default:</b> 0.
     */
    public Legend<T> setY(Number y) {
        this.y = y;
        return self();
    }

    @Override
    public T build() {
        return parent;
    }

    @Override
    protected Legend self() {
        return this;
    }

    /**
     * CSS styles for each legend item when the corresponding series or point is hidden. Only a subset of CSS is
     * supported, notably those options related to text. Properties are inherited from style unless overridden here.
     */
    public CSSObject<Legend<T>> getItemHiddenStyle() {
        if (itemHiddenStyle == null) {
            itemHiddenStyle = new CSSObject<>(self());
        }
        return itemHiddenStyle;
    }

    /**
     * CSS styles for each legend item in hover mode. Only a subset of CSS is supported, notably those options related to
     * text. Properties are inherited from style unless overridden here.
     */
    public CSSObject<Legend<T>> getItemHoverStyle() {
        if (itemHoverStyle == null) {
            itemHoverStyle = new CSSObject<>(self());
        }
        return itemHoverStyle;
    }

    /**
     * CSS styles for each legend item. Only a subset of CSS is supported, notably those options related to text.
     */
    public CSSObject<Legend<T>> getItemStyle() {
        if (itemStyle == null) {
            itemStyle = new CSSObject<>(self());
        }
        return itemStyle;
    }

    /**
     * A title to be added on top of the legend.
     */
    public Title<Legend<T>> getTitle() {
        if (title == null) {
            title = new Title<>(self());
        }
        return title;
    }

}
