/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.chart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Renderable;
import com.jongsoft.highchart.common.GraphColor;
import com.jongsoft.highchart.common.ShapeType;

public class PaneBackground implements Renderable {
    @JsonProperty private String backgroundColor;
    @JsonProperty private ShapeType shape;
    @JsonProperty private Number innerWidth;
    @JsonProperty private Number outerWidth;
    @JsonProperty private Number borderWidth;
    @JsonProperty private String className;
    @JsonProperty private GraphColor borderColor;

    /**
     * The background color or gradient for the pane.
     */
    public PaneBackground setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    /**
     * The shape of the pane background. When {@link ShapeType#SOLID}, the background is circular. When {@link ShapeType#ARC},
     * the background extends only from the min to the max of the value axis.
     * <p>
     * <b>Default</b> {@link ShapeType#SOLID}
     */
    public PaneBackground setShape(ShapeType shape) {
        this.shape = shape;
        return this;
    }

    public PaneBackground setInnerWidth(Number innerWidth) {
        this.innerWidth = innerWidth;
        return this;
    }

    public PaneBackground setOuterWidth(Number outerWidth) {
        this.outerWidth = outerWidth;
        return this;
    }

    /**
     * The pixel border width of the pane background.
     * <p>
     * <b>Default:</b> 1
     */
    public PaneBackground setBorderWidth(Number borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    /**
     * The pane background border color.
     * <p>
     * <b>Default:</b> #cccccc
     */
    public PaneBackground setBorderColor(GraphColor borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    /**
     * The class name for this background.
     * <p>
     * <b>Default:</b> highcharts-pane.
     *
     * @since 5.0.0
     */
    public PaneBackground setClassName(String className) {
        this.className = className;
        return this;
    }

}
