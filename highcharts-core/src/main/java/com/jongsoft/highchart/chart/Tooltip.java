/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.chart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.util.Builder;
import com.jongsoft.highchart.common.CSSObject;
import com.jongsoft.highchart.common.Crosshair;
import com.jongsoft.highchart.common.Function;
import com.jongsoft.highchart.common.Function.Formatter;
import com.jongsoft.highchart.common.Function.Positioner;
import com.jongsoft.highchart.common.Styled;
import com.jongsoft.highchart.series.SeriesPoint;

public class Tooltip<T> extends Styled<Tooltip<T>> implements Builder<T> {

    private final T parent;

    @JsonProperty private Boolean animation;
    @JsonProperty private Crosshair<Tooltip<T>> crosshair;
    @JsonProperty private Boolean enabled;
    @JsonProperty private Boolean followPointer;
    @JsonProperty private Boolean followTouchMove;
    @JsonProperty private String footerFormat;
    @JsonProperty private Formatter formatter;
    @JsonProperty private String headerFormat;
    @JsonProperty private Number hideDelay;
    @JsonProperty private Number padding;
    @JsonProperty private String pointFormat;
    @JsonProperty private Function pointFormatter;
    @JsonProperty private Positioner positioner;
    @JsonProperty private Boolean shadow;
    @JsonProperty private String shape;
    @JsonProperty private Boolean shared;
    @JsonProperty private Number snap;
    @JsonProperty private CSSObject<Tooltip<T>> style;
    @JsonProperty private Boolean useHTML;
    @JsonProperty private Number valueDecimals;

    public Tooltip(T parent) {
        this.parent = parent;
        this.crosshair = new Crosshair<>(self());
        this.style = new CSSObject<>(self());
    }

    public Tooltip<T> setAnimation(Boolean animation) {
        this.animation = animation;
        return self();
    }

    /**
     * Enable or disable the tooltip.
     * <p>
     * <b>Default:</b> true.
     */
    public Tooltip<T> setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return self();
    }

    /**
     * Whether the tooltip should follow the mouse as it moves across columns, pie slices and other point types with an extent. By default it behaves this way
     * for scatter, bubble and pie series by override in the plotOptions for those series types.
     * <p>
     * <b>Note:</b> For touch moves to behave the same way, {@link #setFollowTouchMove(java.lang.Boolean) } must be true also.
     * <b>Default:</b> false
     */
    public Tooltip<T> setFollowPointer(Boolean followPointer) {
        this.followPointer = followPointer;
        return self();
    }

    /**
     * Whether the tooltip should follow the finger as it moves on a touch device. If chart.zoomType is set, it will override followTouchMove.
     * <p>
     * <b>Default</b> true
     */
    public Tooltip<T> setFollowTouchMove(Boolean followTouchMove) {
        this.followTouchMove = followTouchMove;
        return self();
    }

    /**
     * A string to append to the tooltip format
     */
    public Tooltip<T> setFooterFormat(String footerFormat) {
        this.footerFormat = footerFormat;
        return self();
    }

    /**
     * Callback function to format the text of the tooltip. Return false to disable tooltip for a specific point on series.
     * <p>
     * @see Formatter
     */
    public Tooltip<T> setFormatter(Formatter formatter) {
        this.formatter = formatter;
        return self();
    }

    /**
     * The HTML of the tooltip header line. Variables are enclosed by curly brackets. Available variables are point.key, series.name, series.color and other
     * members from the point and series objects. The point.key variable contains the category name, x value or datetime string depending on the type of axis.
     * For datetime axes, the point.key date format can be set using tooltip.xDateFormat.
     * <p>
     * <b>Default:</b> <code>&lt;span style="font-size: 10px"&gt;{point.key}&lt;/span&gt;</code>
     */
    public Tooltip<T> setHeaderFormat(String headerFormat) {
        this.headerFormat = headerFormat;
        return self();
    }

    /**
     * The number of milliseconds to wait until the tooltip is hidden when mouse out from a point or chart.
     * <p>
     * <b>Default</b> 500
     */
    public Tooltip<T> setHideDelay(Number hideDelay) {
        this.hideDelay = hideDelay;
        return self();
    }

    /**
     * Padding inside the tooltip, in pixels.
     * <p>
     * <b>Default:</b> 8
     * @since 5.0.0
     */
    public Tooltip<T> setPadding(Number padding) {
        this.padding = padding;
        return self();
    }

    /**
     * The HTML of the point's line in the tooltip. Variables are enclosed by curly brackets. Available variables are point.x, point.y, series.name and
     * series.color and other properties on the same form. Furthermore, point.y can be extended by the tooltip.valuePrefix and tooltip.valueSuffix variables.
     * This can also be overridden for each series, which makes it a good hook for displaying units.
     * <p>
     * <b>Default:</b> <code>&lt;span style="color:{point.color}"&gt;\u25CF&lt;/span&gt; {series.name}: &lt;b&gt;{point.y}&lt;/b&gt;&lt;br/&gt;</code>
     */
    public Tooltip<T> setPointFormat(String pointFormat) {
        this.pointFormat = pointFormat;
        return self();
    }

    /**
     * A callback function for formatting the HTML output for a single point in the tooltip. Like the pointFormat string, but with more flexibility.
     * <p>
     * The this keyword refers to the {@link SeriesPoint} object.
     */
    public Tooltip<T> setPointFormatter(Function pointFormatter) {
        this.pointFormatter = pointFormatter;
        return self();
    }

    /**
     * A callback function to place the tooltip in a default position. The callback receives three parameters: labelWidth, labelHeight and point, where point
     * contains values for plotX and plotY telling where the reference point is in the plot area. Add chart.plotLeft and chart.plotTop to get the full
     * coordinates.
     * <p>
     * The return should be an object containing x and y values, for example { x: 100, y: 100 }.
     */
    public Tooltip<T> setPositioner(Positioner positioner) {
        this.positioner = positioner;
        return self();
    }

    /**
     * Whether to apply a drop shadow to the tooltip.
     * <p>
     * <b>Default:</b> true
     */
    public Tooltip<T> setShadow(Boolean shadow) {
        this.shadow = shadow;
        return self();
    }

    /**
     * The name of a symbol to use for the border around the tooltip.
     * <p>
     * <b>Default:</b> callout
     */
    public Tooltip<T> setShape(String shape) {
        this.shape = shape;
        return self();
    }

    /**
     * When the tooltip is shared, the entire plot area will capture mouse movement or touch events. Tooltip texts for series types with ordered data (not pie,
     * scatter, flags etc) will be shown in a single bubble. This is recommended for single series charts and for tablet/mobile optimized charts.
     * <p>
     * <b>Default:</b> false.
     */
    public Tooltip<T> setShared(Boolean shared) {
        this.shared = shared;
        return self();
    }

    /**
     * Proximity snap for graphs or single points. Does not apply to bars, columns and pie slices. It defaults to 10 for mouse-powered devices and 25 for touch
     * devices.
     */
    public Tooltip<T> setSnap(Number snap) {
        this.snap = snap;
        return self();
    }

    /**
     * Use HTML to render the contents of the tooltip instead of SVG. Using HTML allows advanced formatting like tables and images in the tooltip. It is also
     * recommended for rtl languages as it works around rtl bugs in early Firefox.
     * <p>
     * <b>Default:</b> false.
     */
    public Tooltip<T> setUseHTML(Boolean useHTML) {
        this.useHTML = useHTML;
        return self();
    }

    /**
     * How many decimals to show in each series' y value. This is overridable in each series' tooltip options object. The default is to preserve all decimals.
     */
    public Tooltip<T> setValueDecimals(Number valueDecimals) {
        this.valueDecimals = valueDecimals;
        return self();
    }

    @Override
    public T build() {
        return parent;
    }

    @Override
    protected Tooltip<T> self() {
        return this;
    }

    public Crosshair<Tooltip<T>> getCrosshair() {
        return crosshair;
    }

    public CSSObject<Tooltip<T>> getStyle() {
        return style;
    }

}
