/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.chart.style;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Renderable;

public class Options3D implements Renderable {
    @JsonProperty private Number alpha;
    @JsonProperty private Number beta;
    @JsonProperty private Number depth;
    @JsonProperty private Number viewDistance;

    @JsonProperty private Boolean enabled;

    /**
     * One of the two rotation angles for the chart. Defaults to 0.
     */
    public Options3D setAlpha(Number alpha) {
        this.alpha = alpha;
        return this;
    }

    /**
     * One of the two rotation angles for the chart. Defaults to 0.
     */
    public Options3D setBeta(Number beta) {
        this.beta = beta;
        return this;
    }

    /**
     * The total depth of the chart. Defaults to 100.
     */
    public Options3D setDepth(Number depth) {
        this.depth = depth;
        return this;
    }

    /**
     * Wether to render the chart using the 3D functionality. Defaults to false.
     */
    public Options3D setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    /**
     * Defines the distance the viewer is standing in front of the chart, this setting is important to calculate
     * the perspective effect in column and scatter charts. It is not used for 3D pie charts. Defaults to 100.
     */
    public Options3D setViewDistance(Number viewDistance) {
        this.viewDistance = viewDistance;
        return this;
    }

}
