/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.util.Builder;
import com.jongsoft.util.EmptyBuilder;
import com.jongsoft.util.IsEmpty;

public class Credits<T> implements Builder<T>, IsEmpty {

    public class Position implements IsEmpty {
        @JsonProperty
        private Alignment align;
        @JsonProperty
        private String verticalAlign;
        @JsonProperty
        private Number x;
        @JsonProperty
        private Number y;

        private final Credits<T> parent;

        public Position(Credits<T> parent) {
            this.parent = parent;
        }

        public Position setAlign(Alignment align) {
            this.align = align;
            return this;
        }

        public Position setVerticalAlign(String verticalAlign) {
            this.verticalAlign = verticalAlign;
            return this;
        }

        public Position setX(Number x) {
            this.x = x;
            return this;
        }

        public Position setY(Number y) {
            this.y = y;
            return this;
        }

        public Credits<T> build() {
            return parent;
        }

        @Override
        public boolean isEmpty() {
            return new EmptyBuilder()
                    .add(this.y)
                    .add(this.align)
                    .add(this.x)
                    .add(this.verticalAlign)
                    .build();
        }
    }

    @JsonProperty
    private Boolean enabled;
    @JsonProperty
    private String href;
    @JsonProperty
    private String text;
    @JsonProperty
    private CSSObject<Credits<T>> style;
    @JsonProperty
    private Position position;

    private T parent;

    public Credits(T parent) {
        this.parent = parent;
        this.style = new CSSObject<>(this);
        this.position = new Position(this);
    }

    /**
     * Whether to show the credits text.
     * <p>
     * <b>Default</b>: true.
     */
    public Credits<T> setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    /**
     * The URL for the credits label.
     * <p>
     * <b>Default:</b> http://www.highcharts.com.
     */
    public Credits<T> setHref(String href) {
        this.href = href;
        return this;
    }

    /**
     * The text for the credits label.
     * <p>
     * <b>Default</b>: Highcharts.com.
     */
    public Credits<T> setText(String text) {
        this.text = text;
        return this;
    }

    @Override
    public T build() {
        return parent;
    }

    @Override
    public boolean isEmpty() {
        return new EmptyBuilder()
                .add(this.enabled)
                .build();
    }

    public CSSObject<Credits<T>> getStyle() {
        return style;
    }

    public Position getPosition() {
        return position;
    }
}
