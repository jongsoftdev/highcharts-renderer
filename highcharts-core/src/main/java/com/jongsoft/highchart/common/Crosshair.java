/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Renderable;
import com.jongsoft.util.Builder;

public class Crosshair<T> implements Builder<T>, Renderable {

    @JsonProperty private GraphColor color;
    @JsonProperty private String className;
    @JsonProperty private DashStyle dashStyle;
    @JsonProperty private Boolean snap;
    @JsonProperty private Number width;
    @JsonProperty private Number zIndex;
    private final T parent;

    public Crosshair(T parent) {
        this.parent = parent;
    }

    /**
     * The color of the crosshair. Defaults to #C0C0C0 for numeric and datetime axes, and rgba(155,200,255,0.2)
     * for category axes, where the crosshair by default highlights the whole category
     */
    public Crosshair<T> setColor(GraphColor color) {
        this.color = color;
        return this;
    }

    /**
     * A class name for the crosshair, especially as a hook for styling.
     * @since 5.0.0
     */
    public Crosshair<T> setClassName(String className) {
        this.className = className;
        return this;
    }

    /**
     * The dash style for the crosshair. See {@link DashStyle} for possible values.
     * <p>
     * <b>Default:</b> {@link DashStyle#SOLID}
     */
    public Crosshair<T> setDashStyle(DashStyle dashStyle) {
        this.dashStyle = dashStyle;
        return this;
    }

    /**
     * Whether the crosshair should snap to the point or follow the pointer independent of points.
     * <p>
     * <b>Default:</b> true.
     */
    public Crosshair<T> setSnap(Boolean snap) {
        this.snap = snap;
        return this;
    }

    /**
     * The pixel width of the crosshair. Defaults to 1 for numeric or datetime axes, and for one category width for category axes.
     */
    public Crosshair<T> setWidth(Number width) {
        this.width = width;
        return this;
    }

    /**
     * The Z index of the crosshair. Higher Z indices allow drawing the crosshair on top of the series or behind the grid lines.
     * <p>
     * <b>Default:</b> 2.
     */
    public Crosshair<T> setzIndex(Number zIndex) {
        this.zIndex = zIndex;
        return this;
    }

    @Override
    public T build() {
        return parent;
    }

}
