/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.common;

public enum DashStyle {
    SOLID("Solid"),
    SHORT_DASH("ShortDash"),
    SHORT_DOT("ShortDot"),
    SHORT_DASH_DOT("ShortDashDot"),
    SHORT_DASH_DOT_DOT("ShortDashDotDot"),
    DOT("Dot"),
    DASH("Dash"),
    LONG_DASH("LongDash"),
    DASH_DOT("DashDot"),
    LONG_DASH_DOT("LongDashDot"),
    LONG_DASH_DOT_DOT("LongDashDotDot");

    private final String type;
    private DashStyle(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
