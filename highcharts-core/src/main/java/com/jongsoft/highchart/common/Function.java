/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.common;

public class Function {

    private final String javaScript;
    private final String[] args;

    private Function(String javaScript, String...args) {
        this.args = args;
        this.javaScript = javaScript;
    }

    public Function(String javaScript) {
        this(javaScript, new String[0]);
    }

    @Override
    public String toString() {
        StringBuilder javaScriptFunction = new StringBuilder()
                .append("function (").append(String.join(", ", this.args)).append(") {").append(System.lineSeparator())
                .append("\t").append(javaScript).append(";").append(System.lineSeparator())
                .append("}");
        return javaScriptFunction.toString();
    }

    /**
     * Callback JavaScript function to format the data label. Available data are:
     * <ul>
     * <li><code>this.percentage</code>, Stacked series and pies only. The point's percentage of the total.</li>
     * <li><code>this.point</code>, The point object. The point name, if defined, is available through this.point.name.</li>
     * <li><code>this.series</code>, The series object. The series name is available through this.series.name.</li>
     * <li><code>this.total</code>, Stacked series only. The total value at this point's x value.</li>
     * <li><code>this.x</code>, The x value.</li>
     * <li><code>this.y</code>, The y value.</li>
     * </ul>
     */
    public static class Formatter extends Function {

        public Formatter(String javaScript) {
            super(javaScript);
        }
    }

    /**
     * One parameter, event, is passed to the function.
     */
    public static class Event extends Function {

        public Event(String javaScript) {
            super(javaScript, "event");
        }

    }

    /**
     * One parameter is passed to the function, being chart.
     */
    public static class Chart extends Function {

        public Chart(String javaScript) {
            super(javaScript, "chart");
        }

    }

    /**
     * One parameter is passed to the function, being point.
     */
    public static class Point extends Function {

        public Point(String javaScript) {
            super(javaScript, "point");
        }

    }

    /**
     * Three parameters are passed to the function, being:
     * <ul>
     *     <li>labelWidth</li>
     *     <li>labelHeight</li>
     *     <li>point</li>
     * </ul>
     */
    public static class Positioner extends Function {

        public Positioner(String javaScript) {
            super(javaScript, "labelWidth", "labelHeight", "point");
        }

    }

}
