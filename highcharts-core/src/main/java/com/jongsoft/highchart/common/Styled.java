/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Renderable;

public abstract class Styled<T> implements Renderable {

    @JsonProperty private GraphColor backgroundColor;
    @JsonProperty private GraphColor borderColor;
    @JsonProperty private String className;

    @JsonProperty private Number borderRadius;
    @JsonProperty private Number borderWidth;

    /**
     * The background color or gradient for the outer chart area.
     */
    public T setBackgroundColor(GraphColor backgroundColor) {
        this.backgroundColor = backgroundColor;
        return self();
    }

    /**
     * The color of the outer chart border.
     */
    public T setBorderColor(GraphColor borderColor) {
        this.borderColor = borderColor;
        return self();
    }

    /**
     * The corner radius of the outer chart border.
     * <p>
     * Defaults to 0.
     */
    public T setBorderRadius(Number borderRadius) {
        this.borderRadius = borderRadius;
        return self();
    }

    /**
     * The pixel width of the outer chart border.
     * <p>
     * Defaults to 0.
     */
    public T setBorderWidth(Number borderWidth) {
        this.borderWidth = borderWidth;
        return self();
    }

    /**
     * A CSS class name to apply to the charts container div, allowing unique CSS styling for each chart.
     */
    public T setClassName(String className) {
        this.className = className;
        return self();
    }

    protected abstract T self();

}
