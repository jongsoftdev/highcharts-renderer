/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Renderable;
import com.jongsoft.util.Builder;
import com.jongsoft.highchart.chart.style.Margin;

public abstract class Title<E extends Title, T> implements Builder<T>, Renderable {

    private final T parent;

    @JsonProperty private Alignment align;
    @JsonProperty private Number x;
    @JsonProperty private Number y;
    @JsonProperty private CSSObject<E> style;

    @JsonProperty private String text;

    public Title(T parent) {
        this.parent = parent;
    }

    /**
     * The horizontal alignment of the title. Can be one of "left", "center" and "right".
     * <p>
     * <b>Default:</b> {@link Alignment#CENTER}.
     */
    public E setAlign(Alignment align) {
        this.align = align;
        return self();
    }

    /**
     * CSS styles for the title. Exact positioning of the title can be achieved by changing the margin property,
     * or by adding position: "absolute" and left and top properties.
     */
    public CSSObject<E> getStyle() {
        if (style == null) {
            style = new CSSObject<>(self());
        }
        return style;
    }

    /**
     * Set the text of the title
     */
    public E setText(String text) {
        this.text = text;
        return self();
    }

    /**
     * The x position of the subtitle relative to the alignment
     * within {@link com.jongsoft.highchart.chart.Chart#setSpacing(Margin)}.
     * <p>
     * <b>Default:</b> 0
     */
    public E setX(Number x) {
        this.x = x;
        return self();
    }

    /**
     * The y position of the subtitle relative to the alignment
     * within {@link com.jongsoft.highchart.chart.Chart#setSpacing(Margin)}.
     * <p>
     * <b>Note:</b> For subtitles, is laid out below the title unless the title is floating.
     * <p>
     * <b>Default:</b> null
     */
    public E setY(Number y) {
        this.y = y;
        return self();
    }

    @Override
    public T build() {
        return parent;
    }
    
    protected abstract E self();

}
