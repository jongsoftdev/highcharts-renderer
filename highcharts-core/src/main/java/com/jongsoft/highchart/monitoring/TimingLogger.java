/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.monitoring;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

abstract class TimingLogger {
    private static final Logger LOG = LoggerFactory.getLogger(TimingLogger.class);
    private static final Logger TIMING_LOGGER = LoggerFactory.getLogger("timing");
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    static {
        OBJECT_MAPPER.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }

    private class TimingInfo {
        @JsonProperty String threadName;
        @JsonProperty(value = "class") String clazz;
        @JsonProperty String method;
        @JsonProperty Long timestamp;
        @JsonProperty Long executionTime;
        @JsonProperty Map<String, Object> params;
    }

    public Object methodTimer(final ProceedingJoinPoint pjp) throws Throwable {
        Long methodStart = System.currentTimeMillis();

        Object response = pjp.proceed();

        logTimingInfo(System.currentTimeMillis() - methodStart, pjp);

        // Regular logging
        if (LOG.isTraceEnabled()) {
            LOG.trace(String.format("Method call %s.%s that took %sms", pjp.getTarget().getClass().getName(),
                    pjp.getSignature().getName(), System.currentTimeMillis() - methodStart));
        }

        return response;
    }

    private void logTimingInfo(long functionTime, ProceedingJoinPoint pjp) throws JsonProcessingException {
        if (pjp.getSignature() instanceof MethodSignature) {
            MethodSignature ms = (MethodSignature) pjp.getSignature();
            Monitoring annotation = ms.getMethod().getAnnotation(Monitoring.class);

            if (shouldLog(annotation.level())) {
                TimingInfo timingInfo = new TimingInfo();
                timingInfo.threadName = Thread.currentThread().getName();
                timingInfo.clazz = pjp.getTarget().getClass().getName();
                timingInfo.method = pjp.getSignature().getName();
                timingInfo.params = new HashMap<>();
                timingInfo.executionTime = functionTime;
                timingInfo.timestamp = System.currentTimeMillis();

                if (annotation.parameterLogging()) {
                    int argNum = 0;
                    for (String param : ms.getParameterNames()) {
                        timingInfo.params.put(param, pjp.getArgs()[argNum]);
                        argNum++;
                    }
                }

                if (TIMING_LOGGER.isWarnEnabled()) {
                    TIMING_LOGGER.warn(OBJECT_MAPPER.writeValueAsString(timingInfo));
                }
            }
        }
    }

    private boolean shouldLog(Monitoring.Level level) {
        return (TIMING_LOGGER.isTraceEnabled() && level.getLevel() >= Monitoring.Level.TRACE.getLevel())
                || (TIMING_LOGGER.isDebugEnabled() && level.getLevel() >= Monitoring.Level.DEBUG.getLevel())
                || (TIMING_LOGGER.isInfoEnabled() && level.getLevel() >= Monitoring.Level.INFO.getLevel())
                || (TIMING_LOGGER.isWarnEnabled() && level.getLevel() >= Monitoring.Level.WARN.getLevel())
                || (TIMING_LOGGER.isErrorEnabled() && level.getLevel() >= Monitoring.Level.ERROR.getLevel());
    }
}
