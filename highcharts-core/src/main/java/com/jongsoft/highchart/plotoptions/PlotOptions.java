/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.plotoptions;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.highchart.common.SeriesType;
import com.jongsoft.util.IsEmpty;

import java.util.EnumMap;

public class PlotOptions implements IsEmpty {

    @JsonProperty private final EnumMap<SeriesType, SeriesPlotOptions> specificOptions;
    @JsonProperty private final SeriesPlotOptions series;

    public PlotOptions() {
        series = new SeriesPlotOptions(null);
        specificOptions = new EnumMap(SeriesType.class);

        for (SeriesType type : SeriesType.values()) {
            specificOptions.put(type, new SeriesPlotOptions(type));
        }
    }

    public SeriesPlotOptions getSeries() {
        return series;
    }

    public SeriesPlotOptions getOptions(SeriesType type) {
        return specificOptions.get(type);
    }

    @Override
    public boolean isEmpty() {
        return series.isEmpty() &&
            specificOptions.values().stream()
                .filter(s -> !s.isEmpty()).count() == 0;
    }
}
