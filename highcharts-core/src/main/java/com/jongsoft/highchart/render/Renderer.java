/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.render;

import java.io.OutputStream;

public interface Renderer<T> {
    void render();

    /**
     * Set the object that should be rendered using the implementation of this render engine.
     *
     * @param object the object that will be rendered to the output stream when {@link #render()} is called
     * @return this instance
     */
    Renderer<T> setRenderObject(T object);

    /**
     * Set the global options to be applied to the rendered object.
     *
     * @param globalOptions the global options to apply
     * @return this instance
     */
    Renderer<T> setGlobalOptions(T globalOptions);

    /**
     * Set the output stream that will be used to write the rendered result to. This stream is <b>not</b> closed by
     * the {@link #render()} method.
     *
     * @param outputStream the output stream to write to
     * @return this instance
     */
    Renderer<T> setOutputStream(OutputStream outputStream);

}
