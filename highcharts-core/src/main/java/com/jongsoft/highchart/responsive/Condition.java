/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.responsive;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.highchart.common.Function;
import com.jongsoft.util.Builder;
import com.jongsoft.util.IsEmpty;

public class Condition implements IsEmpty, Builder<Rule> {

    @JsonProperty private Function callback;
    @JsonProperty private Number maxHeight;
    @JsonProperty private Number maxWidth;
    @JsonProperty private Number minHeight;
    @JsonProperty private Number minWidth;

    private final Rule parent;

    public Condition(Rule parent) {
        this.parent = parent;
    }

    /**
     * A callback function to gain complete control on when the responsive rule applies. Return true if it applies.
     * This opens for checking against other metrics than the chart size, or example the document size or other elements.
     * <p>
     * The this keyword refers to the Chart object.
     *
     * @since 5.0.0
     */
    public Condition setCallback(Function callback) {
        this.callback = callback;
        return this;
    }

    /**
     * The responsive rule applies if the chart width is less than this.
     *
     * @since 5.0.0
     */
    public Condition setMaxWidth(Number maxWidth) {
        this.maxWidth = maxWidth;
        return this;
    }

    /**
     * The responsive rule applies if the chart height is less than this.
     *
     * @since 5.0.0
     */
    public Condition setMaxHeight(Number maxHeight) {
        this.maxHeight = maxHeight;
        return this;
    }

    /**
     * The responsive rule applies if the chart width is greater than this.
     * <p>
     * <b>Default:</b> 0
     *
     * @since 5.0.0
     */
    public Condition setMinWidth(Number minWidth) {
        this.minWidth = minWidth;
        return this;
    }

    /**
     * The responsive rule applies if the chart height is greater than this.
     * <p>
     * <b>Default:</b> 0
     *
     * @since 5.0.0
     */
    public Condition setMinHeight(Number minHeight) {
        this.minHeight = minHeight;
        return this;
    }

    @Override
    public boolean isEmpty() {
        return IsEmpty.isEmpty(this.callback, this.maxHeight, this.maxWidth, this.minHeight, this.minWidth);
    }

    @Override
    public Rule build() {
        return parent;
    }
}
