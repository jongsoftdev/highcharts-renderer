/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.responsive;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.util.Builder;
import com.jongsoft.util.IsEmpty;

import java.util.ArrayList;
import java.util.List;

/**
 * Allows setting a set of rules to apply for different screen or chart sizes.
 * Each rule specifies additional chart options.
 */
public class Responsive<T> implements IsEmpty, Builder<T> {

    @JsonProperty private final List<Rule> rules;

    private final T parent;

    public Responsive(T parent) {
        this.parent = parent;
        this.rules = new ArrayList<>();
    }

    /**
     * A set of rules for responsive settings. The rules are executed from the top down.
     *
     * @since 5.0.0
     */
    public Responsive<T> addRule(Rule rule) {
        this.rules.add(rule);
        return this;
    }

    @Override
    public boolean isEmpty() {
        return rules.stream().filter(r -> !r.isEmpty()).count() == 0;
    }

    @Override
    public T build() {
        return parent;
    }
}
