/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.responsive;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.highchart.AbstractHighchart;
import com.jongsoft.util.IsEmpty;

public class Rule implements IsEmpty {

    @JsonProperty private AbstractHighchart<?, ?> chartOptions;
    @JsonProperty private Condition condition;

    public Rule() {
        this.condition = new Condition(this);
    }

    /**
     * A full set of chart options to apply as overrides to the general chart options. The chart options
     * are applied when the given rule is active.
     *
     * @since 5.0.0
     */
    public <T extends AbstractHighchart<T, ?>> Rule setChartOptions(T chartOptions) {
        this.chartOptions = chartOptions;
        return this;
    }

    /**
     * Under which conditions the rule applies.
     *
     * @since 5.0.0
     */
    public Condition getCondition() {
        return condition;
    }

    @Override
    public boolean isEmpty() {
        return condition.isEmpty();
    }
}
