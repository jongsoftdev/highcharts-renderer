package com.jongsoft.highchart.serializer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.jongsoft.Renderable;
import org.apache.commons.lang3.SerializationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Optional;

public class BaseSerializer<T extends Renderable> extends JsonSerializer<T> {
    private class FieldValue {
        private final String fieldName;
        private final Object value;

        public FieldValue(String fieldName, Object value) {
            this.fieldName = fieldName;
            this.value = value;
        }
    }

    @Override
    public void serialize(T toSerialize, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (isEmpty(serializerProvider, toSerialize)) {
            jsonGenerator.writeNull();
        } else {
            jsonGenerator.writeStartObject();
            FieldUtils.getFieldsListWithAnnotation(toSerialize.getClass(), JsonProperty.class).stream()
                .map(f -> translateField(toSerialize, f))
                .filter(f -> shouldSerialize(f, serializerProvider))
                .forEach(f -> writeObjectField(toSerialize, jsonGenerator, f));
            jsonGenerator.writeEndObject();
        }
    }

    @Override
    public Class<T> handledType() {
        return (Class<T>) Renderable.class;
    }

    private void writeObjectField(T toSerialize, JsonGenerator jsonGenerator, FieldValue subject) {
        try {
            jsonGenerator.writeObjectField(subject.fieldName, subject.value);
        } catch (IOException e) {
            throw new SerializationException(String.format("Unable to serialize property %s of %s, root cause %s",
                    subject.fieldName, toSerialize.getClass().getSimpleName(), e.getCause()), e);
        }
    }

    private boolean shouldSerialize(FieldValue f, SerializerProvider serializerProvider) {
            return Optional.ofNullable(f.value)
                    .map(c -> {
                        try {
                            return serializerProvider.findValueSerializer(f.value.getClass());
                        } catch (JsonMappingException e) {
                            return null;
                        }
                    })
                    .map(s -> !s.isEmpty(serializerProvider, f.value))
                    .orElse(false);
    }

    private FieldValue translateField(T subject, Field field) {
        boolean wasAccessible = field.isAccessible();
        if (!wasAccessible) {
            field.setAccessible(true);
        }

        try {
            String propertyName = Optional.ofNullable(field.getAnnotation(JsonProperty.class))
                    .filter(a -> StringUtils.isNoneBlank(a.value()))
                    .map(JsonProperty::value)
                    .orElse(field.getName());

            return new FieldValue(propertyName, field.get(subject));
        } catch (IllegalAccessException e) {
            throw new SerializerException(String.format("Cannot access property %s of entity %s",
                    field.getName(), subject.getClass().getSimpleName()));
        } finally {
            field.setAccessible(wasAccessible);
        }
    }
}
