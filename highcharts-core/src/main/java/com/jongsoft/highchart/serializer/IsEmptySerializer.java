package com.jongsoft.highchart.serializer;

import com.fasterxml.jackson.databind.SerializerProvider;
import com.jongsoft.util.IsEmpty;

public class IsEmptySerializer extends BaseSerializer<IsEmpty> {

    @Override
    public boolean isEmpty(SerializerProvider provider, IsEmpty value) {
        return value.isEmpty();
    }

    @Override
    public Class<IsEmpty> handledType() {
        return IsEmpty.class;
    }
}
