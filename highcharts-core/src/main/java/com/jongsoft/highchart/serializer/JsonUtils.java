/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.serializer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class JsonUtils {

    private static final JsonUtils INSTANCE = new JsonUtils();

    private final ObjectMapper mapper;

    private JsonUtils() {
        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
        mapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
//        mapper.setAnnotationIntrospector(new JacksonAnnotationIntrospector());
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        SimpleModule highchartModule = new SimpleModule(new Version(2, 0, 0, null, "com.com.jongsoft.utils", "highchart-renderer"));
        highchartModule.addSerializer(new BaseSerializer<>());
        highchartModule.addSerializer(new MarginSerializer());
        highchartModule.addSerializer(new CSSObjectSerializer());
        highchartModule.addSerializer(new PointListSerializer());
        highchartModule.addSerializer(new ListSerializer());
        highchartModule.addSerializer(new GraphColorSerializer());
        highchartModule.addSerializer(new FunctionSerializer());
        highchartModule.addSerializer(new AxisUnitsSerializer());
        highchartModule.addSerializer(new ColorStopSerializer());
        highchartModule.addSerializer(new PlotOptionSerializer());
        highchartModule.addSerializer(new StringSerializer());
        highchartModule.addSerializer(new IsEmptySerializer());
        mapper.registerModule(highchartModule);
    }

    public static <T> String serialize(T object) {
        try {
            return INSTANCE.mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new SerializerException("Could not serialize the graph.", e);
        }
    }
}
