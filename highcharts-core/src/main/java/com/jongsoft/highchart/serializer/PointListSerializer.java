/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.jongsoft.highchart.series.PointList;
import com.jongsoft.highchart.series.SeriesPoint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PointListSerializer extends JsonSerializer<PointList> {

    private static final int MAX_SIMPLE_POINTS = 1000;
    private static final Comparator<SeriesPoint> COMPARATOR = (point1, point2) -> point1.getX() != null && point2.getX() != null
            ? Double.compare(point1.getX().doubleValue(), point2.getX().doubleValue())
            : 0;

    @Override
    public void serialize(PointList value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        List<SeriesPoint> sorted = value.stream()
                .sorted(COMPARATOR)
                .collect(Collectors.toList());
        if (sorted.size() > MAX_SIMPLE_POINTS) {
            List<Number[]> values = new ArrayList<>();
            for (SeriesPoint pt : sorted) {
                if (pt.getValue() != null) {
                    values.add(new Number[]{pt.getX(), pt.getY(), pt.getValue()});
                } else {
                    values.add(new Number[]{pt.getX(), pt.getY()});
                }
            }
            gen.writeObject(values);
        } else {
            gen.writeObject(sorted);
        }
    }

    @Override
    public Class<PointList> handledType() {
        return PointList.class;
    }

}
