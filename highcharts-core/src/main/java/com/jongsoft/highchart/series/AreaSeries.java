/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.series;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.highchart.common.DashStyle;
import com.jongsoft.highchart.common.SeriesType;

public class AreaSeries extends Series<AreaSeries> {
    
    public static class AreaSplinSeries extends AreaSeries {

        public AreaSplinSeries() {
            super();
            setType(SeriesType.AREA_SPLINE);
        }

        @Override
        protected AreaSeries self() {
            return this;
        }
        
    }

    @JsonProperty private Boolean connectEnds;
    @JsonProperty private Boolean connectNulls;
    @JsonProperty private DashStyle dashStyle;

    protected AreaSeries() {
        super(SeriesType.AREA);
    }

    /**
     * Polar charts only. Whether to connect the ends of a line series plot across the extremes.
     * <p>
     * Defaults to true.
     */
    public AreaSeries setConnectEnds(Boolean connectEnds) {
        this.connectEnds = connectEnds;
        return self();
    }

    /**
     * Whether to connect a graph line across null points.
     * <p>
     * Defaults to false.
     */
    public AreaSeries setConnectNulls(Boolean connectNulls) {
        this.connectNulls = connectNulls;
        return self();
    }

    /**
     * A name for the dash style to use for the graph. Applies only to series type having a graph,
     * like line, spline, area and scatter in case it has a lineWidth
     */
    public AreaSeries setDashStyle(DashStyle dashStyle) {
        this.dashStyle = dashStyle;
        return self();
    }

    @Override
    protected AreaSeries self() {
        return this;
    }

}
