package com.jongsoft.highchart.series;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.HashSetBuilder;
import com.jongsoft.HighchartModules;
import com.jongsoft.highchart.common.Function;
import com.jongsoft.highchart.common.GraphColor;
import com.jongsoft.highchart.common.SeriesType;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BarSeries extends Series<BarSeries> {

    @JsonProperty private GraphColor borderColor;
    @JsonProperty private Number borderRadius;
    @JsonProperty private Number borderWidth;
    @JsonProperty private Boolean colorByPoint;
    @JsonProperty private List<GraphColor> colors;
    @JsonProperty private Number depth;
    @JsonProperty private GraphColor edgeColor;
    @JsonProperty private Number edgeWidth;
    @JsonProperty private Number groupPadding;
    @JsonProperty private Number groupZPadding;
    @JsonProperty private Boolean grouping;
    @JsonProperty private Number maxPointWidth;
    @JsonProperty private Number minPointLength;

    @JsonProperty private Function pointDescriptionFormatter;
    @JsonProperty private Number pointPadding;
    @JsonProperty private Number pointPlacement;
    @JsonProperty private Number pointRange;
    @JsonProperty private Number pointStart;
    @JsonProperty private Number pointWidth;


    public BarSeries() {
        this(SeriesType.BAR);
    }

    protected BarSeries(SeriesType type) {
        super(type);
        this.colors = new ArrayList<>();
    }

    /**
     * The color of the border surrounding each column or bar.
     * <p>
     * <b>Default:</b> #FFFFFF
     */
    public BarSeries setBorderColor(GraphColor borderColor) {
        this.borderColor = borderColor;
        return self();
    }

    /**
     * The corner radius of the border surrounding each column or bar.
     * <p>
     * <b>Default:</b> 0
     */
    public BarSeries setBorderRadius(Number borderRadius) {
        this.borderRadius = borderRadius;
        return self();
    }

    /**
     * The width of the border surrounding each column or bar.
     * <p>
     * <b>Default:</b> 1
     */
    public BarSeries setBorderWidth(Number borderWidth) {
        this.borderWidth = borderWidth;
        return self();
    }

    /**
     * When using automatic point colors pulled from the options.colors collection, this option determines whether
     * the chart should receive one color per series or one color per point.
     * <p>
     * <b>Default:</b> false
     *
     * @since 2.0
     */
    public BarSeries setColorByPoint(Boolean value) {
        this.colorByPoint = value;
        return self();
    }

    /**
     * A series specific or series type specific color set to apply instead of the global colors when
     * {@link #setColorByPoint(Boolean)} is true.
     */
    public BarSeries addColor(GraphColor color) {
        this.colors.add(color);
        return self();
    }

    /**
     * Depth of the columns in a 3D column chart.
     * <p>
     * <b>Default:</b> 25
     *
     * @since 4.0
     */
    public BarSeries setDepth(Number depth) {
        this.depth = depth;
        return self();
    }

    /**
     * 3D columns only. The color of the edges. Similar to {@link #setBorderColor(GraphColor)}, except it defaults to the same color
     * as the column.
     */
    public BarSeries setEdgeColor(GraphColor edgeColor) {
        this.edgeColor = edgeColor;
        return self();
    }

    /**
     * 3D columns only. The width of the colored edges.
     * <p>
     * <b>Default:</b> 1
     */
    public BarSeries setEdgeWidth(Number edgeWidth) {
        this.edgeWidth = edgeWidth;
        return self();
    }

    /**
     * Padding between each value groups, in x axis units.
     * <p>
     * <b>Default:</b> 0.2
     */
    public BarSeries setGroupPadding(Number groupPadding) {
        this.groupPadding = groupPadding;
        return self();
    }

    /**
     * The spacing between columns on the Z Axis in a 3D chart.
     * <p>
     * <b>Default:</b> 1
     */
    public BarSeries setGroupZPadding(Number groupZPadding) {
        this.groupZPadding = groupZPadding;
        return self();
    }

    /**
     * Whether to group non-stacked columns or to let them render independent of each other. Non-grouped columns
     * will be laid out individually and overlap each other.
     * <p>
     * <b>Default:</b> true
     */
    public BarSeries setGrouping(Boolean grouping) {
        this.grouping = grouping;
        return self();
    }

    /**
     * The maximum allowed pixel width for a column, translated to the height of a bar in a bar chart. This prevents the
     * columns from becoming too wide when there is a small number of points in the chart.
     * <p>
     * <b>Default:</b> null
     *
     * @since 4.1.8
     */
    public BarSeries setMaxPointWidth(Number maxPointWidth) {
        this.maxPointWidth = maxPointWidth;
        return self();
    }

    /**
     * The minimal height for a column or width for a bar. By default, 0 values are not shown. To visualize a 0
     * (or close to zero) point, set the minimal point length to a pixel value like 3. In stacked column charts,
     * {@link #setMinPointLength(Number)} might not be respected for tightly packed values.
     * <p>
     * <b>Default:</b> 0
     */
    public BarSeries setMinPointLength(Number minPointLength) {
        this.minPointLength = minPointLength;
        return self();
    }

    /**
     * Same as accessibility.pointDescriptionFormatter, but for an individual series. Overrides the chart wide configuration.
     */
    public BarSeries setPointFormatter(Function pointFormatter) {
        this.pointDescriptionFormatter = pointFormatter;
        return self();
    }

    /**
     * Padding between each column or bar, in x axis units.
     * <p>
     * <b>Default:</b> 0.1
     */
    public BarSeries setPointPadding(Number pointPadding) {
        this.pointPadding = pointPadding;
        return self();
    }

    /**
     * Note that pointPlacement needs a pointRange to work. For column series this is computed, but for line-type series
     * it needs to be set.
     * <p>
     * For the xrange series type and gantt charts, if the Y axis is a category axis, the pointPlacement applies to the
     * Y axis rather than the (typically datetime) X axis.
     * </p>
     */
    public BarSeries setPointPlacement(Number pointPlacement) {
        this.pointPlacement = pointPlacement;
        return self();
    }

    /**
     * The X axis range that each point is valid for. This determines the width of the column. On a categorized axis,
     * the range will be 1 by default (one category unit). On linear and datetime axes, the range will be computed as
     * the distance between the two closest data points.
     * <p>
     * The default null means it is computed automatically, but this option can be used to override the automatic value.
     * </p>
     * <b>Default:</b> null
     */
    public BarSeries setPointRange(Number pointRange) {
        this.pointRange = pointRange;
        return self();
    }

    /**
     * If no x values are given for the points in a series, pointStart defines on what value to start.
     * For example, if a series contains one yearly value starting from 1945, set pointStart to 1945.
     */
    public BarSeries setPointStart(Number pointStart) {
        this.pointStart = pointStart;
        return self();
    }

    /**
     * A pixel value specifying a fixed width for each column or bar. When null, the width is calculated from the
     * pointPadding and groupPadding.
     */
    public BarSeries setPointWidth(Number pointWidth) {
        this.pointWidth = pointWidth;
        return self();
    }

    @Override
    protected BarSeries self() {
        return this;
    }


    @Override
    public Set<HighchartModules> neededModules() {
        return new HashSetBuilder<HighchartModules>()
                .add(this.depth != null, HighchartModules.THREE_D)
                .add(this.groupZPadding != null, HighchartModules.THREE_D)
                .build();
    }
}
