/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.series;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.util.Builder;
import com.jongsoft.highchart.common.Alignment;
import com.jongsoft.highchart.common.Function;
import com.jongsoft.highchart.common.Styled;

public class DataLabels<T> extends Styled<DataLabels<T>> implements Builder<T> {

    @JsonProperty private Alignment align;
    @JsonProperty private Boolean allowOverlap;
    @JsonProperty private String color;
    @JsonProperty private Boolean crop;
    @JsonProperty private Boolean defer;
    @JsonProperty private Boolean enabled;
    @JsonProperty private String format;
    @JsonProperty private Function.Formatter formatter;
    @JsonProperty private Boolean inside;
    @JsonProperty private String overflow;
    @JsonProperty private Number padding;
    @JsonProperty private Number x;
    @JsonProperty private Number y;
    @JsonProperty private Boolean reserveSpace;
    @JsonProperty private Number rotation;
    private final T parent;

    public DataLabels(T parent) {
        this.parent = parent;
    }

    /**
     * The alignment of the data label compared to the point. If right, the right side of the label should be
     * touching the point. For points with an extent, like columns, the alignments also dictates how to align
     * it inside the box, as given with the inside option. Can be one of "left", "center" or "right".
     * <p>
     * <b>Default:</b> center.
     */
    public DataLabels<T> setAlign(Alignment align) {
        this.align = align;
        return self();
    }

    /**
     * Whether to allow data labels to overlap. To make the labels less sensitive for overlapping,
     * the {@link #setPadding(Number)} can be set to 0.
     * <p>
     * <b>Default:</b> false.
     */
    public DataLabels<T> setAllowOverlap(Boolean allowOverlap) {
        this.allowOverlap = allowOverlap;
        return self();
    }

    /**
     * The text color for the data labels.
     * <p>
     * <b>Default:</b> null.
     */
    public DataLabels<T> setColor(String color) {
        this.color = color;
        return self();
    }

    /**
     * Whether to hide data labels that are outside the plot area. By default, the data label is moved inside the
     * plot area according to the {@link #setOverflow(String)} option.
     * <p>
     * <b>Default:</b> true.
     *
     * @since 2.3.3
     */
    public DataLabels<T> setCrop(Boolean crop) {
        this.crop = crop;
        return self();
    }

    /**
     * Whether to defer displaying the data labels until the initial series animation has finished.
     * <p>
     * <b>Default:</b> true.
     *
     * @since 4.0
     */
    public DataLabels<T> setDefer(Boolean defer) {
        this.defer = defer;
        return self();
    }

    /**
     * Enable or disable the data labels.
     * <p>
     * <b>Default:</b> false.
     */
    public DataLabels<T> setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return self();
    }

    /**
     * For points with an extent, like columns, whether to align the data label inside the box or to the actual value point.
     * <p>
     * <b>Default:</b> false in most cases, true in stacked columns.
     *
     * @since 3.0
     */
    public DataLabels<T> setInside(Boolean inside) {
        this.inside = inside;
        return self();
    }

    /**
     * A format string for the data label. Available variables are the same as for formatter. <b>Default:</b> to {y}.
     *
     * @see <a href="http://www.highcharts.com/docs/chart-concepts/labels-and-string-formatting">Online documentation format rules</a>
     */
    public DataLabels<T> setFormat(String format) {
        this.format = format;
        return self();
    }

    /**
     * Callback JavaScript function to format the data label. Note that if a format is defined, the format takes precedence
     * and the formatter is ignored.
     */
    public DataLabels<T> setFormatter(Function.Formatter formatter) {
        this.formatter = formatter;
        return self();
    }

    /**
     * How to handle data labels that flow outside the plot area. The default is justify, which aligns them inside the plot area.
     * For columns and bars, this means it will be moved inside the bar. To display data labels outside the plot area,
     * set {@link #setCrop(Boolean)} to false and overflow to "none".
     * <p>
     * <b>Default:</b> justify.
     *
     * @since 3.0.6
     */
    public DataLabels<T> setOverflow(String overflow) {
        this.overflow = overflow;
        return self();
    }

    /**
     * When either the borderWidth or the backgroundColor is set, this	is the padding within the box.
     * <p>
     * <b>Default:</b> 5.
     *
     * @since 2.2.1
     */
    public DataLabels<T> setPadding(Number padding) {
        this.padding = padding;
        return self();
    }

    /**
     * Whether to reserve space for the labels. This can be turned off when for example the labels are rendered inside
     * the plot area instead of outside.
     * <p>
     * <b>Default:</b> true
     *
     * @since 4.1.10
     */
    public DataLabels<T> setReserveSpace(Boolean reserveSpace) {
        this.reserveSpace = reserveSpace;
        return self();
    }

    /**
     * Text rotation in degrees. Note that due to a more complex structure, backgrounds, borders and padding will
     * be lost on a rotated data label.
     * <p>
     * <b>Default:</b> 0
     */
    public DataLabels<T> setRotation(Number rotation) {
        this.rotation = rotation;
        return self();
    }

    public DataLabels<T> setX(Number x) {
        this.x = x;
        return this;
    }

    public DataLabels<T> setY(Number y) {
        this.y = y;
        return this;
    }

    @Override
    public T build() {
        return parent;
    }

    @Override
    protected DataLabels<T> self() {
        return this;
    }

}
