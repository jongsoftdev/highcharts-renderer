package com.jongsoft.highchart.series;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GaugePoint extends SeriesPoint {

    @JsonProperty private Number innerRadius;
    @JsonProperty private Number radius;

    /**
     * The inner radius of an individual point in a solid gauge. Can be given as a number (pixels) or percentage string.
     *
     * @since 4.1.6
     */
    public GaugePoint setInnerRadius(Number innerRadius) {
        this.innerRadius = innerRadius;
        return this;
    }

    /**
     * The outer radius of an individual point in a solid gauge. Can be given as a number (pixels) or percentage string.
     */
    public GaugePoint setRadius(Number outerRadius) {
        this.radius = outerRadius;
        return this;
    }
}
