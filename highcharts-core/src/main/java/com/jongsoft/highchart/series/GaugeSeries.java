package com.jongsoft.highchart.series;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.highchart.common.SeriesType;

public class GaugeSeries extends Series<GaugeSeries> {

    @JsonProperty private Boolean rounded;

    protected GaugeSeries(boolean solid) {
        super(solid ? SeriesType.GAUGE_SOLID : SeriesType.GAUGE);
    }

    public GaugeSeries setRounded(Boolean rounded) {
        this.rounded = rounded;
        return this;
    }

    @Override
    protected GaugeSeries self() {
        return this;
    }

}
