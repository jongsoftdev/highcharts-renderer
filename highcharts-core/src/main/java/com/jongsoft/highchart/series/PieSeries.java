package com.jongsoft.highchart.series;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.highchart.common.SeriesType;

public class PieSeries extends Series<PieSeries> {

    @JsonProperty private Number innerSize;

    public PieSeries() {
        super(SeriesType.PIE);
    }

    public PieSeries setInnerSize(Number innerSize) {
        this.innerSize = innerSize;
        return this;
    }

    @Override
    protected PieSeries self() {
        return this;
    }

}
