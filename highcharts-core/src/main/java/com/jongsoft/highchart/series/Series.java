/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.series;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.Extensions;
import com.jongsoft.highchart.AbstractHighchart;
import com.jongsoft.highchart.axis.Marker;
import com.jongsoft.highchart.common.CursorType;
import com.jongsoft.highchart.common.GraphColor;
import com.jongsoft.highchart.common.SeriesType;
import com.jongsoft.util.IsEmpty;

import java.util.ArrayList;
import java.util.List;

/**
 * The actual series to append to the chart. In addition to the members listed below, any member of the plotOptions for that specific type of plot can be added
 * to a series individually. For example, even though a general lineWidth is specified in plotOptions.series, an individual lineWidth can be specified for each
 * series.
 */
public abstract class Series<T> implements IsEmpty, Extensions {

    @JsonProperty private Boolean allowPointSelect;
    @JsonProperty private Boolean animation;
    @JsonProperty private Number animationLimit;
    @JsonProperty private GraphColor color;
    @JsonProperty private Number cropThreshold;
    @JsonProperty private String className;
    @JsonProperty private CursorType cursor;
    @JsonProperty private List<SeriesPoint> data;
    @JsonProperty private DataLabels<T> dataLabels;
    @JsonProperty private Boolean enableMouseTracking;
    @JsonProperty private String id;
    @JsonProperty private Number index;
    @JsonProperty private List<String> keys;
    @JsonProperty private Number legendIndex;
    @JsonProperty private String linkedTo;
    @JsonProperty private Marker<T> marker;
    @JsonProperty private String name;
    @JsonProperty private GraphColor negativeColor;
    @JsonProperty private Boolean showInLegend;
    @JsonProperty private String stack;
    @JsonProperty private String stacking;
    @JsonProperty private Number turboThreshold;
    @JsonProperty private Boolean visible;
    @JsonProperty private SeriesType type;
    @JsonProperty private Number xAxis;
    @JsonProperty private Number yAxis;
    @JsonProperty private Number zIndex;
    @JsonProperty private String zoneAxis;

    protected Series(SeriesType type) {
        this.type = type;
        this.keys = new ArrayList<>();
    }

    protected void setType(SeriesType type) {
        this.type = type;
    }

    /**
     * @since 5.0.0
     */
    public T setClassName(String className) {
        this.className = className;
        return self();
    }

    /**
     * Enable or disable the initial animation when a series is displayed. The animation can also be set as a configuration object. Please note that this option
     * only applies to the initial animation of the series itself.
     */
    public T setAnimation(Boolean animation) {
        this.animation = animation;
        return self();
    }

    /**
     * The name of the series as shown in the legend, tooltip etc.
     */
    public T setName(String name) {
        this.name = name;
        return self();
    }

    /**
     * This option allows grouping series in a stacked chart. The stack option can be a string or a number
     * or anything else, as long as the grouped series' stack options match each other.
     */
    public T setStack(String stack) {
        this.stack = stack;
        return self();
    }

    /**
     * Whether to stack the values of each series on top of each other. Possible values are null to
     * disable, "normal" to stack by value or "percent". When stacking is enabled,
     * data must be sorted in ascending X order.
     */
    public T setStacking(String stacking) {
        this.stacking = stacking;
        return self();
    }

    /**
     * Allow this series' points to be selected by clicking on the markers, bars or pie slices.
     * <p>
     * Default: false.
     */
    public T setAllowPointSelect(Boolean allowPointSelect) {
        this.allowPointSelect = allowPointSelect;
        return self();
    }

    /**
     * For some series, there is a limit that shuts down initial animation by default when the total number of points in
     * the chart is too high. For example, for a column chart and its derivatives, animation doesn't run if there is more
     * than 250 points totally.
     * <p>
     * To disable this cap, set animationLimit to <code>Infinity</code>.
     */
    public T setAnimationLimit(Number animationLimit) {
        this.animationLimit = animationLimit;
        return self();
    }

    /**
     * The main color or the series. In line type series it applies to the line and the point markers unless otherwise specified. In bar type series it applies
     * to the bars unless a color is specified per point.
     * <p>
     * The default value is pulled from the {@link AbstractHighchart#getColors()} array.
     */
    public T setColor(GraphColor color) {
        this.color = color;
        return self();
    }

    /**
     * When the series contains less points than the crop threshold, all points are drawn, event if the points fall outside the visible plot area at the current
     * zoom. The advantage of drawing all points (including markers and columns), is that animation is performed on updates. On the other hand, when the series
     * contains more points than the crop threshold, the series data is cropped to only contain points that fall within the plot area.
     * <p>
     * The advantage of cropping away invisible points is to increase performance on large series.
     * <p>
     * Default: 300.
     */
    public T setCropThreshold(Number cropThreshold) {
        this.cropThreshold = cropThreshold;
        return self();
    }

    /**
     * You can set the cursor to "pointer" if you have click events attached to the series, to signal to the user that the points and lines can be clicked.
     */
    public T setCursor(CursorType cursor) {
        this.cursor = cursor;
        return self();
    }

    /**
     * Enable or disable the mouse tracking for a specific series. This includes point tooltips and click events on graphs and points. For large datasets it
     * improves performance.
     * <p>
     * <b>Default:</b> true
     */
    public T setEnableMouseTracking(Boolean enableMouseTracking) {
        this.enableMouseTracking = enableMouseTracking;
        return self();
    }

    /**
     * An id for the series. This can be used after render time to get a pointer to the series object
     * through chart.get()
     *
     * @since 1.2.0
     */
    public T setId(String id) {
        this.id = id;
        return self();
    }

    /**
     * The index of the series in the chart, affecting the internal index in the chart.series array,
     * the visible Z index as well as the order in the legend.
     *
     * @since 2.3.0
     */
    public T setIndex(Number index) {
        this.index = index;
        return self();
    }

    /**
     * An array specifying which option maps to which key in the data point array. This makes it convenient to
     * work with unstructured data arrays from different sources.
     *
     * @since 4.1.6
     */
    public T addKey(String key) {
        this.keys.add(key);
        return self();
    }

    /**
     * The sequential index of the series in the legend.
     */
    public T setLegendIndex(Number legendIndex) {
        this.legendIndex = legendIndex;
        return self();
    }

    /**
     * The id of another series to link to. Additionally, the value can be ":previous" to link to the previous series.
     * When two series are linked, only the first one appears in the legend. Toggling the visibility of this also
     * toggles the linked series.
     *
     * @since 3.0.0
     */
    public T setLinkedTo(String linkedTo) {
        this.linkedTo = linkedTo;
        return self();
    }

    /**
     * The color for the parts of the graph or points that are below the <code>threshold</code>.
     * <p>
     * <b>Default:</b> null
     *
     * @since 3.0.0
     */
    public T setNegativeColor(GraphColor negativeColor) {
        this.negativeColor = negativeColor;
        return self();
    }

    /**
     * Add a point to the {@link Series} which will translate into one point on the chart.
     */
    public T addPoint(SeriesPoint point) {
        if (data == null) {
            data = new PointList();
        }
        data.add(point);
        return self();
    }

    /**
     * When a series contains a data array that is longer than this, only one dimensional arrays of numbers,
     * or two dimensional arrays with x and y values are allowed. Also, only the first point is tested, and the
     * rest are assumed to be the same format. This saves expensive data checking and indexing in long series.
     * <p>
     * Set it to 0 disable.
     * <p>
     * <b>Default:</b> 1000
     *
     * @since 2.2
     */
    public T setTurboThreshold(Number turboThreshold) {
        this.turboThreshold = turboThreshold;
        return self();
    }

    /**
     * Set the initial visibility of the series.
     * <p>
     * <b>Default:</b> true
     */
    public T setVisible(Boolean visible) {
        this.visible = visible;
        return self();
    }

    /**
     * When using dual or multiple x axes, this number defines which xAxis the particular series is connected to. It refers to either the axis id or the index
     * of the axis in the xAxis array, with 0 being the first.
     */
    public T setXAxis(Number xAxis) {
        this.xAxis = xAxis;
        return self();
    }

    /**
     * When using dual or multiple y axes, this number defines which yAxis the particular series is connected to. It refers to either the axis id or the index
     * of the axis in the yAxis array, with 0 being the first
     */
    public T setYAxis(Number yAxis) {
        this.yAxis = yAxis;
        return self();
    }

    /**
     * Define the visual z index of the series.
     */
    public T setZIndex(Number zIndex) {
        this.zIndex = zIndex;
        return self();
    }

    /**
     * Defines the Axis on which the zones are applied.
     * <p>
     * <b>Default:</b> y
     */
    public T setZoneAxis(String zoneAxis) {
        this.zoneAxis = zoneAxis;
        return self();
    }

    /**
     * Whether to display this particular series or series type in the legend. The default value is true
     * for standalone series, false for linked series.
     * <p>
     * <b>Default: </b> true.
     */
    public T setShowInLegend(Boolean legend) {
        this.showInLegend = legend;
        return self();
    }

    protected abstract T self();

    public DataLabels<T> getDataLabels() {
        if (dataLabels == null) {
            dataLabels = new DataLabels(self());
        }
        return dataLabels;
    }

    /**
     * Options for the point markers of line-like series. Properties like fillColor, lineColor and lineWidth
     * define the visual appearance of the markers. Other series types, like column series, don't have markers,
     * but have visual options on the series level instead.
     */
    public Marker<T> getMarker() {
        if (marker == null) {
            marker = new Marker<>(self());
        }
        return marker;
    }

    @Override
    public boolean isEmpty() {
        return (data == null || data.isEmpty()) &&
                IsEmpty.isEmpty(allowPointSelect, animation, animationLimit, name, allowPointSelect, color,
                        cropThreshold, cursor, xAxis, yAxis, id, index, enableMouseTracking);
    }

}
