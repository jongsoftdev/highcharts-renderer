/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.series;

import com.jongsoft.highchart.common.SeriesType;

public class SeriesFactory {

    private SeriesFactory() {
    }

    /**
     * Create a {@link Series} for the provided {@link SeriesType}.
     *
     * @throws UnsupportedSeriesException   in case a not yet supported {@link SeriesType} is used
     */
    @SuppressWarnings("unchecked")
    public static <T extends Series<T>> T createSeries(SeriesType type) {
        switch (type) {
            case LINE: return (T) new LineSeries();
            case AREA: return (T) new AreaSeries();
            case AREA_SPLINE: return (T) new AreaSeries.AreaSplinSeries();
            case COLUMN: return (T) new ColumnSeries();
            case SPLINE: return (T) new SplineSeries();
            case SCATTER: return (T) new ScatterSeries();
            case HEAT_MAP: return (T) new HeatMapSeries();
            case BAR: return (T) new BarSeries();
            case PIE: return (T) new PieSeries();
            case GAUGE: return (T) new GaugeSeries(false);
            case GAUGE_SOLID: return (T) new GaugeSeries(true);
            default:
                throw new UnsupportedSeriesException("Series type not yet supported " + type);
        }
    }
}
