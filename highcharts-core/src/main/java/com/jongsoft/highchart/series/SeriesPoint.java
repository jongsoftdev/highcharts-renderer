/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.series;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SeriesPoint {
    @JsonProperty private Number x;
    @JsonProperty private Number y;
    @JsonProperty private Number value;
    @JsonProperty private String color;
    @JsonProperty private String name;

    /**
     * The x value of the point. For datetime axes, the X value is the timestamp in milliseconds since 1970.
     */
    public SeriesPoint setX(Number x) {
        this.x = x;
        return this;
    }

    /**
     * The y value of the point.
     */
    public SeriesPoint setY(Number y) {
        this.y = y;
        return this;
    }

    /**
     * Individual color for the point. By default the color is pulled from the global colors array. Defaults to undefined.
     */
    public SeriesPoint setColor(String color) {
        this.color = color;
        return this;
    }

    public SeriesPoint setValue(Number value) {
        this.value = value;
        return this;
    }

    public SeriesPoint setName(String name) {
        this.name = name;
        return this;
    }

    public Number getX() {
        return x;
    }

    public Number getY() {
        return y;
    }

    public Number getValue() {
        return value;
    }

    public String getColor() {
        return color;
    }

    public String getName() {
        return name;
    }
    
}
