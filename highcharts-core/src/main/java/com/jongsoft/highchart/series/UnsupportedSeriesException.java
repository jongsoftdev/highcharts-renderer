package com.jongsoft.highchart.series;

import com.jongsoft.highchart.HighchartRuntimeException;

public class UnsupportedSeriesException extends HighchartRuntimeException {

    public UnsupportedSeriesException(String message) {
        super(message);
    }

}
