package com.jongsoft.highchart.series;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jongsoft.highchart.common.DashStyle;
import com.jongsoft.highchart.common.GraphColor;

public class Zone {

    @JsonProperty private String className;
    @JsonProperty private GraphColor color;
    @JsonProperty private DashStyle dashStyle;
    @JsonProperty private GraphColor fillColor;
    @JsonProperty private Number value;

    /**
     * Styled mode only. A custom class name for the zone.
     *
     * @since 5.0.0
     */
    public Zone setClassName(String className) {
        this.className = className;
        return this;
    }

    /**
     * Defines the color of the series.
     *
     * @since 4.1.0
     */
    public Zone setColor(GraphColor color) {
        this.color = color;
        return this;
    }

    /**
     * A name for the dash style to use for the graph.
     *
     * @since 4.1.0
     */
    public Zone setDashStyle(DashStyle dashStyle) {
        this.dashStyle = dashStyle;
        return this;
    }

    /**
     * Defines the fill color for the series (in area type series)
     *
     * @since 4.1.0
     */
    public Zone setFillColor(GraphColor fillColor) {
        this.fillColor = fillColor;
        return this;
    }

    /**
     * The value up to where the zone extends, if undefined the zones stretches to the last value in the series.
     * <p />
     * <b>Default:</b> undefined
     *
     * @since 4.1.0
     */
    public Zone setValue(Number value) {
        this.value = value;
        return this;
    }

}
