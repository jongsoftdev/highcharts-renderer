package com.jongsoft.util;

public class EmptyBuilder {
    private boolean isEmpty = true;

    public <T> EmptyBuilder add(T object) {
        isEmpty = isEmpty && (object == null);
        return this;
    }

    public boolean build() {
        return isEmpty;
    }
}
