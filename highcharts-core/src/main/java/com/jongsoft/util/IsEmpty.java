package com.jongsoft.util;

import com.jongsoft.Renderable;

public interface IsEmpty extends Renderable {
    boolean isEmpty();

    static boolean isEmpty(Object...objects) {
        for(Object object : objects) {
            if (object != null) {
                return false;
            }
        }
        return true;
    }
}
