package com.jongsoft.highchart;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jongsoft.highchart.common.Alignment;
import com.jongsoft.highchart.common.SeriesType;
import com.jongsoft.highchart.series.LineSeries;
import org.hamcrest.Matchers;

import static org.hamcrest.MatcherAssert.assertThat;

public class Highchart3DTest {

    private static String CHART_STRING = "\"chart\" : {" + System.lineSeparator() +
            "    \"alignTicks\" : true," + System.lineSeparator() +
            "    \"animation\" : false," + System.lineSeparator() +
            "    \"height\" : 500," + System.lineSeparator() +
            "    \"width\" : 600," + System.lineSeparator() +
            "    \"options3D\" : {" + System.lineSeparator() +
            "      \"alpha\" : 0.22," + System.lineSeparator() +
            "      \"beta\" : 0.44" + System.lineSeparator() +
            "    }" + System.lineSeparator() +
            "  }";
    private static String EXPORTING_STRING = "\"exporting\" : {" + System.lineSeparator() +
            "    \"enabled\" : false" + System.lineSeparator() +
            "  },";

    public void charTest3D() throws JsonProcessingException {
        Highchart3D highchart = new Highchart3D();
        highchart.getChart()
                .setAlignTicks(true)
                .setAnimation(false)
                .setHeight(500)
                .setWidth(600)
                .getOptions3D()
                .setAlpha(0.22)
                .setBeta(0.44);
        highchart.getTitle()
                .setText("My first demo")
                .setAlign(Alignment.CENTER);
        highchart.getExporting()
                .setEnabled(false);
        highchart.addSeries(TestUtils.<LineSeries>generateSeries("Demo 1", SeriesType.LINE, 25))
                .addSeries(TestUtils.<LineSeries>generateSeries("Demo 2", SeriesType.LINE, 25));

        String jsonVersion = highchart.toJson();

        assertThat(jsonVersion, Matchers.containsString(CHART_STRING));
        assertThat(jsonVersion, Matchers.containsString(EXPORTING_STRING));
    }

}
