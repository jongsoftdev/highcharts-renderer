package com.jongsoft.highchart;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jongsoft.highchart.axis.Axis;
import com.jongsoft.highchart.axis.AxisEvents;
import com.jongsoft.highchart.chart.style.Margin;
import com.jongsoft.highchart.common.Alignment;
import com.jongsoft.highchart.common.Function;
import com.jongsoft.highchart.common.GraphColor;
import com.jongsoft.highchart.common.SeriesType;
import com.jongsoft.highchart.series.LineSeries;
import com.jongsoft.highchart.series.ScatterSeries;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class HighchartTest {

    @Test
    @SuppressWarnings("unchecked")
    public void chartTest() throws JsonProcessingException {
        // @formatter:off
        Highchart highchart = new Highchart();
        highchart
            .getChart()
                .setAlignTicks(true)
                .setAnimation(false)
                .setHeight(500)
                .setWidth(600)
                .setMargin(new Margin(15, 20))
                .getStyle()
                    .css("color", new GraphColor("#ffffff"))
                    .build()
                .build()
            .getCredits()
                .setEnabled(true)
                .setHref("http://www.google.com")
                .setText("My credit line")
                .build()
            .getAccessibility()
                .getKeyboardNavigation()
                    .setEnabled(true)
                    .build()
                .setEnabled(true)
                .setPointDateFormatter(new Function.Point("test"));
        // @formatter:on

        highchart.addSeries(TestUtils.<LineSeries>generateSeries("Demo 1", SeriesType.LINE, 25))
                .addSeries(TestUtils.<LineSeries>generateSeries("Demo 2", SeriesType.LINE, 25));

        String jsonVersion = highchart.toJson();

        assertThat(jsonVersion, containsString("\"credits\" : {" + System.lineSeparator() +
                "    \"enabled\" : true," + System.lineSeparator() +
                "    \"href\" : \"http://www.google.com\"," + System.lineSeparator() +
                "    \"text\" : \"My credit line\"" + System.lineSeparator() +
                "  }"));
    }

    @Test
    public void formatterTest() throws JsonProcessingException {
        Highchart highchart = new Highchart();
        // @formatter:off
        highchart
            .getChart()
                .setAlignTicks(true)
                .setAnimation(false)
                .setHeight(500)
                .setWidth(600)
                .setMargin(new Margin(15, 20))
                .getStyle()
                    .css("color", new GraphColor("#ffffff"))
                    .build()
                .build()
            .getCredits()
                .setEnabled(false)
                .build()
            .addXAxis(new Axis()
                .getTitle()
                    .setText("My Axis")
                    .build()
                .setEvents(new AxisEvents().setAfterBreaks(new Function("return y")))
                .setMin(0));
        // @formatter:on

        LineSeries series = TestUtils.generateSeries("Demo 1", SeriesType.LINE, 50);
        series.getDataLabels().setFormatter(new Function.Formatter("return this.point.name"));
        highchart.addSeries(series);

        String jsonVersion = highchart.toJson();
        assertThat(jsonVersion, containsString("\"data\" : [ {"));
    }

    @Test
    public void maxPointLimitExeededTest() throws JsonProcessingException {
        Highchart highchart = new Highchart();
        highchart.getChart()
                .setAlignTicks(true)
                .setAnimation(false)
                .setHeight(500)
                .setWidth(600)
                .setMargin(new Margin(15, 20))
                .getStyle()
                .css("color", new GraphColor("#ffffff"))
                .build()
                .build()
                .getCredits()
                .setEnabled(false);
        ScatterSeries series = TestUtils.generateSeries("Demo 1", SeriesType.SCATTER, 10000);
        series.setShowInLegend(false);
        series.getDataLabels().setFormatter(new Function.Formatter("return this.point.name"));
        highchart.addSeries(series);

        String jsonVersion = highchart.toJson();
        assertThat(jsonVersion, containsString("\"data\" : [ ["));
        assertThat(jsonVersion, containsString("\"showInLegend\" : false"));
    }
}
