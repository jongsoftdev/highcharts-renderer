package com.jongsoft.highchart.axis;

import com.jongsoft.highchart.common.Alignment;
import com.jongsoft.highchart.serializer.JsonUtils;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class AxisLabelsTest {

    @Test
    public void labelFormatter() {
        String json = JsonUtils.serialize(new AxisLabels<>(null)
                .setFormat("{point.y:.2f}"));

        assertThat(json, containsString("\"format\" : \"{point.y:.2f}\""));
    }

    @Test
    public void formatting() {
        String json = JsonUtils.serialize(new AxisLabels<>(null)
                .setAlign(Alignment.LEFT)
                .setAutoRotationLimit(10)
                .setDistance(2)
                .setPadding(3)
                .setRotation(170));

        assertThat(json, containsString("\"align\" : \"left\""));
        assertThat(json, containsString("\"autoRotationLimit\" : 10"));
        assertThat(json, containsString("\"distance\" : 2"));
        assertThat(json, containsString("\"padding\" : 3"));
        assertThat(json, containsString("\"rotation\" : 170"));
    }
}
