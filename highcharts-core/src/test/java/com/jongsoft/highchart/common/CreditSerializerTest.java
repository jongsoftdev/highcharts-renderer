package com.jongsoft.highchart.common;

import com.jongsoft.highchart.serializer.JsonUtils;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

public class CreditSerializerTest {

    @Test
    public void withPosition() {
        Credits<Object> credits = new Credits<>(null)
                .setEnabled(true)
                .getPosition()
                .setAlign(Alignment.CENTER)
                .build();

        String jsonString = JsonUtils.serialize(credits);

        assertThat(jsonString, containsString("\"align\" : \"center\""));
    }
}
