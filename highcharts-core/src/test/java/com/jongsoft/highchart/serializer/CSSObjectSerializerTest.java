package com.jongsoft.highchart.serializer;

import com.jongsoft.highchart.common.CSSObject;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class CSSObjectSerializerTest {

    @Test
    public void empty() {
        String serialized = JsonUtils.serialize(new CSSObject<>(null));

        assertThat(serialized, equalTo("null"));
    }

    @Test
    public void cssStyles() {
        String style = JsonUtils.serialize(new CSSObject<>(null)
            .css("color", "white")
            .css("border", "1px solid black"));

        assertThat(style, containsString("\"color\" : \"white\""));
        assertThat(style, containsString("\"border\" : \"1px solid black\""));
    }
}
