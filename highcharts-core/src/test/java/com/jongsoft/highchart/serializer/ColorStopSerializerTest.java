package com.jongsoft.highchart.serializer;

import com.jongsoft.highchart.axis.ColorAxis;
import com.jongsoft.highchart.common.GraphColor;
import org.junit.Test;


import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class ColorStopSerializerTest {

    @Test
    public void serialize() {
        String serialized = JsonUtils.serialize(new ColorAxis.ColorStop(23, new GraphColor(Color.cyan)));
        assertThat(serialized, equalTo("[ 23, \"#00ffff\" ]"));
    }

    @Test
    public void serializeList() {
        List<ColorAxis.ColorStop> stops = new ArrayList<>();
        stops.add(new ColorAxis.ColorStop(0, new GraphColor(Color.black)));
        stops.add(new ColorAxis.ColorStop(25, new GraphColor(Color.darkGray)));
        stops.add(new ColorAxis.ColorStop(50, new GraphColor(Color.gray)));
        stops.add(new ColorAxis.ColorStop(75, new GraphColor(Color.lightGray)));
        stops.add(new ColorAxis.ColorStop(100, new GraphColor(Color.white)));

        String serialized = JsonUtils.serialize(stops);
        assertThat(serialized, equalTo("[ [ 0, \"#000000\" ], [ 25, \"#404040\" ], [ 50, \"#808080\" ], [ 75, \"#c0c0c0\" ], [ 100, \"#ffffff\" ] ]"));
    }

}
