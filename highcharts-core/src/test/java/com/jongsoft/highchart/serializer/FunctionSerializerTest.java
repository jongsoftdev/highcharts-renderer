package com.jongsoft.highchart.serializer;

import com.jongsoft.highchart.common.Function;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class FunctionSerializerTest {

    @Test
    public void function() {
        String serialized = JsonUtils.serialize(new Function("console.log(1)"));

        assertThat(serialized, equalTo("function () {" + System.lineSeparator() +
                "\tconsole.log(1);" + System.lineSeparator() +
                "}"));
    }

    @Test
    public void event() {
        String serialized = JsonUtils.serialize(new Function.Event("console.log(1)"));

        assertThat(serialized, equalTo("function (event) {" + System.lineSeparator() +
                "\tconsole.log(1);" + System.lineSeparator() +
                "}"));
    }

    @Test
    public void positioner() {
        String serialized = JsonUtils.serialize(new Function.Positioner("console.log(1)"));

        assertThat(serialized, equalTo("function (labelWidth, labelHeight, point) {" + System.lineSeparator() +
                "\tconsole.log(1);" + System.lineSeparator() +
                "}"));
    }
}
