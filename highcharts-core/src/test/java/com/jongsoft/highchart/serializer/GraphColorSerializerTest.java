package com.jongsoft.highchart.serializer;

import com.jongsoft.highchart.common.GraphColor;
import org.junit.Test;

import java.awt.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class GraphColorSerializerTest {

    @Test
    public void empty() {
        assertThat(JsonUtils.serialize(new GraphColor("")), equalTo("null"));
    }

    @Test
    public void whiteColor() {
        assertThat(JsonUtils.serialize(new GraphColor(Color.white)), equalTo("\"#ffffff\""));
    }

}
