package com.jongsoft.highchart.serializer;

import com.jongsoft.highchart.chart.style.Margin;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class MarginSerializerTest {

    @Test
    public void all5() {
        assertThat(JsonUtils.serialize(new Margin(5)), equalTo("[ 5, 5, 5, 5 ]"));
    }

    @Test
    public void twoByFive() {
        assertThat(JsonUtils.serialize(new Margin(2, 5)), equalTo("[ 2, 5, 2, 5 ]"));
    }

    @Test
    public void allSet() {
        assertThat(JsonUtils.serialize(new Margin(2, 5, 3, 6)), equalTo("[ 3, 5, 6, 2 ]"));
    }
}
