package com.jongsoft.highchart.serializer;

import com.jongsoft.highchart.common.SeriesType;
import com.jongsoft.highchart.plotoptions.PlotOptions;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class PlotOptionSerializerTest {

    @Test
    public void empty() {
        assertThat(JsonUtils.serialize(new PlotOptions()), equalTo("null"));
    }

    @Test
    public void baseSettings() {
        PlotOptions plotOptions = new PlotOptions();
        plotOptions.getSeries()
                .setAllowPointSelect(true)
                .setAnimation(false);

        String json = JsonUtils.serialize(plotOptions);
        assertThat(json, equalTo("{" + System.lineSeparator() +
                "  \"series\" : {" + System.lineSeparator() +
                "    \"allowPointSelect\" : true," + System.lineSeparator() +
                "    \"animation\" : false" + System.lineSeparator() +
                "  }" + System.lineSeparator() +
                "}"));
    }

    @Test
    public void lineSettings() {
        PlotOptions plotOptions = new PlotOptions();
        plotOptions.getOptions(SeriesType.LINE)
                .setAnimation(false)
                .setId("line");

        String json = JsonUtils.serialize(plotOptions);
        assertThat(json, equalTo("{" + System.lineSeparator() +
                "  \"line\" : {" + System.lineSeparator() +
                "    \"animation\" : false," + System.lineSeparator() +
                "    \"id\" : \"line\"," + System.lineSeparator() +
                "    \"type\" : \"line\"" + System.lineSeparator() +
                "  }" + System.lineSeparator() +
                "}"));
    }
}
