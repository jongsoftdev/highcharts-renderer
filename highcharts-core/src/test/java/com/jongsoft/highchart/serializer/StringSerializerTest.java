package com.jongsoft.highchart.serializer;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class StringSerializerTest {

    @Test
    public void nullSerialize() {
        String nullString = null;
        assertThat(JsonUtils.serialize(nullString), equalTo("null"));
    }

    @Test
    public void stringSerialize() {
        assertThat(JsonUtils.serialize("myText"), equalTo("\"myText\""));
    }

}
