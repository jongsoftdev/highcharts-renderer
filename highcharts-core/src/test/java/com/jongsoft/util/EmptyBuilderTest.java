package com.jongsoft.util;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class EmptyBuilderTest {

    @Test
    public void add() {
        assertThat(new EmptyBuilder().add(null).build(), equalTo(true));
        assertThat(new EmptyBuilder().add(null).add("test").build(), equalTo(false));
        assertThat(new EmptyBuilder().add("true").build(), equalTo(false));
    }
}