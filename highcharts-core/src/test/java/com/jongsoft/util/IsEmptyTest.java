package com.jongsoft.util;


import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class IsEmptyTest {

    @Test
    public void isEmpty() {
        assertThat(IsEmpty.isEmpty(null, null), equalTo(true));
        assertThat(IsEmpty.isEmpty("test", null), equalTo(false));
    }

}