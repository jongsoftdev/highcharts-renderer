# Highchart Export Module
![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.jongsoft.utils/highchart-export-server/badge.svg)

The Highchart Export module includes:

* A render engine that will sent the Highchart POJO to an export server to generate a graph PNG file
* Object pooling support to enable large batch processing of POJO instances at once

## Maven dependency information
The module is published in Maven Central for each release.

### Maven
```xml
<dependency>
  <groupId>com.jongsoft.utils</groupId>
  <artifactId>highchart-export-server</artifactId>
  <version>2.2.3</version>
</dependency>
```

### Gradle
```
compile group: 'com.jongsoft.utils', name: 'highchart-export-server', version: '2.2.3'
```

### SBT
```
libraryDependencies += "com.jongsoft.utils" % "highchart-export-server" % "2.2.3"
```

## Code example

```java
ExportApiRenderer imageRenderer = new ExportApiRenderer();

FileOutputStream out = new FileOutputStream("/render-output/exportserver-testRender.png");
Highchart chartOptions = new Highchart();

imageRenderer.setRenderObject(chartOptions);
imageRenderer.setOutputStream(out);
try {
    imageRenderer.render();
} catch (RenderException e) {
    e.printStackTrace();
}

out.close();
```

This example will create a new file located at `/render-output/exportserver-testRender.png` containing the rendered image.

#### Configuring the Export Server module

System property | Default Value | Description
--------------- | ------------- | ------------
`highcharts.exportServer` | `http://export.highcharts.com/` | The location of the Export Server that is called
`highcharts.export.poolsize` | 15  | The size of the default Export Render driver pool
