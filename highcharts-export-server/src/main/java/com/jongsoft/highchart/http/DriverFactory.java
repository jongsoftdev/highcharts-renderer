package com.jongsoft.highchart.http;

import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClientBuilder;

public class DriverFactory implements PooledObjectFactory<HttpClient> {

    private static final int DEFAULT_CONNECTION_TIMEOUT = 5000;

    @Override
    public PooledObject<HttpClient> makeObject() throws Exception {
        return new DefaultPooledObject<>(HttpClientBuilder.create()
                .setDefaultRequestConfig(RequestConfig.custom()
                        .setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT)
                        .build())
                .disableAuthCaching()
                .build());
    }

    @Override
    public void destroyObject(PooledObject<HttpClient> pooledObject) throws Exception {
        // nothing to do
    }

    @Override
    public boolean validateObject(PooledObject<HttpClient> pooledObject) {
        return true;
    }

    @Override
    public void activateObject(PooledObject<HttpClient> pooledObject) throws Exception {
        // nothing to do
    }

    @Override
    public void passivateObject(PooledObject<HttpClient> pooledObject) throws Exception {
        // nothing to do
    }

}
