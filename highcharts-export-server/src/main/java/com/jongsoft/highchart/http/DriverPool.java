package com.jongsoft.highchart.http;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.http.client.HttpClient;

public class DriverPool extends GenericObjectPool<HttpClient> {

    DriverPool() {
        super(new DriverFactory());

        setMaxTotal(ExportServerConfiguration.getDefaultPoolSize());
        setBlockWhenExhausted(true);
    }

}
