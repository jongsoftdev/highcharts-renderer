package com.jongsoft.highchart.http;

/**
 * The {@link DriverPoolFactory} allows the managing of {@link DriverPool} instances. It allows for easy creation
 * of new driver pools. Or using the singleton driver pool contained within.
 */
public class DriverPoolFactory {
    private static final DriverPool INSTANCE = new DriverPool();

    private DriverPoolFactory() {
        // should never be called
    }

    /**
     * Get a driver pool to manage the {@link com.jongsoft.highchart.render.ExportApiRenderer} instances.
     * <p>
     * <b>Note:</b> if you create a new driver pool you are responsible for cleaning it and storing it.
     *
     * @param newInstance   if true a new driver pool will be created and returned, if false the internal driver
     *                      pool will be used
     * @return  an instance of the {@link DriverPool} for getting {@link com.jongsoft.highchart.render.ExportApiRenderer}
     *          instances
     */
    public static DriverPool getDriverPool(boolean newInstance) {
        if (newInstance) {
            return new DriverPool();
        }

        return INSTANCE;
    }

}
