package com.jongsoft.highchart.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExportServerConfiguration {
    public static final String EXPORT_SERVER_URI = "highcharts.exportServer";
    public static final String DEFAULT_EXPORT_SERVER_POOLSIZE = "highcharts.export.poolsize";

    private static final Logger LOG = LoggerFactory.getLogger(ExportServerConfiguration.class);

    private ExportServerConfiguration() {
        // Hidden constructor for utility class
    }

    public static final int getDefaultPoolSize() {
        try {
            return Integer.valueOf(System.getProperty(DEFAULT_EXPORT_SERVER_POOLSIZE, "15"));
        } catch (NumberFormatException ne) {
            LOG.error("Invalid configuration for {} provided, falling back to default 15", System.getProperty(DEFAULT_EXPORT_SERVER_POOLSIZE));
        }

        return 15;
    }

    public static final String getExportServerUri() {
        return System.getProperty(EXPORT_SERVER_URI, "http://export.highcharts.com/");
    }
}
