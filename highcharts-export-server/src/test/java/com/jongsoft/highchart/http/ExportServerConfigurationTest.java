package com.jongsoft.highchart.http;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ExportServerConfigurationTest {

    @Before
    public void setup() {
        System.clearProperty(ExportServerConfiguration.DEFAULT_EXPORT_SERVER_POOLSIZE);
        System.clearProperty(ExportServerConfiguration.EXPORT_SERVER_URI);
    }

    @Test
    public void defaultPoolSize() {
        assertThat(ExportServerConfiguration.getDefaultPoolSize(), equalTo(15));
    }

    @Test
    public void customPoolSize() {
        System.setProperty(ExportServerConfiguration.DEFAULT_EXPORT_SERVER_POOLSIZE, "200");
        assertThat(ExportServerConfiguration.getDefaultPoolSize(), equalTo(200));
    }

    @Test
    public void invalidPoolSize() {
        System.setProperty(ExportServerConfiguration.DEFAULT_EXPORT_SERVER_POOLSIZE, "naha");
        assertThat(ExportServerConfiguration.getDefaultPoolSize(), equalTo(15));
    }

    @Test
    public void defaultExportServer() {
        assertThat(ExportServerConfiguration.getExportServerUri(), equalTo("http://export.highcharts.com/"));
    }

    @Test
    public void customExportServer() {
        System.setProperty(ExportServerConfiguration.EXPORT_SERVER_URI, "my-url-com");
        assertThat(ExportServerConfiguration.getExportServerUri(), equalTo("my-url-com"));
    }

}
