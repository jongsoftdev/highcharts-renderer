package com.jongsoft.highchart.render;

import com.jongsoft.highchart.Highchart;
import com.jongsoft.highchart.common.Alignment;
import com.jongsoft.highchart.common.SeriesType;
import com.jongsoft.highchart.series.LineSeries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExportApiRendererTest {
    
    @BeforeClass
    public static void setUp() {
        new File("target/render-output").mkdir();
    }

    @Test
    public void testRender() throws IOException {
        ExportApiRenderer imageRenderer = new ExportApiRenderer();

        FileOutputStream out = new FileOutputStream("target/render-output/exportserver-testRender.png");
        Highchart chartOptions = createChart();

        imageRenderer.setRenderObject(chartOptions);
        imageRenderer.setOutputStream(out);
        try {
            imageRenderer.render();
        } catch (RenderException e) {
            e.printStackTrace();
        }

        out.close();
    }

    private Highchart createChart() {
        Highchart chartOptions = new Highchart();
        chartOptions
                .getChart()
                .setWidth(600)
                .setHeight(400)
                .build()
                .getCredits()
                .setText("Sample application")
                .setHref("http://www.example.com")
                .build()
                .getExporting()
                .setEnabled(false);
        chartOptions.getTitle()
                .setText("Demo Highchart line graph")
                .setAlign(Alignment.CENTER);
        chartOptions
                .getCredits()
                .setEnabled(false);
        chartOptions.addSeries(TestUtils.<LineSeries>generateSeries("Series 1", SeriesType.LINE, 50));
        return chartOptions;
    }

}
