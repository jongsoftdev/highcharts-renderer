package com.jongsoft.highchart.render;

import com.jongsoft.highchart.common.SeriesType;
import com.jongsoft.highchart.series.Series;
import com.jongsoft.highchart.series.SeriesFactory;
import com.jongsoft.highchart.series.SeriesPoint;

import java.util.Random;

import static com.jongsoft.highchart.common.SeriesType.COLUMN;
import static com.jongsoft.highchart.common.SeriesType.LINE;

public class TestUtils {

    public static <T extends Series<T>> T generateSeries(String s, SeriesType type, int pointLimit) {
        T series = SeriesFactory.createSeries(type);
        series.setAnimation(false);
        series.setName(s);

        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < pointLimit; i++) {
            if (type == LINE || type == COLUMN)
                series.addPoint(new SeriesPoint().setX(i).setY(random.nextDouble() * pointLimit));
            else 
                series.addPoint(new SeriesPoint().setX(random.nextDouble() * 45 - 10).setY(random.nextDouble() * 1000));
        }

        return series;
    }
}
