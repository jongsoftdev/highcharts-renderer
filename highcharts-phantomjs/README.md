# Highchart PhantomJS module
![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.jongsoft.utils/highchart-phantomjs/badge.svg)

When you desire to use charts in PDF or as static images you can also opt to use the phantom js implementation that
is provided with this library to generate PNG files. To do so you will need to use the `PhantomJSRenderer` class, 
providing it with an `OutputStream` to write the image to.

## Prerequisites

* Install the required modules as indicated on [PhantomJs Installation Guide](http://phantomjs.org/download.html)
* Install the ghostscript fonts (Debian: gsfonts, CentOs: ghostscript-fonts)

## Maven dependency information
The module is published in Maven Central for each release.

### Maven
```xml
<dependency>
  <groupId>com.jongsoft.utils</groupId>
  <artifactId>highchart-phantomjs</artifactId>
  <version>2.2.4</version>
</dependency>
```

### Gradle
```
compile group: 'com.jongsoft.utils', name: 'highchart-phantomjs', version: '2.2.3'
```

### SBT
```
libraryDependencies += "com.jongsoft.utils" % "highchart-phantomjs" % "2.2.3"
```

## Code Example
```java
PhantomJSRenderer imageRenderer = new PhantomJSRenderer();
FileOutputStream out = new FileOutputStream("/render-output/phantom-testRender.png");
imageRenderer.setOutputStream(out);

// Fill this highchart object with all desired settings
Highchart highchart = new Highchart();
imageRenderer.setRenderObject(highchart);

// Perform the render operation in a try-catch as it may throw an exception when rendering
try {
    imageRenderer.render();
} catch (RenderException e) {
    // Do something usefull with the exception thrown
}
```

The code above will create a file in the location `/render-output/phantom-testRender.png` with the screenshot it took of the chart using phantom js.

#### Configuring the PhantomJS module

System property | Default Value | Description
--------------- | ------------- | ------------
`highcharts.exportDir` | `java.io.tmpdir` | Allows for overriding of the location where the JavaScript files are extracted to
`highcharts.phantomjs.poolsize` | 15  | The size of the default PhantomJS driver pool