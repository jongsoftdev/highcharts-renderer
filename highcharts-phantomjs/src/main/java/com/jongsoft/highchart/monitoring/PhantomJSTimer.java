package com.jongsoft.highchart.monitoring;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class PhantomJSTimer extends TimingLogger {

    @Override
    @Around("@within(com.jongsoft.highchart.monitoring.Monitoring) || @annotation(com.jongsoft.highchart.monitoring.Monitoring)")
    public Object methodTimer(final ProceedingJoinPoint pjp) throws Throwable {
        return super.methodTimer(pjp);
    }

}
