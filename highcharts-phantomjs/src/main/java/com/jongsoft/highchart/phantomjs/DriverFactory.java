/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.phantomjs;

import com.jongsoft.Startable;
import com.jongsoft.highchart.monitoring.Monitoring;
import net.anthavio.phanbedder.Phanbedder;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.charset.Charset;

/**
 * The DriverFactory allows for creating {@link PhantomJSDriver} instances that are wrapped in an {@link PooledObject}.
 * <p>
 * During startup of the factory it will unpack the bundled PhantomJS web driver into the <code>java.io.tmp</code>
 * directory. This web driver binary will then be used for each {@link PhantomJSDriver} that is instantiated using this
 * factory.
 */
public class DriverFactory implements PooledObjectFactory<TimedPhantomJsDriver>, Startable {

    private static final Logger LOG = LoggerFactory.getLogger(DriverFactory.class);

    private final DesiredCapabilities seleniumCapabilities;
    private Status status;
    private String blankPath;

    public DriverFactory() {
        File phantomBinary = Phanbedder.unpack();
        seleniumCapabilities = new DesiredCapabilities();
        seleniumCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, phantomBinary.getAbsolutePath());
        seleniumCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {
                "--webdriver-loglevel=TRACE",
                "--script-encoding=" + Charset.defaultCharset()
        });
        status = Status.IDLE;
        
        blankPath = DriverPoolFactory.getRenderUri();
    }

    @Override
    @Monitoring(level = Monitoring.Level.TRACE)
    public boolean start() {
        status = Status.RUNNING;
        return true;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    @Monitoring(level = Monitoring.Level.TRACE)
    public void stop() {
        status = Status.IDLE;
    }

    @Override
    @Monitoring(level = Monitoring.Level.WARN)
    public PooledObject<TimedPhantomJsDriver> makeObject() throws Exception {
        LOG.info("Initializing new phantom js driver");
        return new DefaultPooledObject<>(new TimedPhantomJsDriver(seleniumCapabilities));
    }

    @Override
    @Monitoring(level = Monitoring.Level.WARN)
    public void destroyObject(PooledObject<TimedPhantomJsDriver> p) throws Exception {
        if (p.getObject() != null) {
            LOG.info("Destroying phantom js driver");
            p.getObject().close();
            p.getObject().quit();
        }
    }

    @Override
    @Monitoring(level = Monitoring.Level.INFO)
    public boolean validateObject(PooledObject<TimedPhantomJsDriver> p) {
        try {
            Long exitCode = (Long) p.getObject().executePhantomJS("return 1;");
            LOG.trace("Validated phantom js driver with exitCode {}", exitCode);
            return 1l == exitCode;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @Monitoring(level = Monitoring.Level.INFO)
    public void activateObject(PooledObject<TimedPhantomJsDriver> p) throws Exception {
        p.getObject().get(DriverPoolFactory.getRenderUri());
        LOG.trace("Resetting page to the blank page to: {}", blankPath);
    }

    @Override
    public void passivateObject(PooledObject<TimedPhantomJsDriver> p) throws Exception {
        p.getObject().get(DriverPoolFactory.getRenderUri());
    }
}
