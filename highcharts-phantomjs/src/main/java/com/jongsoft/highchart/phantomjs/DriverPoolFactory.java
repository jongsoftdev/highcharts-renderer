/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.phantomjs;

import com.jongsoft.HighchartModules;
import org.apache.commons.io.IOUtils;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;

public class DriverPoolFactory {

    private static final Logger LOG = LoggerFactory.getLogger(DriverPoolFactory.class);
    
    private static final DriverPool INSTANCE = new DriverPool();
    
    static {
        new DriverPoolFactory();
    }

    private DriverPoolFactory() {
        // Extract the javascript files into a tmp directory
        ensureUnpackLocation();

        for (HighchartModules javascriptFile : HighchartModules.values()) {
            try (InputStream resourceAsStream = DriverPoolFactory.class.getClassLoader().getResourceAsStream("javascript/" + javascriptFile)) {
                if (resourceAsStream != null) {
                    LOG.trace("Expanding file {} to temporary directory.", javascriptFile);
                    IOUtils.copy(resourceAsStream, new FileOutputStream(DriverPoolFactory.getJavaScriptLocation() + javascriptFile));
                }
            } catch (IOException e) {
                LOG.error(format("Unable to unpack file %s to target location %s", javascriptFile, getJavaScriptLocation()), e);
            }
        }

        List<String> extractWebRoots = new ArrayList<>();
        extractWebRoots.add("render.html");

        for (String fontSet : Arrays.asList("opensans")) {
            new Reflections("wwwroot." + fontSet, new ResourcesScanner())
                    .getResources(x -> true)
                    .forEach(extractWebRoots::add);
        }

        for (String file : extractWebRoots) {
            try (InputStream renderPage = DriverPoolFactory.class.getClassLoader().getResourceAsStream("wwwroot/" + file)) {
                if (renderPage != null) {
                    LOG.trace("Unpacking file {} to temporary directory", file);
                    IOUtils.copy(renderPage, new FileOutputStream(DriverPoolFactory.getWebRoot() + file));
                }
            } catch (IOException e) {
                LOG.error(format("Unable to unpack file %s to target location %s", file, DriverPoolFactory.getWebRoot()), e);
            }
        }


        int expandedFiles = new File(getJavaScriptLocation()).list().length;
        if (expandedFiles != HighchartModules.values().length) {
            LOG.error("Some JavaScript files might not have been extracted properly expected {} files but got {} files",
                    expandedFiles, HighchartModules.values().length);
        }
    }

    private boolean ensureUnpackLocation() {
        LOG.trace("Creating temporary directory for expanding the files to");
        File tempDirectory = new File(DriverPoolFactory.getExpansionDir());
        if (!tempDirectory.exists() && !tempDirectory.mkdirs()) {
            LOG.error("Could not create the root temporary directory for expanding");
            return false;
        }

        File javaScriptDir = new File(DriverPoolFactory.getJavaScriptLocation());
        if (!javaScriptDir.exists() && !javaScriptDir.mkdirs()) {
            LOG.error("Could not create the javascript temporary directory for expanding");
            return false;
        }

        File webRootDir = new File(DriverPoolFactory.getWebRoot());
        if (!webRootDir.exists() && !webRootDir.mkdirs()) {
            LOG.error("Could not create the web root temporary directory for expanding");
            return false;
        }

        File openSansDir = new File(DriverPoolFactory.getWebRoot() + "opensans");
        if (!openSansDir.exists() && !openSansDir.mkdirs()) {
            LOG.error("Could not create the web root temporary directory for expanding");
            return false;
        }

        return true;
    }
    
    private static String getExpansionDir() {
        return PhantomJSConfiguration.getExportDir() + "highchart-renderer/";
    }

    public static String getJavaScriptLocation() {
        return getExpansionDir() + "javascript/";
    }

    public static String getWebRoot() {
        return getExpansionDir() + "wwwroot/";
    }

    public static final String getRenderUri() {
        return new File(DriverPoolFactory.getWebRoot() + "render.html").toURI().toString();
    }

    public static DriverPool getDriverPool(boolean newInstance) {
        if (newInstance) {
            return new DriverPool();
        }

        return INSTANCE;
    }

}
