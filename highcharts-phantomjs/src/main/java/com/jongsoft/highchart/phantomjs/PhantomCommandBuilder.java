/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.phantomjs;

import com.jongsoft.HighchartModules;

/**
 * The PhantomCommandBuilder will construct a JavaScript command that can be sent to a {@link org.openqa.selenium.phantomjs.PhantomJSDriver}. This will
 * always return a {@link java.util.Map} with the keys below:
 *
 * <ul>
 *     <li>exitCode: containing the health of the execution, where 0 is successful and all other codes indicate an error</li>
 *     <li>exception: a {@link java.util.List} of messages with exception occurred during execution</li>
 * </ul>
 */
public class PhantomCommandBuilder {
    public static final Long CODE_INJECT_JS_FAILED = 1l;
    public static final Long CODE_SUCCESS = 10l;

    private final StringBuilder phantomCommand = new StringBuilder();

    public PhantomCommandBuilder() {
        phantomCommand.append("var responseObject = {exitCode: 0, libraries: [], exception: [], console: {log: []}};")
                .append("phantom.onError = function(msg, trace) {").append(System.lineSeparator())
                .append("   var exception = {message: msg, trace: []};").append(System.lineSeparator())
                .append("   if (trace && trace.length) {").append(System.lineSeparator())
                .append("       trace.forEach(function(t) { ").append(System.lineSeparator())
                .append("           var trace = (t.file || t.sourceURL) + ':' + t.line + (t.function ? ' in function ' + t.function : '');").append(System.lineSeparator())
                .append("           exception.trace.push(trace);").append(System.lineSeparator())
                .append("       });").append(System.lineSeparator())
                .append("   }").append(System.lineSeparator())
                .append("   responseObject.exception.push(exception);").append(System.lineSeparator())
                .append("}").append(System.lineSeparator())
                .append("phantom.onConsoleMessage = function (msg, lineNum, sourceId) {")
                .append("   responseObject.console.log.push({source: sourceId, line: lineNum, message: msg});")
                .append("};")
                .append("var page = this;").append(System.lineSeparator());
    }

    public PhantomCommandBuilder libraryPath(String path) {
        phantomCommand.append("page.libraryPath = '").append(path).append("';").append(System.lineSeparator());
        return this;
    }

    public PhantomCommandBuilder includeJs(String uri) {
        phantomCommand.append("{var lib = {path: '").append(uri).append("', loaded: false};");
        phantomCommand.append("page.includeJs('").append(uri).append("', function() { lib.loaded = true; });");
        phantomCommand.append("responseObject.libraries.push(lib); }").append(System.lineSeparator());
        return this;
    }

    public PhantomCommandBuilder evaluate(String toEvaluate) {
        phantomCommand.append("page.evaluate(function() {")
                .append(toEvaluate)
                .append("});");
        return this;
    }

    public PhantomCommandBuilder injectJs(HighchartModules module) {
        return injectJs(module.toString());
    }

    public PhantomCommandBuilder injectJs(String fileName) {
        phantomCommand.append("if (!page.injectJs('").append(fileName).append("')) {").append(System.lineSeparator());
        phantomCommand.append("  responseObject.exitCode = ").append(CODE_INJECT_JS_FAILED).append(";").append(System.lineSeparator());
        phantomCommand.append("  phantom.onError('Failed to load javascript file ").append(fileName).append("', null);").append(System.lineSeparator());
        phantomCommand.append("} else {").append(System.lineSeparator());
        phantomCommand.append(" responseObject.libraries.push('").append(fileName).append("');").append(System.lineSeparator());
        phantomCommand.append("}").append(System.lineSeparator());
        return this;
    }

    public PhantomCommandBuilder appendScript(String script) {
        phantomCommand.append(script).append(System.lineSeparator());
        return this;
    }

    public String build() {
        return phantomCommand.toString() + " return responseObject;";
    }
}
