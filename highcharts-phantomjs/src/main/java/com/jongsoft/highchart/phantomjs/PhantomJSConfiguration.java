package com.jongsoft.highchart.phantomjs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class PhantomJSConfiguration {
    private static final Logger LOG = LoggerFactory.getLogger(PhantomJSConfiguration.class);

    public static final String EXPORT_DIR = "highcharts.exportDir";
    public static final String DEFAULT_PHANTOMJS_POOLSIZE = "highcharts.phantomjs.poolsize";

    private PhantomJSConfiguration() {
    }

    public static final int getDefaultPoolSize() {
        try {
            return Integer.valueOf(System.getProperty(DEFAULT_PHANTOMJS_POOLSIZE, "15"));
        } catch (NumberFormatException ne) {
            LOG.error("Invalid configuration for {} provided, falling back to default 15", System.getProperty(DEFAULT_PHANTOMJS_POOLSIZE));
        }

        return 15;
    }

    public static final String getExportDir() {
        String javaIoTmp = System.getProperty("java.io.tmpdir").replaceAll("\\\\", "/");
        String tempDirectory = System.getProperty(EXPORT_DIR, javaIoTmp).replaceAll("\\\\", "/");

        File tempLocation = new File(tempDirectory);
        if (!tempLocation.isDirectory()) {
            LOG.error("The PhantomJs export directory '{}' provided is not an actual directory, falling back to java.io.tmpdir", tempDirectory);
            return javaIoTmp;
        }

        return tempDirectory + (tempDirectory.endsWith("/") ? "" : "/");
    }
}
