/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.phantomjs;

import com.jongsoft.highchart.monitoring.Monitoring;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.jongsoft.highchart.monitoring.Monitoring.Level.INFO;

public class TimedPhantomJsDriver extends PhantomJSDriver {
    
    private static final Logger LOG = LoggerFactory.getLogger(TimedPhantomJsDriver.class);

    public TimedPhantomJsDriver(Capabilities desiredCapabilities) {
        super(desiredCapabilities);
    }
    
    @Override
    @Monitoring(level = INFO)
    public Object executePhantomJS(String script, Object... args) {
        LOG.trace("====== Executing phantomJS script ====");
        LOG.trace(script);
        LOG.trace("======================================");
        return super.executePhantomJS(script, args); //To change body of generated methods, choose Tools | Templates.
    }
    
}
