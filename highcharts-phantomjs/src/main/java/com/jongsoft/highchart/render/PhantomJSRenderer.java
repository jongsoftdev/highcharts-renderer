/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.render;

import com.jongsoft.HighchartModules;
import com.jongsoft.highchart.AbstractHighchart;
import com.jongsoft.highchart.Highchart;
import com.jongsoft.highchart.monitoring.Monitoring;
import com.jongsoft.highchart.phantomjs.DriverPoolFactory;
import com.jongsoft.highchart.phantomjs.PhantomCommandBuilder;
import com.jongsoft.highchart.phantomjs.TimedPhantomJsDriver;
import com.jongsoft.highchart.serializer.JsonUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

import static com.jongsoft.highchart.monitoring.Monitoring.Level.TRACE;
import static java.lang.String.format;

/**
 * The {@link PhantomJSRenderer} allows for rendering the Highcharts graph server side. Using
 * PhantomJS as a embedded server taking a screenshot from the browser started by PhantomJS.
 */
public class PhantomJSRenderer implements Renderer<AbstractHighchart> {

    private static final Logger LOG = LoggerFactory.getLogger(PhantomJSRenderer.class);

    private Optional<AbstractHighchart> globalOptions = Optional.empty();
    private Optional<AbstractHighchart> chartOptions = Optional.empty();
    private Optional<OutputStream> outStream = Optional.empty();

    @Override
    @Monitoring(level = TRACE)
    public void render() {
        TimedPhantomJsDriver phantomJSDriver;

        validateRenderer();

        try {
            LOG.trace("Attempting to obtain a phantom js driver for rendering.");
            phantomJSDriver = DriverPoolFactory.getDriverPool(false).borrowObject();
        } catch (Exception e) {
            throw new RenderException("Unable to borrow phantom js driver from driver pool", e);
        }

        globalOptions.get().getChart().setRenderTo("svg-chart");
        globalOptions.get().getPlotOptions().getSeries().setAnimation(false);

        try {
            AbstractHighchart highchart = chartOptions.get();
            LOG.trace("Executing a phantom js script for the render.");
            Set<HighchartModules> modules = highchart.neededModules();
            PhantomCommandBuilder phantomCommand = new PhantomCommandBuilder()
                    .libraryPath(DriverPoolFactory.getJavaScriptLocation());

            modules.stream()
                    .sorted(Comparator.comparingInt(HighchartModules::getOrder))
                    .forEach(m -> phantomCommand.injectJs(m.toString()));

            phantomCommand.evaluate(format("Highcharts.setOptions(%s);", globalOptions.get().toJson()));
            phantomCommand.evaluate(format("new Highcharts.Chart(%s);", highchart.toJson()));

            Object response = phantomJSDriver.executePhantomJS(phantomCommand.build());
            processPhantomJS(response, phantomJSDriver);
        } catch (IOException | WebDriverException e) {
            LOG.error("Unable to save image to provided output stream", e);
            throw new RenderException("Unable to save image to output stream");
        } finally {
            DriverPoolFactory.getDriverPool(false).returnObject(phantomJSDriver);
        }
    }

    @Override
    public Renderer<AbstractHighchart> setRenderObject(AbstractHighchart chartOptions) {
        this.chartOptions = Optional.ofNullable(chartOptions);
        return this;
    }

    @Override
    public Renderer<AbstractHighchart> setGlobalOptions(AbstractHighchart chartOptions) {
        this.globalOptions = Optional.ofNullable(chartOptions);
        return this;
    }

    @Override
    public Renderer<AbstractHighchart> setOutputStream(OutputStream outputStream) {
        this.outStream = Optional.of(outputStream);
        return this;
    }

    private void validateRenderer() {
        if (!globalOptions.isPresent()) {
            LOG.trace("No global options provided, instantiate default settings.");
            globalOptions = Optional.of(new Highchart());
        }

        if (!outStream.isPresent()) {
            throw new RenderException("Cannot render highcharts when no outputstream is set");
        }

        if (!chartOptions.isPresent()) {
            throw new RenderException("Cannot render when no highcharts graph is provided");
        }
    }

    private void processPhantomJS(Object phantomResponse, PhantomJSDriver phantomJSDriver) throws IOException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("=================== Response ====================" + System.lineSeparator() + JsonUtils.serialize(phantomResponse));
        }
        if (phantomResponse instanceof Map) {
            Map realResponse = (Map) phantomResponse;
            if (realResponse.containsKey("exitCode")) {
                long exitCode = (long) realResponse.get("exitCode");
                List exceptions = (List) realResponse.get("exception");

                if (exitCode == 0 && exceptions.isEmpty()) {
                    outStream.get().write(phantomJSDriver.getScreenshotAs(OutputType.BYTES));
                    outStream.get().flush();
                } else {
                    throw new RenderException(format("Failed to process script exit code %s exceptions %s ",
                            exitCode, JsonUtils.serialize(exceptions)));
                }
            } else {
                throw new RenderException("Failed to process script, exception: " + JsonUtils.serialize(phantomResponse));
            }
        }
    }
}
