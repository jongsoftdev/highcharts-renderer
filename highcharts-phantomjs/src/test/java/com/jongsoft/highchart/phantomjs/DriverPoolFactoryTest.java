package com.jongsoft.highchart.phantomjs;

import com.jongsoft.HighchartModules;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class DriverPoolFactoryTest {

    @Test
    public void validatePath() {
        String extractionPath = DriverPoolFactory.getJavaScriptLocation();
        assertThat(extractionPath, containsString("highchart-renderer/javascript"));
    }

    @Test
    public void validateExtracted() {
        File extractionPath = new File(DriverPoolFactory.getJavaScriptLocation());

        assertThat(extractionPath.exists(), equalTo(true));
        assertThat(extractionPath.listFiles().length, equalTo(HighchartModules.values().length));
    }
}
