package com.jongsoft.highchart.phantomjs;

import com.jongsoft.HighchartModules;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class PhantomCommandBuilderTest {

    @Test
    public void emptyCommand() {
        String phantomCommand = new PhantomCommandBuilder()
                .build();

        assertThat(phantomCommand, containsString("var responseObject = {exitCode: 0, libraries: [], exception: [], console: {log: []}};"));
        assertThat(phantomCommand, containsString("responseObject.console.log.push({source: sourceId, line: lineNum, message: msg});"));
    }

    @Test
    public void libraryPath() {
        String phantomCommand = new PhantomCommandBuilder()
                .libraryPath("/tmp/library-path")
                .build();

        assertThat(phantomCommand, containsString("page.libraryPath = '/tmp/library-path';"));
    }

    @Test
    public void injectJs() {
        String phantomCommand = new PhantomCommandBuilder()
                .injectJs("test.js")
                .build();

        assertThat(phantomCommand, containsString("page.injectJs('test.js')"));
    }

    @Test
    public void injectJsModule() {
        String phantomCommand = new PhantomCommandBuilder()
                .injectJs(HighchartModules.EXPORT)
                .build();

        assertThat(phantomCommand, containsString("page.injectJs('highcharts-export.js')"));
    }
}
