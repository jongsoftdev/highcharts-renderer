/*
 * The MIT License
 *
 * Copyright 2016 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.render;

import com.jongsoft.highchart.HeatmapChart;
import com.jongsoft.highchart.axis.Axis;
import com.jongsoft.highchart.axis.AxisType;
import com.jongsoft.highchart.axis.ColorAxis;
import com.jongsoft.highchart.axis.Marker;
import com.jongsoft.highchart.common.Alignment;
import com.jongsoft.highchart.common.GraphColor;
import com.jongsoft.highchart.common.SeriesType;
import com.jongsoft.highchart.phantomjs.DriverPool;
import com.jongsoft.highchart.phantomjs.DriverPoolFactory;
import com.jongsoft.highchart.series.HeatMapSeries;
import com.jongsoft.highchart.series.SeriesFactory;
import com.jongsoft.highchart.series.SeriesPoint;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static java.time.LocalDateTime.of;
import static org.junit.Assert.fail;

public class ImageRenderHeatmapTest {
    final Random random = new Random(System.currentTimeMillis());

    @BeforeClass
    public static void setup() {
        final DriverPool driverPool = DriverPoolFactory.getDriverPool(false);
        driverPool.setMaxTotal(3);
        driverPool.start();

        new File("target/render-output").mkdir();
    }

    @AfterClass
    public static void shutdown() {
        DriverPoolFactory.getDriverPool(false).stop();
    }

    @Test
    public void testHeatmap() throws IOException {
        PhantomJSRenderer imageRenderer = new PhantomJSRenderer();

        FileOutputStream out = new FileOutputStream("target/render-output/phantom-heatmapTest.png");

        HeatmapChart chartOptions = new HeatmapChart()
                .getTitle()
                .setText("Demo graph Heatmmap \u2074")
                .build()
                .setColorAxis(createColorAxis())
                .getChart()
                .setWidth(600)
                .setHeight(400)
                .build();

        chartOptions.addYAxis(new Axis()
                .setTickInterval(1.0)
                .setStartOnTick(Boolean.FALSE)
                .setEndOnTick(Boolean.FALSE)
                .setMin(0)
                .setMax(24)
                .getTitle().setText("Hour").build()
                .getLabels().setFormat("{value}:00").build());

        chartOptions.addXAxis(new Axis()
                .getLabels().setAlign(Alignment.LEFT).setX(5).setY(14).build()
                .setType(AxisType.DATETIME)
                .setShowLastLabel(false));

        final HeatMapSeries mapSeries = SeriesFactory.createSeries(SeriesType.HEAT_MAP);
        mapSeries
                .setColsize(24 * 36e5)
                .getMarker()
                .setSymbol(Marker.MarkerSymbol.RECTANGLE)
                .build();

        generateValues(of(2010, Month.JANUARY, 1, 0, 0), 100, 35).forEach(p -> mapSeries.addPoint(p));
        chartOptions.addSeries(mapSeries);

        imageRenderer.setRenderObject(chartOptions);
        imageRenderer.setOutputStream(out);
        try {
            imageRenderer.render();
        } catch (RenderException e) {
            fail(e.getMessage());
        }

        out.close();
    }
    
    private ColorAxis createColorAxis() {
        final ColorAxis colorAxis = new ColorAxis();

        colorAxis.setEndOnTick(false)
                .setStartOnTick(false);

        // Set the color grading based upon the usage type
        colorAxis.setMin(-10.0).setMax(35.0);
        colorAxis
                .addStop(new ColorAxis.ColorStop(0.0, new GraphColor("#0000FF")))
                .addStop(new ColorAxis.ColorStop(0.22, new GraphColor("#01FD00")))
                .addStop(new ColorAxis.ColorStop(0.44, new GraphColor("#FDFD00")))
                .addStop(new ColorAxis.ColorStop(0.72, new GraphColor("#FD7800")))
                .addStop(new ColorAxis.ColorStop(1, new GraphColor("#FD0000")));

        return colorAxis;
    }
    
    public List<SeriesPoint> generateValues(LocalDateTime startDate, int days, int maxValue) {
        LocalDateTime valueDate = startDate;

        List<SeriesPoint> values = new ArrayList<>();
        while (valueDate.isBefore(startDate.plusDays(days))) {
            values.add(new SeriesPoint()
                    .setX(valueDate.toEpochSecond(ZoneOffset.UTC))
                    .setY(valueDate.getHour())
                    .setValue(new BigDecimal(random.nextDouble() * maxValue, new MathContext(3, RoundingMode.HALF_UP)).doubleValue() - 10));
            valueDate = valueDate.plusHours(1);
        }
        return values;
    }
}
