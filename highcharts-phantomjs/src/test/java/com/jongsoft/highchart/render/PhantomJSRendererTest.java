/*
 * The MIT License
 *
 * Copyright 2015 Jong Soft.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jongsoft.highchart.render;

import com.jongsoft.highchart.Highchart;
import com.jongsoft.highchart.common.Alignment;
import com.jongsoft.highchart.common.Function;
import com.jongsoft.highchart.common.SeriesType;
import com.jongsoft.highchart.phantomjs.DriverPool;
import com.jongsoft.highchart.phantomjs.DriverPoolFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class PhantomJSRendererTest {

    public static final int BULK_TEST_AMOUNT = 2;
    public static final int BURST_AMOUNT = 4;

    private static final Logger LOG = LoggerFactory.getLogger(PhantomJSRendererTest.class);

    @BeforeClass
    public static void setup() {
        final DriverPool driverPool = DriverPoolFactory.getDriverPool(false);
        driverPool.setMaxTotal(50);
        driverPool.start();

        new File("target/render-output").mkdir();
    }

    @AfterClass
    public static void shutdown() {
        DriverPoolFactory.getDriverPool(false).stop();
    }

    @Test
    public void testRender() throws IOException {
        PhantomJSRenderer imageRenderer = new PhantomJSRenderer();

        FileOutputStream out = new FileOutputStream("target/render-output/phantom-testRender.png");

        Highchart chartOptions = createChart();
        chartOptions.addSeries(TestUtils.generateSeries("Series 1", SeriesType.LINE, 50));

        chartOptions.getChart().getEvents()
                .setLoad(new Function("this.renderer.image('https://upload.wikimedia.org/wikipedia/commons/a/ab/Logo_TV_2015.png', 0, 220, 324, 90).add();"));

        imageRenderer.setRenderObject(chartOptions);
        imageRenderer.setOutputStream(out);
        try {
            imageRenderer.render();
        } catch (RenderException e) {
            e.printStackTrace();
        }

        out.close();
    }

    @Test
    public void performanceBulkTest() throws InterruptedException {
        ThreadGroup th = new ThreadGroup("bulklist");
        List<Boolean> complete = new ArrayList<>();
        for (int i = 0; i < BULK_TEST_AMOUNT * BURST_AMOUNT; i++) {
            int iteration = i;
            new Thread(th, () -> {
                try {
                    PhantomJSRenderer imageRenderer = new PhantomJSRenderer();
                    final Highchart createChart = createChart();
                    createChart.addSeries(TestUtils.generateSeries("Series 1", SeriesType.LINE, 100));
                    createChart.addSeries(TestUtils.generateSeries("Series 2", SeriesType.LINE, 100));
                    imageRenderer.setRenderObject(createChart);
                    FileOutputStream outputStream = new FileOutputStream("target/render-output/phantom-performanceBulkTest-" + iteration + ".png");
                    imageRenderer.setOutputStream(outputStream);
                    imageRenderer.render();
                    outputStream.close();
                } catch (RenderException | IOException re) {
                    LOG.error("Failed to run test", re);
                    Assert.fail("Failed to render " + re.getMessage());
                } finally {
                    complete.add(true);
                }
            }).start();
        }

        while (complete.size() < (BURST_AMOUNT * BULK_TEST_AMOUNT)) {
            Thread.sleep(500);
        }
    }

    @Test
    public void performanceBurstTest() throws InterruptedException {
        ThreadGroup th = new ThreadGroup("bulklist");
        List<Boolean> complete = new ArrayList<>();
        List<RenderException> exceptions = new ArrayList<>();
        for (int bulk = 0; bulk < BURST_AMOUNT; bulk++) {
            int bulkIteration = bulk;
            new Thread(th, new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < BULK_TEST_AMOUNT; i++) {
                        int iteration = bulkIteration * i;
                        new Thread(th, () -> {
                            try {
                                PhantomJSRenderer imageRenderer = new PhantomJSRenderer();
                                final Highchart createChart = createChart();
                                createChart.addSeries(TestUtils.generateSeries("Series 1", SeriesType.LINE, 1000));
                                imageRenderer.setRenderObject(createChart);
                                FileOutputStream outputStream = new FileOutputStream("target/render-output/phantom-performanceBurstTest-" + iteration + ".png");
                                imageRenderer.setOutputStream(outputStream);
                                imageRenderer.render();
                                outputStream.close();
                            } catch (RenderException re) {
                                LOG.error("Failed to run test", re);
                                exceptions.add(re);
                            } catch (IOException e) {
                                LOG.error("Failed to run test", e);
                            } finally {
                                complete.add(true);
                            }
                        }).start();
                    }
                }
            }).start();

            Thread.sleep(200);
        }

        while (complete.size() < (BURST_AMOUNT * BULK_TEST_AMOUNT)) {
            Thread.sleep(500);
        }

        Assert.assertTrue(exceptions.isEmpty());
    }

    private Highchart createChart() {
        Highchart chartOptions = new Highchart();
        chartOptions
                .getChart()
                .setWidth(600)
                .setHeight(400)
                .build()
                .getCredits()
                .setText("Sample application")
                .setHref("http://www.example.com")
                .build()
                .getExporting()
                .setEnabled(false);
        chartOptions.getTitle()
                .setText("Demo Highchart line graph")
                .setAlign(Alignment.CENTER);
        chartOptions
                .getCredits()
                .setEnabled(false);
        return chartOptions;
    }
    
}
