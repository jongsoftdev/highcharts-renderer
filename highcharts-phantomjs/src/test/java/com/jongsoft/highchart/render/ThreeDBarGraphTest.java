package com.jongsoft.highchart.render;

import com.jongsoft.highchart.AbstractHighchart;
import com.jongsoft.highchart.Highchart;
import com.jongsoft.highchart.Highchart3D;
import com.jongsoft.highchart.axis.Axis;
import com.jongsoft.highchart.common.Alignment;
import com.jongsoft.highchart.common.SeriesType;
import com.jongsoft.highchart.phantomjs.DriverPool;
import com.jongsoft.highchart.phantomjs.DriverPoolFactory;
import com.jongsoft.highchart.series.BarSeries;
import com.jongsoft.highchart.series.SeriesFactory;
import com.jongsoft.highchart.series.SeriesPoint;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ThreeDBarGraphTest {

    @BeforeClass
    public static void setup() {
        final DriverPool driverPool = DriverPoolFactory.getDriverPool(false);
        driverPool.setMaxTotal(50);
        driverPool.start();

        new File("target/render-output").mkdir();
    }

    @Test
    public void barGraph3D() throws IOException {
        Highchart3D charts = new Highchart3D();
        fillBarChart(charts);

        // @formatter:off
        charts.getChart()
                .getOptions3D()
                    .setDepth(50)
                    .setEnabled(true);
        // @formatter:on

        FileOutputStream out = new FileOutputStream("target/render-output/phantom-barGraph3d.png");
        new PhantomJSRenderer()
                .setRenderObject(charts)
                .setOutputStream(out)
                .render();
        out.close();
    }

    @Test
    public void barGraphImplicit3D() throws IOException {
        Highchart charts = new Highchart();
        fillBarChart(charts);

        FileOutputStream out = new FileOutputStream("target/render-output/phantom-barGraph-implicit3d.png");
        new PhantomJSRenderer()
                .setRenderObject(charts)
                .setOutputStream(out)
                .render();
        out.close();
    }

    private void fillBarChart(AbstractHighchart charts) {
        BarSeries barSeries = SeriesFactory.createSeries(SeriesType.BAR);
        BarSeries barSeries2 = SeriesFactory.createSeries(SeriesType.BAR);

        // @formatter:off
        charts.getChart()
                .setWidth(800)
                .setHeight(400)
                .setAnimation(false)
                .build();
        charts.addXAxis(new Axis()
                    .getTitle()
                        .setText("Year")
                        .setAlign(Alignment.CENTER)
                        .setRotation(270)
                        .build());
        charts.addYAxis(new Axis()
                    .getTitle()
                        .setText("# of voters")
                        .setRotation(0)
                        .build());
        charts.getTitle()
                .setText("Voters in the world")
                .build();
        charts.addSeries(barSeries);
        charts.addSeries(barSeries2);

        barSeries.setDepth(50)
                .addPoint(new SeriesPoint().setX(2010).setY(25))
                .addPoint(new SeriesPoint().setX(2011).setY(15))
                .addPoint(new SeriesPoint().setX(2012).setY(10))
                .addPoint(new SeriesPoint().setX(2013).setY(45))
                .setName("Foo");
        barSeries2.setDepth(50)
                .addPoint(new SeriesPoint().setX(2010).setY(60))
                .addPoint(new SeriesPoint().setX(2011).setY(55))
                .addPoint(new SeriesPoint().setX(2012).setY(76))
                .addPoint(new SeriesPoint().setX(2013).setY(23))
                .setName("Bar");
        // @formatter:on
    }
}
