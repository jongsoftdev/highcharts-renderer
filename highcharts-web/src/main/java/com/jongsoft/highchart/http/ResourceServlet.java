package com.jongsoft.highchart.http;

import com.jongsoft.HighchartModules;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The {@link ResourceServlet} can be used to directly serve the JavaScript libraries needed to support Highchart.js rendering
 * in the client browser.
 * <p>
 * The servlet can be registered in the application using the <code><b>web.xml</b></code> by following the entries below:
 * <pre>
 *   &lt;!-- Register the servlet to a logical name in the webapplication --&gt;
 *   &lt;servlet&gt;
 *       &lt;servlet-name&gt;HighchartResources&lt;/servlet-name&gt;
 *       &lt;servlet-class&gt;com.jongsoft.highchart.http.ResourceServlet&lt;/servlet-class&gt;
 *   &lt;/servlet&gt;
 *
 *   &lt;!-- Register the servlet to an URI using the logical name --&gt;
 *   &lt;servlet-mapping&gt;
 *       &lt;servlet-name&gt;HighchartResources&lt;/servlet-name&gt;
 *       &lt;url-pattern&gt;/highchart/javascript&lt;/url-pattern&gt;
 *   &lt;/servlet-mapping&gt;
 * </pre>
 */
public class ResourceServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(ResourceServlet.class);

    public static final String MODULE_NAME = "m";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] moduleList = Optional.ofNullable(req.getParameterValues(MODULE_NAME))
                .orElse(new String[0]);

        String outputContent = Stream.of(moduleList)
                .map(HighchartModules::byLogicalName)
                .filter(Objects::nonNull)
                .distinct()
                .sorted(Comparator.comparingInt(HighchartModules::getOrder))
                .map(this::moduleToString)
                .collect(Collectors.joining(";"));

        resp.addHeader("Content-Type", "application/javascript");
        resp.addHeader("Cache-Control", "public");
        resp.getWriter().append(outputContent);
    }

    private String moduleToString(HighchartModules highchartModules) {
        try (InputStream module = ResourceServlet.class.getClassLoader().getResourceAsStream("javascript/" + highchartModules.toString())) {
            return IOUtils.toString(module, "UTF-8");
        } catch (IOException ioe) {
            LOG.error("Could not load module {} due to IOException", highchartModules);
        }

        return "";
    }

}
