package com.jongsoft.highchart.http;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class ResourceServletTest {

    private ResourceServlet subject = new ResourceServlet();
    private HttpServletRequest mockedRequest;
    private HttpServletResponse mockedResponse;
    private PrintWriter mockedWriter;

    @Before
    public void setup() throws IOException {
        mockedRequest = mock(HttpServletRequest.class);
        mockedResponse = mock(HttpServletResponse.class);
        mockedWriter = Mockito.mock(PrintWriter.class);

        when(mockedRequest.getProtocol()).thenReturn("HTTP/1.1");
        when(mockedResponse.getWriter()).thenReturn(mockedWriter);
    }

    @Test
    public void getNoModuleParam() throws ServletException, IOException {
        validateContent(equalTo(""));
    }

    @Test
    public void getNonExisting() throws ServletException, IOException {
        when(mockedRequest.getParameterValues(Mockito.eq(ResourceServlet.MODULE_NAME))).thenReturn(
                new String[] {"nono"});

        validateContent(equalTo(""));
    }

    @Test
    public void getHighchartJS() throws ServletException, IOException {
        when(mockedRequest.getParameterValues(Mockito.eq(ResourceServlet.MODULE_NAME))).thenReturn(
                new String[] {"core"});

        validateContent(equalTo("highchart.js"));
    }

    @Test
    public void getHighchart3DJS() throws ServletException, IOException {
        when(mockedRequest.getParameterValues(Mockito.eq(ResourceServlet.MODULE_NAME))).thenReturn(
                new String[] {"core", "3d"});

        validateContent(equalTo("highchart.js;highchart-3d.js"));
    }

    @Test
    public void getHighchart3DJSOutOrder() throws ServletException, IOException {
        when(mockedRequest.getParameterValues(Mockito.eq(ResourceServlet.MODULE_NAME))).thenReturn(
                new String[] {"3d", "core"});

        validateContent(equalTo("highchart.js;highchart-3d.js"));
    }

    private void validateContent(Matcher<String> expectedContent) throws ServletException, IOException {
        subject.doGet(mockedRequest, mockedResponse);

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(mockedResponse).addHeader(Matchers.eq("Content-Type"), Matchers.eq("application/javascript"));
        verify(mockedResponse).addHeader(Matchers.eq("Cache-Control"), Matchers.eq("public"));
        verify(mockedWriter).append(captor.capture());

        assertThat(captor.getValue(), expectedContent);
    }
}
